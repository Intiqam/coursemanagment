<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 16-Oct-19
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:14 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<!-- Login Page Start Here -->
<div class="login-page-wrap">
    <div class="login-page-content">
        <div class="login-box">
            <div class="item-logo">
                <img src="img/logo2.png" alt="logo">
            </div>
            <form action="/"  method="post" class="login-form">


                <div class="form-group">
                    <label>Username</label>
                    <input type="text"  placeholder="Enter userename" class="form-control" name="username">
                    <i class="far fa-envelope"></i>
                    <c:if test="${param.error!=null}">
                        Email veya parol yanlisdir
                    </c:if>
                    <c:if test="${param.logout != null}">       3
                        <p>
                            You have been logged out.
                        </p>
                    </c:if>
                    <input type="hidden"
                           name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" placeholder="Enter password" class="form-control" name="password">

                    <i class="fas fa-lock"></i>


                </div>
                <div class="form-group d-flex align-items-center justify-content-between">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="remember-me" name="remember-me">
                        <label for="remember-me" class="form-check-label">Remember Me</label>
                    </div>
                    <a href="#" class="forgot-btn">Forgot Password?</a>
                </div>
                <div class="form-group">
                    <button type="submit" class="login-btn">Login</button>
                </div>
            </form>
            <div class="login-social">
                <p>or sign in with</p>
                <ul>
                    <li><a href="#" class="bg-fb"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#" class="bg-twitter"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" class="bg-gplus"><i class="fab fa-google-plus-g"></i></a></li>
                    <li><a href="#" class="bg-git"><i class="fab fa-github"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="sign-up">Don't have an account ? <a href="#">Signup now!</a></div>
    </div>
</div>
<!-- Login Page End Here -->
<!-- jquery-->
<script src="js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="js/plugins.js"></script>
<!-- Popper js -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- Scroll Up Js -->
<script src="js/jquery.scrollUp.min.js"></script>
<!-- Custom Js -->
<script src="js/main.js"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:15 GMT -->
</html>
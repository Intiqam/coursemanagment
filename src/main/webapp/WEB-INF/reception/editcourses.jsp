<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 21-Sep-19
  Time: 4:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">


<!-- Mirrored from learning.frontendmatter.com/html/website-instructor-course-edit-course.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:01:57 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Learning</title>

    <!-- Vendor CSS BUNDLE
      Includes styling for all of the 3rd party libraries used with this module, such as Bootstrap, Font Awesome and others.
      TIP: Using bundles will improve performance by reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/vendor/all.css" rel="stylesheet">

    <!-- Vendor CSS Standalone Libraries
          NOTE: Some of these may have been customized (for example, Bootstrap).
          See: src/less/themes/{theme_name}/vendor/ directory -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/bootstrap.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/font-awesome.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/picto.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/material-design-iconic-font.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/datepicker3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.minicolors.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/railscasts.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/owl.carousel.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/slick.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/daterangepicker-bs3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/select2.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.countdown.css" rel="stylesheet"> -->

    <!-- APP CSS BUNDLE [${pageContext.servletContext.contextPath}/css/app/app.css]
  INCLUDES:
      - The APP CSS CORE styling required by the "html" module, also available with main.css - see below;
      - The APP CSS STANDALONE modules required by the "html" module;
  NOTE:
      - This bundle may NOT include ALL of the available APP CSS STANDALONE modules;
        It was optimised to load only what is actually used by the "html" module;
        Other APP CSS STANDALONE modules may be available in addition to what's included with this bundle.
        See src/less/themes/html/app.less
  TIP:
      - Using bundles will improve performance by greatly reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/app/app.css" rel="stylesheet">

    <!-- App CSS CORE
  This variant is to be used when loading the separate styling modules -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/main.css" rel="stylesheet"> -->

    <!-- App CSS Standalone Modules
      As a convenience, we provide the entire UI framework broke down in separate modules
      Some of the standalone modules may have not been used with the current theme/module
      but ALL modules are 100% compatible -->

    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/essentials.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/material.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/layout.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar-skins.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/navbar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/messages.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/media.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/charts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/maps.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-alerts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-background.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-buttons.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-text.css" rel="stylesheet" /> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
  WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Fixed navbar -->
<jsp:include page="../admin/header2.jsp"/>
<jsp:include page="menu.jsp"/>
<div class="container" style="margin-left: 230px;">

    <div class="page-section">
        <div class="row">
            <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">

                <h4 class="page-section-heading">Create Group</h4>
                <form action="" method="post">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-control-material">
                                        <input type="email" class="form-control" id="inputEmail1"
                                               placeholder="Type here..">
                                        <label for="inputEmail1">Name</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="select1">Room name</label>
                                    <select name="select" class="selectpicker" id="select1"
                                            data-style="btn-white" data-live-search="true" data-size="5">
                                        <option>Toronto</option>
                                        <option>Mozambik</option>
                                        <option>Tanzaniya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-none">
                                <div class="col-sm-12">
                                    <label for="fenler">Muellim</label>
                                    <select class="selectpicker" id="fenler" multiple data-style="btn-white" title='Muellim'>
                                        <option>Kamal</option>
                                        <option>Camal</option>
                                        <option>Amal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label for="select3">Mekteb</label>
                                    <select name="select" class="selectpicker" id="select3"
                                            data-style="btn-white" data-live-search="true" data-size="5">
                                        <option>1990</option>
                                        <option>1991</option>
                                        <option>1992</option>
                                        <option>1993</option>
                                        <option>1994</option>
                                        <option>1995</option>
                                        <option>1996</option>
                                        <option>1997</option>
                                        <option>1998</option>
                                        <option>1999</option>
                                        <option>2000</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group margin-bottom-none">
                                <div class="col-sm-12">
                                    <label for="telebe">Sagirdler</label>
                                    <select class="selectpicker" id="telebe" multiple data-style="btn-white" title='Sagirdler'>
                                        <option>Eli</option>
                                        <option>Veli</option>
                                        <option>Sixeli</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>

                        <div class="col-sm-12">
                            <div class="form-control-material">
                                <input type="submit" style="width: 100%;" class="btn btn-primary" href="/login" value="Save">

                            </div>
                        </div>


                </form>

            </div>
        </div>
    </div>

</div>

<!-- Footer -->
<footer class="footer">
    <strong>Learning</strong> v1.1.0 &copy; Copyright 2015
</footer>
<!-- // Footer -->

<!-- Inline Script for colors and config objects; used by various external scripts; -->
<script>
    var colors = {
        "danger-color": "#e74c3c",
        "success-color": "#81b53e",
        "warning-color": "#f0ad4e",
        "inverse-color": "#2c3e50",
        "info-color": "#2d7cb5",
        "default-color": "#6e7882",
        "default-light-color": "#cfd9db",
        "purple-color": "#9D8AC7",
        "mustard-color": "#d4d171",
        "lightred-color": "#e15258",
        "body-bg": "#f6f6f6"
    };
    var config = {
        theme: "html",
        skins: {
            "default": {
                "primary-color": "#42a5f5"
            }
        }
    };
</script>

<!-- Vendor Scripts Bundle
  Includes all of the 3rd party JavaScript libraries above.
  The bundle was generated using modern frontend development tools that are provided with the package
  To learn more about the development process, please refer to the documentation.
  Do not use it simultaneously with the separate bundles above. -->
<script src="${pageContext.servletContext.contextPath}/js/vendor/all.js"></script>

<!-- Vendor Scripts Standalone Libraries -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/bootstrap.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/breakpoints.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.nicescroll.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/isotope.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/packery-mode.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.grid-a-licious.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.cookie.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery-ui.custom.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/handlebars.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/load_image.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.debouncedresize.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/modernizr.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/velocity.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/tables/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/forms/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/media/slick.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/charts/flot/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/nestable/jquery.nestable.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/countdown/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/angular/all.js"></script> -->

<!-- App Scripts Bundle
  Includes Custom Application JavaScript used for the current theme/module;
  Do not use it simultaneously with the standalone modules below. -->
<script src="${pageContext.servletContext.contextPath}/js/app/app.js"></script>

<!-- App Scripts Standalone Modules
  As a convenience, we provide the entire UI framework broke down in separate modules
  Some of the standalone modules may have not been used with the current theme/module
  but ALL the modules are 100% compatible -->

<!-- <script src="${pageContext.servletContext.contextPath}/js/app/essentials.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/material.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/layout.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/sidebar.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/media.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/messages.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/maps.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/charts.js"></script> -->

<!-- App Scripts CORE [html]:
      Includes the custom JavaScript for this theme/module;
      The file has to be loaded in addition to the UI modules above;
      app.js already includes main.js so this should be loaded
      ONLY when using the standalone modules; -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/main.js"></script> -->

</body>


<!-- Mirrored from learning.frontendmatter.com/html/website-instructor-course-edit-course.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:01:59 GMT -->
</html>

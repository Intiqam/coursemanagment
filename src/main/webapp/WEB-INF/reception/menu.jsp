<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/21/19
  Time: 6:58 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="sidebar left sidebar-size-3 sidebar-offset-0 sidebar-visible-desktop sidebar-visible-mobile sidebar-skin-dark" id="sidebar-menu" data-type="collapse">
    <div data-scrollable>

        <div class="sidebar-block">
            <div class="profile">
                <a href="#">
                    <img src="${pageContext.servletContext.contextPath}/images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />
                </a>
                <h4 class="text-display-1 margin-none">Instructor Name</h4>
            </div>
        </div>

        <ul class="sidebar-menu">
            <li class="active"><a href="/reception"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
            <li><a href="/reception/message"><i class="fa fa-paper-plane"></i><span>Messages</span></a></li>
            <li><a href="mycourses"><i class="fa fa-mortar-board"></i><span>My Courses</span></a></li>
            <li><a href="app-instructor-earnings.html"><i class="fa fa-bar-chart-o"></i><span>Earnings</span></a></li>
            <li><a href="app-instructor-statement.html"><i class="fa fa-dollar"></i><span>Statement</span></a></li>
            <li><a href="#"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
        </ul>
    </div>
</div>


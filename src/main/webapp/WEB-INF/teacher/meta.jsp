<%--
  Created by IntelliJ IDEA.
  User: itcity.academy
  Date: 10/19/2019
  Time: 2:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>AKKHOR | All Students</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
<!-- Normalize CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/normalize.css">
<!-- Main CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/main.css">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css">
<!-- Fontawesome CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/all.min.css">
<!-- Flaticon CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/fonts/flaticon.css">
<!-- Animate CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/animate.min.css">
<!-- Data Table CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/jquery.dataTables.min.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/style.css">
<!-- Modernize js -->
<script src="${pageContext.servletContext.contextPath}/js/modernizr-3.6.0.min.js"></script>
<!-- Select 2 CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/select2.min.css">
<!-- Date Picker CSS -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/datepicker.min.css">
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/fullcalendar.min.css">


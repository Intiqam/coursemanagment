<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/21/19
  Time: 4:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="collapse navbar-collapse" id="main-nav">
    <ul class="nav navbar-nav navbar-nav-margin-left">
        <li class="dropdown active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li class="active"><a href="index-2.html">Home page</a></li>
                <li><a href="pricing.html">Pricing</a></li>
                <li><a href="tutors">Tutors</a></li>
                <li><a href="survey.html">Survey</a></li>
                <li><a href="website-forum.html">Forum Home</a></li>
                <li><a href="website-forum-category.html">Forum Category</a></li>
                <li><a href="website-forum-thread.html">Forum Thread</a></li>
                <li><a href="website-blog.html">Blog Listing</a></li>
                <li><a href="website-blog-post.html">Blog Post</a></li>
                <li><a href="website-contact.html">Contact</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="website-directory-grid.html">Grid Directory</a></li>
                <li><a href="website-directory-list.html">List Directory</a></li>
                <li><a href="website-course.html">Single Course</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Student <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="website-user-dashboard.html">Dashboard</a></li>
                <li><a href="website-user-courses.html">My Courses</a></li>
                <li><a href="website-take-course.html">Take Course</a></li>
                <li><a href="website-course-forums.html">Course Forums</a></li>
                <li><a href="website-take-quiz.html">Take Quiz</a></li>
                <li><a href="website-user-messages.html">Messages</a></li>
                <li><a href="website-user-profile.html">Private Profile</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Instructor <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="website-instructor-dashboard.html">Dashboard</a></li>
                <li><a href="website-instructor-courses.html">My Courses</a></li>
                <li><a href="website-instructor-course-edit-course.html">Edit Course</a></li>
                <li><a href="website-instructor-earnings.html">Earnings</a></li>
                <li><a href="website-instructor-statement.html">Statement</a></li>
                <li><a href="website-instructor-messages.html">Messages</a></li>
                <li><a href="website-instructor-profile.html">Private Profile</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">UI <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="essential-buttons.html">Buttons</a></li>
                <li><a href="essential-icons.html">Icons</a></li>
                <li><a href="essential-progress.html">Progress</a></li>
                <li><a href="essential-grid.html">Grid</a></li>
                <li><a href="essential-forms.html">Forms</a></li>
                <li><a href="essential-tables.html">Tables</a></li>
                <li><a href="essential-tabs.html">Tabs</a></li>
            </ul>
        </li>
    </ul>
    <div class="navbar-right">
        <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">
            <!-- user -->
            <!-- // END user -->
        </ul>
        <a href="login" class="navbar-btn btn btn-primary">Log In</a>
    </div>
</div>

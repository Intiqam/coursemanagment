
<%--
  Created by IntelliJ IDEA.
  User: rahil
  Date: 20/09/2019
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from learning.frontendmatter.com/html/sign-up.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:02:26 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
   <jsp:include page="../meta.jsp"/>
    <title>Learning</title>

    <!-- Vendor CSS BUNDLE
      Includes styling for all of the 3rd party libraries used with this module, such as Bootstrap, Font Awesome and others.
      TIP: Using bundles will improve performance by reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/vendor/all.css" rel="stylesheet">

    <!-- Vendor CSS Standalone Libraries
          NOTE: Some of these may have been customized (for example, Bootstrap).
          See: src/less/themes/{theme_name}/vendor/ directory -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/bootstrap.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/font-awesome.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/picto.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/material-design-iconic-font.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/datepicker3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.minicolors.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/railscasts.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/owl.carousel.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/slick.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/daterangepicker-bs3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/select2.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.countdown.css" rel="stylesheet"> -->

    <!-- APP CSS BUNDLE [${pageContext.servletContext.contextPath}/css/app/app.css]
  INCLUDES:
      - The APP CSS CORE styling required by the "html" module, also available with main.css - see below;
      - The APP CSS STANDALONE modules required by the "html" module;
  NOTE:
      - This bundle may NOT include ALL of the available APP CSS STANDALONE modules;
        It was optimised to load only what is actually used by the "html" module;
        Other APP CSS STANDALONE modules may be available in addition to what's included with this bundle.
        See src/less/themes/html/app.less
  TIP:
      - Using bundles will improve performance by greatly reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/app/app.css" rel="stylesheet">

    <!-- App CSS CORE
  This variant is to be used when loading the separate styling modules -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/main.css" rel="stylesheet"> -->

    <!-- App CSS Standalone Modules
      As a convenience, we provide the entire UI framework broke down in separate modules
      Some of the standalone modules may have not been used with the current theme/module
      but ALL modules are 100% compatible -->

    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/essentials.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/material.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/layout.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar-skins.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/navbar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/messages.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/media.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/charts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/maps.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-alerts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-background.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-buttons.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-text.css" rel="stylesheet" /> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
  WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="login">

<div id="content">
    <div class="container-fluid">

        <div class="lock-container">
            <div class="panel panel-default text-center paper-shadow" data-z="0.5">
                <h1 class="text-display-1">Create account</h1>
                <div class="panel-body">

                    <!-- Signup -->
                    <form:form modelAttribute="registrationForm" action="register" method="post">
                        <div class="form-group">
                            <div class="form-control-material">
                                <label for="firstName">First name</label>
                                <form:errors path="name" cssStyle="color: red" style="margin-top: 5px"/>
                                <form:input  path="name" id="firstName" type="text" class="form-control" placeholder="First Name" cssStyle="margin-bottom: 3px"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-material">
                                <form:input path="surname" id="lastName" type="text" class="form-control" placeholder="Last Name"/>
                                <label style="margin-bottom: 5px;" for="lastName">Last name</label>
                                <form:errors path="surname" cssStyle="color: red"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-material">
                                <form:input path="email" id="email" type="email" class="form-control" placeholder="Email"/>
                                <label style="margin-bottom: 5px;" for="email">Email address</label>
                                <form:errors path="email" cssStyle="color: red"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-material">
                                <form:input path="password" id="password" type="password" class="form-control" placeholder="Password"/>
                                <label style="margin-bottom: 3px;"  for="password">Password</label>
                                <form:errors path="password" cssStyle="color: red"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-control-material">
                                <form:input path="passwordConfirmation" id="passwordConfirmation" type="password" class="form-control" placeholder="Password Confirmation"/>
                                <label style="margin-bottom: 5px;" for="passwordConfirmation">Re-type password</label>
                                <form:errors path="passwordConfirmation" cssStyle="color: red"/>

                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="checkbox">
                                <input type="checkbox" id="agree" />
                                <label for="agree">* I Agree with <a href="#">Terms &amp; Conditions!</a></label>
                            </div>
                        </div>
                        <div class="text-center">
<%--
                            <a href="website-user-dashboard.html" class="btn btn-primary">Create an Account</a>
--%>
                            <input type="submit"value="Create an Account">
                        </div>
                    </form:form>
                        <!-- //Signup -->

                </div>
            </div>
        </div>

    </div>
</div>

<!-- Footer -->
<jsp:include page="../footer.jsp"/>
<!-- // Footer -->
<!-- Inline Script for colors and config objects; used by various external scripts; -->
<script>

    $(document).ready(function () {

        function is_valid_email(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $('#email').change(function () {
            var email = $('#email').val();
            console.log('email change event, value = ' + email);

            var is_valid = is_valid_email(email);
            console.log('is valid email = ' + is_valid);

            if (is_valid) {
                console.log('call ajax email validation');
                // validate on backedn
                $.ajax({
                    type: 'GET',
                    data: {type: 'email',
                        value: email
                    },
                    url: 'reg-validate',
                    // async: false,

                    success: function (result) {
                        console.log('server response = ' + result);

                        if (result) {
                            $('#email_span').text('Bu email artiq qeydiyyatdan kecib');
                            $('#email_span').show();
                            // todo fix
                            // $('#register_btn').attr('disabled', true);
                        } else {
                            $('#register_btn').attr('disabled', false);
                        }
                    },
                    error: function () {
                        alert('server error');
                    }
                });
            } else {
                console.log('email is invalid');
                $('#email_span').val('Email yanlisdir');
                $('#email_span').show();
                console.log('email error msg = ' + $('#email_span').val());
                alert($('#email_span').val());
                // todo fix
                // $('#register_btn').attr('disabled', true);
            }

        });
    });




    var colors = {
        "danger-color": "#e74c3c",
        "success-color": "#81b53e",
        "warning-color": "#f0ad4e",
        "inverse-color": "#2c3e50",
        "info-color": "#2d7cb5",
        "default-color": "#6e7882",
        "default-light-color": "#cfd9db",
        "purple-color": "#9D8AC7",
        "mustard-color": "#d4d171",
        "lightred-color": "#e15258",
        "body-bg": "#f6f6f6"
    };
    var config = {
        theme: "html",
        skins: {
            "default": {
                "primary-color": "#42a5f5"
            }
        }
    };
</script>

<!-- Vendor Scripts Bundle
  Includes all of the 3rd party JavaScript libraries above.
  The bundle was generated using modern frontend development tools that are provided with the package
  To learn more about the development process, please refer to the documentation.
  Do not use it simultaneously with the separate bundles above. -->
<script src="${pageContext.servletContext.contextPath}/js/vendor/all.js"></script>

<!-- Vendor Scripts Standalone Libraries -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/bootstrap.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/breakpoints.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.nicescroll.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/isotope.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/packery-mode.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.grid-a-licious.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.cookie.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery-ui.custom.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/handlebars.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/load_image.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.debouncedresize.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/modernizr.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/velocity.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/tables/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/forms/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/media/slick.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/charts/flot/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/nestable/jquery.nestable.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/countdown/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/angular/all.js"></script> -->

<!-- App Scripts Bundle
  Includes Custom Application JavaScript used for the current theme/module;
  Do not use it simultaneously with the standalone modules below. -->
<script src="${pageContext.servletContext.contextPath}/js/app/app.js"></script>

<!-- App Scripts Standalone Modules
  As a convenience, we provide the entire UI framework broke down in separate modules
  Some of the standalone modules may have not been used with the current theme/module
  but ALL the modules are 100% compatible -->

<!-- <script src="${pageContext.servletContext.contextPath}/js/app/essentials.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/material.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/layout.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/sidebar.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/media.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/messages.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/maps.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/charts.js"></script> -->

<!-- App Scripts CORE [html]:
      Includes the custom JavaScript for this theme/module;
      The file has to be loaded in addition to the UI modules above;
      app.js already includes main.js so this should be loaded
      ONLY when using the standalone modules; -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/main.js"></script> -->

</body>


<!-- Mirrored from learning.frontendmatter.com/html/sign-up.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:02:26 GMT -->
</html>

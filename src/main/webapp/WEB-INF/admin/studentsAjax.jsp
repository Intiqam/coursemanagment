<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 17-Dec-19
  Time: 5:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>DataTable</title>
</head>
<body>
                <table id="stList">
                    <thead>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Surname</td>
                        <td>Gender</td>
                        <td>Class</td>
                        <td>Section</td>
                        <td>DateOfBirth</td>
                        <td>Phone</td>
                        <td>Action</td>
                        <td>Full Name</td>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

</body>



<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
<link rel="stylesheet" type="text/css" href="/media/css/site-examples.css?_=50d134a99f38dd18b1cce14412acc94f">
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css">

<style type="text/css" class="init">

</style>
<script type="text/javascript" src="/media/js/site.js?_=a64810efc82bfd3b645784011efa5963"></script>
<script type="text/javascript" src="/media/js/dynamic.php?comments-page=examples%2Fstyling%2FjqueryUI.html" async></script>
<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.jqueryui.min.js"></script>
<script type="text/javascript" language="javascript" src="../resources/demo.js"></script>



<%--<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">--%>
<%--
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
--%>



<script type="text/javascript" class="init">

    $(document).ready(function () {
        $('#stList').DataTable(
            {
                dom: 'Blfrtip',
                buttons: [
                    'selectAll',
                    'selectNone'
                ],
                language: {
                    buttons: {
                        selectAll: "Select all items",
                        selectNone: "Select none"
                    }
                },
                "processing": true,
                "serverSide": true,
                "ajax": "/rest/datatable",
                "lengthMenu": [[5, 10, 25, 50,100,-1], [5, 10, 25, 50, 100, "All"]],

                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "surname"},
                    {"data": "gender"},
                    {"data": "class"},
                    {"data": "section"},
                    {"data": "birthdate"},
                    {"data": "phone"},

                    {
                        "data": function renderActionLinks(data, type, dataToSet) {
                            return "<a target='_blank' href='/view-person?id=" + data.id + "'>View</a>&nbsp;" +
                                "<a target='_blank' href='/edit-person?id=" + data.id + "'>Edit</a>&nbsp;" +
                                "<a target='_blank' href='/delete-person?id=" + data.id + "'>Delete</a>&nbsp;";
                        }
                    },
                    {
                        "data": function showFullName(data, type, dataToSet) {
                            return data.name + ' ' + data.surname;
                        }
                    }
                ]
            })

    })
</script>



</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 17-Oct-19
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/all-student.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

    <jsp:include page="meta.jsp"/>





</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->

    <jsp:include page="header.jsp"/>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->

        <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Students</h3>
                <ul>
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>All Teachers</li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->
            <!-- Student Table Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>All Teachers Data</h3>&nbsp;
                            <h4 style="color: red">${mesage}</h4>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-times text-orange-red"></i>Close</a>
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                            </div>
                        </div>
                    </div>
                    <form class="mg-b-20">

                    </form>
                    <div class="table-responsive" >
                        <table class="table display  text-nowrap"  id="teacherTable">
                            <thead>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input checkAll">
                                        <label class="form-check-label">Roll</label>
                                    </div>
                                </th>
                                <th>Photo</th>
                                <th>Fincod</th>
                                <th>FullName</th>
                                <th>Gender</th>
                                <th>Section</th>
                                <th>Diplom no</th>
                                <th>Date Of Birth</th>
                                <th>Hire date</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>E-mail</th>
                            </tr>
                            </thead>

                            <tbody>
                            <c:if test="${not empty teachers}">

                                <c:forEach items="${teachers}" var="teacher">
                                    <tr>
                                        <td>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input">
                                                <label class="form-check-label">${teacher.id}</label>
                                            </div>
                                        </td>
                                        <td class="text-center"><img src="${pageContext.servletContext.contextPath}/img/figure/student2.png" alt="student"></td>
                                       <td>${teacher.fincod}</td>
                                        <td>${teacher.name} ${teacher.surname} ${teacher.fatherName}</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${teacher.gender ==1}">
                                                    kisi
                                                </c:when>
                                                <c:otherwise>
                                                    qadin
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${teacher.section==0}">
                                                    Aze
                                                </c:when>
                                                <c:otherwise>
                                                    Rus
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>${teacher.diplomNo} </td>
                                        <td>${teacher.birthDate}</td>
                                        <td>${teacher.idate}</td>
                                        <td>
                                                ${teacher.contact.address}</td>
                                        <td>${teacher.contact.telMobile}</td>
                                        <td>${teacher.contact.email}</td>

                                        <td>
                                            <div class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                   aria-expanded="false">
                                                    <span class="flaticon-more-button-of-three-dots"></span>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><i
                                                            class="fas fa-times text-orange-red"></i>Close</a>
                                                    <a class="dropdown-item" href="/admin/studentDetail/${teacher.id}"><i
                                                            class="fas fa-cogs text-dark-pastel-green"></i>View</a>
                                                    <a class="dropdown-item" href="#"><i
                                                            class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>

                            </c:if>

                            </tbody>
                            <tbody>
                            <tr>
                                <th>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input checkAll">
                                        <label class="form-check-label">Roll</label>
                                    </div>
                                </th>
                                <th>Photo</th>
                                <th>Fincod</th>
                                <th>FullName</th>
                                <th>Gender</th>
                                <th>Section</th>
                                <th>Diplom no</th>
                                <th>Date Of Birth</th>
                                <th>Hire date</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>E-mail</th>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- Student Table Area End Here -->
            <%-- <footer class="footer-wrap-layout1">
                 <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by <a
                         href="#">PsdBosS</a></div>
             </footer>--%>
            <jsp:include page="footer.jsp"/>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Data Table Js -->

<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>



<script type="text/javascript">

    $(document).ready(function () {
        console.log('Salam ');
        $('#teacherTable').DataTable({
            // "processing":true,
            // "serverSide":true,
            // "ajax":"/reception-rest/",
      "lengthMenu":[[5,7,9,-1],[5,7,9,"All"]]
        });
    });
</script>

</html>

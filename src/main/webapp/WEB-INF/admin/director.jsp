<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 20-Sep-19
  Time: 7:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html class="st-layout ls-top-navbar-large ls-bottom-footer show-sidebar sidebar-l3" lang="en">


<!-- Mirrored from learning.frontendmatter.com/html/app-instructor-dashboard.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:02:29 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <jsp:include page="../meta.jsp"/>
    <title>Learning</title>

    <!-- Vendor CSS BUNDLE
      Includes styling for all of the 3rd party libraries used with this module, such as Bootstrap, Font Awesome and others.
      TIP: Using bundles will improve performance by reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/vendor/all.css" rel="stylesheet">

    <!-- Vendor CSS Standalone Libraries
          NOTE: Some of these may have been customized (for example, Bootstrap).
          See: src/less/themes/{theme_name}/vendor/ directory -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/bootstrap.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/${pageContext.servletContext.contextPath}/css/vendor/font-awesome.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/picto.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/material-design-iconic-font.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/datepicker3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.minicolors.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/railscasts.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/owl.carousel.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/slick.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/daterangepicker-bs3.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/select2.css" rel="stylesheet"> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/vendor/jquery.countdown.css" rel="stylesheet"> -->

    <!-- APP CSS BUNDLE [${pageContext.servletContext.contextPath}/css/app/app.css]
  INCLUDES:
      - The APP CSS CORE styling required by the "html" module, also available with main.css - see below;
      - The APP CSS STANDALONE modules required by the "html" module;
  NOTE:
      - This bundle may NOT include ALL of the available APP CSS STANDALONE modules;
        It was optimised to load only what is actually used by the "html" module;
        Other APP CSS STANDALONE modules may be available in addition to what's included with this bundle.
        See src/less/themes/html/app.less
  TIP:
      - Using bundles will improve performance by greatly reducing the number of network requests the client needs to make when loading the page. -->
    <link href="${pageContext.servletContext.contextPath}/css/app/app.css" rel="stylesheet">

    <!-- App CSS CORE
  This variant is to be used when loading the separate styling modules -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/main.css" rel="stylesheet"> -->

    <!-- App CSS Standalone Modules
      As a convenience, we provide the entire UI framework broke down in separate modules
      Some of the standalone modules may have not been used with the current theme/module
      but ALL modules are 100% compatible -->

    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/essentials.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/material.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/layout.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/sidebar-skins.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/navbar.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/messages.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/media.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/charts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/maps.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-alerts.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-background.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-buttons.css" rel="stylesheet" /> -->
    <!-- <link href="${pageContext.servletContext.contextPath}/css/app/colors-text.css" rel="stylesheet" /> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
  WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Wrapper required for sidebar transitions -->
<div class="st-container">

    <!-- Fixed navbar -->
    <jsp:include page="header2.jsp"/>
    <!-- Sidebar component with st-effect-1 (set on the toggle button within the navbar) -->
    <div class="sidebar left sidebar-size-3 sidebar-offset-0 sidebar-visible-desktop sidebar-visible-mobile sidebar-skin-dark" id="sidebar-menu" data-type="collapse">
        <div data-scrollable>

            <div class="sidebar-block">
                <div class="profile">
                    <a href="#">
                        <img src="${pageContext.servletContext.contextPath}/images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />
                    </a>
                    <h4 class="text-display-1 margin-none">Instructor Name</h4>
                </div>
            </div>

            <ul class="sidebar-menu">
                <li class="active"><a href="app-instructor-dashboard.html"><i class="fa fa-home"></i><span>Dashboard</span></a></li>
                <li><a href="app-instructor-messages.html"><i class="fa fa-paper-plane"></i><span>Messages</span></a></li>
                <li><a href="app-instructor-courses.html"><i class="fa fa-mortar-board"></i><span>My Courses</span></a></li>
                <li><a href="app-instructor-earnings.html"><i class="fa fa-bar-chart-o"></i><span>Earnings</span></a></li>
                <li><a href="app-instructor-statement.html"><i class="fa fa-dollar"></i><span>Statement</span></a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
            </ul>
        </div>
    </div>

    <!-- sidebar effects OUTSIDE of st-pusher: -->
    <!-- st-effect-1, st-effect-2, st-effect-4, st-effect-5, st-effect-9, st-effect-10, st-effect-11, st-effect-12, st-effect-13 -->

    <!-- content push wrapper -->
    <div class="st-pusher" id="content">

        <!-- sidebar effects INSIDE of st-pusher: -->
        <!-- st-effect-3, st-effect-6, st-effect-7, st-effect-8, st-effect-14 -->

        <!-- this is the wrapper for the content -->
        <div class="st-content">

            <!-- extra div for emulating position:fixed of the menu -->
            <div class="st-content-inner padding-none">

                <div class="container-fluid">

                    <div class="page-section">
                        <h1 class="text-display-1">Dashboard</h1>
                    </div>

                    <div class="row" data-toggle="isotope">
                        <div class="item col-xs-12 col-lg-6">
                            <div class="panel panel-default paper-shadow" data-z="0.5">
                                <div class="panel-heading">
                                    <div class="media v-middle">
                                        <div class="media-body">
                                            <h4 class="text-headline margin-none">Earnings</h4>
                                            <p class="text-subhead text-light">This Month</p>
                                        </div>
                                        <div class="media-right">
                                            <a class="btn btn-white btn-flat" href="app-instructor-earnings.html">Reports</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="line-holder" data-toggle="flot-chart-earnings" class="flotchart-holder height-200"></div>
                                </div>
                                <hr/>
                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-md-6">
                                            <h4 class="margin-none">Gross Revenue</h4>
                                            <p class="text-display-1 text-warning margin-none">102.4k</p>
                                        </div>
                                        <div class="col-md-6">
                                            <h4 class="margin-none">Net Revenue</h4>
                                            <p class="text-display-1 text-success margin-none">55k</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item col-xs-12 col-lg-6">
                            <div class="panel panel-default paper-shadow" data-z="0.5">
                                <div class="panel-heading">
                                    <h4 class="text-headline margin-none">My Courses</h4>
                                    <p class="text-subhead text-light">Your recent courses</p>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item media v-middle">
                                        <div class="media-body">
                                            <a href="app-instructor-course-edit-course.html" class="text-subhead list-group-link">Basics of HTML</a>
                                        </div>
                                        <div class="media-right">
                                            <div class="progress progress-mini width-100 margin-none">
                                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item media v-middle">
                                        <div class="media-body">
                                            <a href="app-instructor-course-edit-course.html" class="text-subhead list-group-link">Angular in Steps</a>
                                        </div>
                                        <div class="media-right">
                                            <div class="progress progress-mini width-100 margin-none">
                                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item media v-middle">
                                        <div class="media-body">
                                            <a href="app-instructor-course-edit-course.html" class="text-subhead list-group-link">Bootstrap Foundations</a>
                                        </div>
                                        <div class="media-right">
                                            <div class="progress progress-mini width-100 margin-none">
                                                <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="panel-footer text-right">
                                    <a href="app-instructor-courses.html" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated>View all</a>
                                    <a href="app-instructor-course-edit-course.html" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated>CREATE COURSE <i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="item col-xs-12 col-lg-6">

                        </div>
                    </div>
                    <div class="panel panel-default" style="margin-top: 140px; width: 100%">
                     <a href="apply"> <button class="btn btn-primary">Add Student</button></a>
                        <!-- Data table -->
                        <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Ad</th>
                                <th>Soyad</th>
                                <th>Ata adı</th>
                                <th>tel</th>
                                <th>Borc</th>
                                <th>Fənnlər</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Ad</th>
                                <th>Soyad</th>
                                <th>Ata adı</th>
                                <th>tel</th>
                                <th>Borc</th>
                                <th>Fənnlər</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr>
                                <td>Rahil</td>
                                <td>Vəliyev</td>
                                <td>-----</td>
                                <td>0702548500</td>
                                <td>120</td>
                                <td>Az dili,Riyaiyyat</td>
                            </tr>

                            </tbody>
                        </table>
                        <!-- // Data table -->
                    </div>
                </div>

            </div>
            <!-- /st-content-inner -->

        </div>
        <!-- /st-content -->

    </div>
    <!-- /st-pusher -->

    <!-- Footer -->
    <jsp:include page="../footer.jsp"/>
    <!-- // Footer -->

</div>
<!-- /st-container -->

<!-- Inline Script for colors and config objects; used by various external scripts; -->
<script>
    var colors = {
        "danger-color": "#e74c3c",
        "success-color": "#81b53e",
        "warning-color": "#f0ad4e",
        "inverse-color": "#2c3e50",
        "info-color": "#2d7cb5",
        "default-color": "#6e7882",
        "default-light-color": "#cfd9db",
        "purple-color": "#9D8AC7",
        "mustard-color": "#d4d171",
        "lightred-color": "#e15258",
        "body-bg": "#f6f6f6"
    };
    var config = {
        theme: "html",
        skins: {
            "default": {
                "primary-color": "#42a5f5"
            }
        }
    };
</script>

<!-- Vendor Scripts Bundle
  Includes all of the 3rd party JavaScript libraries above.
  The bundle was generated using modern frontend development tools that are provided with the package
  To learn more about the development process, please refer to the documentation.
  Do not use it simultaneously with the separate bundles above. -->
<script src="${pageContext.servletContext.contextPath}/js/vendor/all.js"></script>

<!-- Vendor Scripts Standalone Libraries -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/bootstrap.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/breakpoints.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.nicescroll.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/isotope.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/packery-mode.pkgd.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.grid-a-licious.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.cookie.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery-ui.custom.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/handlebars.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.hotkeys.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/load_image.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/jquery.debouncedresize.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/modernizr.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/core/velocity.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/tables/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/forms/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/media/slick.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/charts/flot/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/nestable/jquery.nestable.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/countdown/all.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/vendor/angular/all.js"></script> -->

<!-- App Scripts Bundle
  Includes Custom Application JavaScript used for the current theme/module;
  Do not use it simultaneously with the standalone modules below. -->
<script src="${pageContext.servletContext.contextPath}/js/app/app.js"></script>

<!-- App Scripts Standalone Modules
  As a convenience, we provide the entire UI framework broke down in separate modules
  Some of the standalone modules may have not been used with the current theme/module
  but ALL the modules are 100% compatible -->

<!-- <script src="${pageContext.servletContext.contextPath}/js/app/essentials.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/material.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/layout.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/sidebar.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/media.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/messages.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/maps.js"></script> -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/charts.js"></script> -->

<!-- App Scripts CORE [html]:
      Includes the custom JavaScript for this theme/module;
      The file has to be loaded in addition to the UI modules above;
      app.js already includes main.js so this should be loaded
      ONLY when using the standalone modules; -->
<!-- <script src="${pageContext.servletContext.contextPath}/js/app/main.js"></script> -->

</body>


<!-- Mirrored from learning.frontendmatter.com/html/app-instructor-dashboard.html by HTTraQt Website Copier/1.x [Karbofos 2012-2017] Wed, 18 Sep 2019 17:02:29 GMT -->
</html>

<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: itcity.academy
  Date: 10/18/2019
  Time: 1:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/all-subject.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:17 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->

    <jsp:include page="header.jsp"/>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area Start Here -->

        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->

            <br/>
            <div class="ui-btn-wrap">

                <button id="studentBtnId" type="button" class="btn-fill-xl radius-30 text-light shadow-orange-peel bg-orange-peel">Student</button>
                <button id="attendenceBtnId"  type="button" class="btn-fill-xl radius-30 text-light shadow-orange-peel bg-orange-peel">Attendence</button>
                <button id="timeTableBtnId" type="button" class="btn-fill-xl radius-30 text-light shadow-orange-peel bg-orange-peel">TimeTable</button>


            </div>
            <br/>
            <!-- Breadcubs Area End Here -->

            <div class="row">
                <!-- All Student Table Start Here -->
                <div id="studentTablId" class="col-8-xxxl col-12">
                    <div class="card height-auto">
                        <div class="card-body">
                            <div class="heading-layout1">
                                <div class="item-title">
                                    <h3>All Subjects</h3>
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                       aria-expanded="false">...</a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-times text-orange-red"></i>Delete</a>
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                        <a class="dropdown-item" href="#"><i
                                                class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                    </div>
                                </div>
                            </div>
                            <form class="mg-b-20">
                                <div class="row gutters-8">
                                    <div class="col-lg-4 col-12 form-group">
                                        <input type="text" placeholder="Search by Exam ..." class="form-control">
                                    </div>
                                    <div class="col-lg-3 col-12 form-group">
                                        <input type="text" placeholder="Search by Subject ..." class="form-control">
                                    </div>
                                    <div class="col-lg-3 col-12 form-group">
                                        <input type="text" placeholder="dd/mm/yyyy" class="form-control">
                                    </div>
                                    <div class="col-lg-2 col-12 form-group">
                                        <button type="submit"
                                                class="fw-btn-fill btn-gradient-yellow">SEARCH</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table display data-table text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input checkAll">
                                                <label class="form-check-label">ID</label>
                                            </div>
                                        </th>
                                        <th>Name</th>
                                        <th>Surname</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Address</th>

                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <c:if test="${not empty studentList}">
                                        <c:forEach items="${studentList}" var="student">
                                            <tr>
                                                <td>
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input">
                                                        <label class="form-check-label">${student.id}</label>
                                                    </div>
                                                </td>
                                                <td>${student.name}</td>
                                                <td>${student.surname}</td>
                                                <td>${student.contact.telMobile}</td>
                                                <td>${student.contact.email}</td>
                                                <td>${student.contact.address}</td>

                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                           aria-expanded="false">
                                                            <span class="flaticon-more-button-of-three-dots"></span>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="/admin/delete/${subjectl.id}"><i
                                                                    class="fas fa-times text-orange-red"></i>Delete</a>

                                                            <a class="dropdown-item" href="/admin/edit/${subjectl.id}"><i
                                                                    class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>

                                                            <a class="dropdown-item" href="/admin/subjects"><i
                                                                    class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                                            <a class="dropdown-item" href="/admin/groups/${subjectl.id}"><i
                                                                    class="fas fa-ad text-orange-peel"></i>Groups</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Student Attendence Area Start Here -->
                <div  id="studentAttendanceId" class="card-body">
                    <!-- Student Attendence Search Area Start Here -->
                            <div class=" col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="heading-layout1">
                                            <div class="item-title">
                                                <h3>Student Attendence</h3>
                                            </div>
                                            <div class="dropdown">
                                                <a class="dropdown-toggle" href="#" role="button"
                                                   data-toggle="dropdown" aria-expanded="false">...</a>

                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><i class="fas fa-times text-orange-red"></i>Close</a>
                                                    <a class="dropdown-item" href="#"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                                    <a class="dropdown-item" href="#"><i class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="new-added-form">
                                            <div class="row">
                                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                                    <label>Select Class</label>
                                                    <select class="select2">
                                                        <option value="">Select Class</option>
                                                        <option value="1">Nursery</option>
                                                        <option value="2">Play</option>
                                                        <option value="3">One</option>
                                                        <option value="4">Two</option>
                                                        <option value="5">Three</option>
                                                    </select>
                                                </div>
                                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                                    <label>Select Section</label>
                                                    <select class="select2">
                                                        <option value="0">Select Section</option>
                                                        <option value="1">A</option>
                                                        <option value="2">B</option>
                                                        <option value="3">C</option>
                                                        <option value="4">D</option>
                                                    </select>
                                                </div>
                                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                                    <label>Select Month</label>
                                                    <select class="select2">
                                                        <option value="0">Select Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">August</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                                <div class="col-xl-3 col-lg-6 col-12 form-group">
                                                    <label>Select Session</label>
                                                    <select class="select2">
                                                        <option value="0">Select Session</option>
                                                        <option value="1">2016-2017</option>
                                                        <option value="2">2017-20108</option>
                                                        <option value="3">2018-2019</option>
                                                        <option value="4">2020-2021</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 form-group mg-t-8">
                                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                                    <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Student Attendence Search Area End Here -->
                            <div class="table-responsive">
                                <form action="/admin/attendance/${id}" method="post">
                                    <table class="table bs-table table-striped table-bordered text-nowrap">
                                        <thead>
                                        <tr>
                                            <th class="text-left">ID</th>
                                            <th class="text-left">Students</th>
                                            <th><%=LocalDate.now()%></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:choose>
                                            <c:when test="${not empty studentList}">
                                                <c:forEach items="${studentList}" var="student">
                                                    <tr>
                                                        <td class="text-left">${student.id}</td>
                                                        <td class="text-left">${student.name}  ${student.surname}</td>
                                                        <td>
                                                             <input type="radio"  name="${student.id}" value="0">Present </input>
                                                            &nbsp;<input type="radio" style="margin-left: 200px;" name="${student.id}" value="1">Absent</input>
                                                        </td>
                                                    </tr>

                                                </c:forEach>
                                            </c:when>
                                        </c:choose>



                                        </tbody>
                                    </table>
                                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                </form>
                            </div>
                        </div>


                <!-- All Subjects Area End Here -->
            </div>




            <%-- <footer class="footer-wrap-layout1">
                 <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by <a
                         href="#">PsdBosS</a></div>
             </footer>--%>
            <jsp:include page="footer.jsp"/>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Select 2 Js -->
<script src="${pageContext.servletContext.contextPath}/js/select2.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Data Table Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.dataTables.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>


<script>

    $('#studentBtnId').click(function () {
        console.log('clicked student button ');
        $('#studentTablId').show();
        $('#studentAttendanceId').hide();


    });
    $('#attendenceBtnId').click(function () {
        console.log('clicked student button ');
        $('#studentAttendanceId').show();
        $('#studentTablId').hide();


    });



    $(document).ready(function () {
        $('#studentAttendanceId').hide();
        console.log('document ready test');
    });

</script>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/all-subject.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:17 GMT -->
</html>
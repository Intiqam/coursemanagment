<%--
  Created by IntelliJ IDEA.
  User: itcity.academy
  Date: 10/18/2019
  Time: 8:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/student-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
   <jsp:include page="header.jsp"/>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->
      <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Students</h3>
                <ul>
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>Student Details</li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->
            <!-- Student Details Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>About Me</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i class="fas fa-times text-orange-red"></i>Close</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                            </div>
                        </div>
                    </div>
                    <div class="single-info-details">
                        <div class="item-img">
                            <img src="${pageContext.servletContext.contextPath}/admin/media/${user.id}" alt="student" id="profil">
                        </div>
                        <div class="item-content">
                            <div class="header-inline item-header">
                                <h3 class="text-dark-medium font-medium">Jessia Rose</h3>
                                <div class="header-elements">
                                    <ul>
                                        <li><a href="#"><i class="far fa-edit"></i></a></li>
                                        <li><a href="#"><i class="fas fa-print"></i></a></li>
                                        <li><a href="#"><i class="fas fa-download"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <p>Aliquam erat volutpat. Curabiene natis massa sedde lacu stiquen sodale
                                word moun taiery.Aliquam erat volutpaturabiene natis massa sedde  sodale
                                word moun taiery.</p>
                            <div class="info-table table-responsive">

                                <form action="" method="post">
                                    <table class="table text-nowrap">
                                        <tbody>
                                        <tr>
                                            <td>ID:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" readonly value="${user.id}"></td>
                                        </tr>
                                        <tr>
                                            <td>Name:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.name}"> </td>
                                        </tr>
                                        <tr>
                                            <td>Gender:</td>
                                            <td class="font-medium text-dark-medium">
                                                <c:choose>
                                                    <c:when test="${user.gender==1}">
                                                        <c:set value="Kisi" var="cins"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set value="Qadin" var="cins"></c:set>
                                                    </c:otherwise>
                                                </c:choose>

                                                <input type="text" value="${cins}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Father Name:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.fatherName}"></td>
                                        </tr>
                                        <tr>
                                            <td>Date Of Birth:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.birthDate}"></td>
                                        </tr>

                                        <tr>
                                            <td>E-mail:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.contact.email}"></td>
                                        </tr>

                                        <tr>
                                            <td>Class:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.grade}"></td>
                                        </tr>
                                        <tr>
                                            <td>Section:</td>
                                            <td class="font-medium text-dark-medium">
                                                <c:choose>
                                                    <c:when test="${user.section==0}">
                                                        <c:set value="Aze" var="x"></c:set>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:set value="Rus" var="x"></c:set>
                                                    </c:otherwise>
                                                </c:choose>

                                                <input type="text" value="${x}">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Address:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.contact.address}"></td>
                                        </tr>
                                        <tr>
                                            <td>Phone-mobil:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.contact.telMobile}"></td>
                                        </tr>
                                        <tr>
                                            <td>Phone-home:</td>
                                            <td class="font-medium text-dark-medium"><input type="text" value="${user.contact.telHome}"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <input type="submit" value="Save">
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Student Details Area End Here -->
            <footer class="footer-wrap-layout1">
                <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by <a href="#">PsdBosS</a></div>
            </footer>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/student-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:55 GMT -->
</html>

<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 10-Nov-19
  Time: 5:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/student-attendence.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
<jsp:include page="header.jsp"/>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->
     <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Student Attendence</h3>
                <ul>
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>Attendence</li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->
            <div class="row">
                <!-- Student Attendence Search Area Start Here -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="heading-layout1">
                                <div class="item-title">
                                    <h3>Student Attendence</h3>
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-expanded="false">...</a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="fas fa-times text-orange-red"></i>Close</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                    </div>
                                </div>
                            </div>
                            <form class="new-added-form" method="post" action="showAttendance">
                                <div class="row">
                                    <div class="col-xl-3 col-lg-6 col-12 form-group">


                                        <c:choose>
                                            <c:when test="${not empty groupList}">
                                                <label>Select Class</label>
                                                <label>
                                                    <select class="select2" name="id">
                                                        <option value="-1">Select Class</option>
                                                            <c:forEach items="${groupList}" var="group">
                                                                <option value="${group.id}">${group.name}</option>
                                                            </c:forEach>
                                                    </select>
                                                </label>
                                            </c:when>
                                        </c:choose>
                                        <label>Select Month</label>
                                        <label>
                                            <select class="select2" name="month">
                                                <option value="-1">Select Class</option>
                                                <option value="1">Yanvar</option>
                                                <option value="2">Fevral</option>
                                                <option value="3">Mart</option>
                                                <option value="4">Aprel</option>
                                                <option value="5">May</option>
                                                <option value="6">Iyun</option>
                                                <option value="7">Iyul</option>
                                                <option value="8">Avqust</option>
                                                <option value="9">Sentyabr</option>
                                                <option value="10">Oktyabr</option>
                                                <option value="11">Noyabr</option>
                                                <option value="12">Dekabr</option>
                                            </select>
                                        </label>

                                    </div>
                                    <div class="col-12 form-group mg-t-8">
                                        <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                        <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Student Attendence Search Area End Here -->
                <!-- Student Attendence Area Start Here -->
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="heading-layout1">
                                <div class="item-title">
                                    <h3>Attendence Sheet Of Class One: Section A, April 2019</h3>
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle" href="#" role="button"
                                       data-toggle="dropdown" aria-expanded="false">...</a>

                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="fas fa-times text-orange-red"></i>Close</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                        <a class="dropdown-item" href="#"><i class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table bs-table table-striped table-bordered text-nowrap">
                                    <thead>
                                    <tr>
                                        <th class="text-left">Students</th>
                                        <th>1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4</th>
                                        <th>5</th>
                                        <th>6</th>
                                        <th>7</th>
                                        <th>8</th>
                                        <th>9</th>
                                        <th>10</th>
                                        <th>11</th>
                                        <th>12</th>
                                        <th>13</th>
                                        <th>14</th>
                                        <th>15</th>
                                        <th>16</th>
                                        <th>17</th>
                                        <th>18</th>
                                        <th>19</th>
                                        <th>20</th>
                                        <th>21</th>
                                        <th>22</th>
                                        <th>23</th>
                                        <th>24</th>
                                        <th>25</th>
                                        <th>26</th>
                                        <th>27</th>
                                        <th>28</th>
                                        <th>29</th>
                                        <th>30</th>
                                        <th>31</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Michele Johnson</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Richi Akon</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Amanda Kherr</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-times text-danger"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td><i class="fas fa-check text-success"></i></td>
                                        <td>-</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Student Attendence Area End Here -->
            <footer class="footer-wrap-layout1">
                <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by <a href="#">PsdBosS</a></div>
            </footer>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Select 2 Js -->
<script src="${pageContext.servletContext.contextPath}/js/select2.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/student-attendence.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:56 GMT -->
</html>

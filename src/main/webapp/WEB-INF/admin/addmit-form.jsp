<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 17-Oct-19
  Time: 3:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/admit-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:56 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
    <div class="navbar navbar-expand-md header-menu-one bg-light">
        <div class="nav-bar-header-one">
            <div class="header-logo">
                <a href="index.html">
                    <img src="${pageContext.servletContext.contextPath}/img/logo.png" alt="logo">
                </a>
            </div>
            <div class="toggle-button sidebar-toggle">
                <button type="button" class="item-link">
                        <span class="btn-icon-wrap">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                </button>
            </div>
        </div>
        <div class="d-md-none mobile-nav-bar">
            <button class="navbar-toggler pulse-animation" type="button" data-toggle="collapse" data-target="#mobile-navbar" aria-expanded="false">
                <i class="far fa-arrow-alt-circle-down"></i>
            </button>
            <button type="button" class="navbar-toggler sidebar-toggle-mobile">
                <i class="fas fa-bars"></i>
            </button>
        </div>
        <div class="header-main-menu collapse navbar-collapse" id="mobile-navbar">
            <ul class="navbar-nav">
                <li class="navbar-item header-search-bar">
                    <div class="input-group stylish-input-group">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="flaticon-search" aria-hidden="true"></span>
                                </button>
                            </span>
                        <input type="text" class="form-control" placeholder="Find Something . . .">
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="navbar-item dropdown header-admin">
                    <a class="navbar-nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-expanded="false">
                        <div class="admin-title">
                            <h5 class="item-title">Stevne Zone</h5>
                            <span>Admin</span>
                        </div>
                        <div class="admin-img">
                            <img src="${pageContext.servletContext.contextPath}/img/figure/admin.jpg" alt="Admin">
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="item-header">
                            <h6 class="item-title">Steven Zone</h6>
                        </div>
                        <div class="item-content">
                            <ul class="settings-list">
                                <li><a href="#"><i class="flaticon-user"></i>My Profile</a></li>
                                <li><a href="#"><i class="flaticon-list"></i>Task</a></li>
                                <li><a href="#"><i class="flaticon-chat-comment-oval-speech-bubble-with-text-lines"></i>Message</a></li>
                                <li><a href="#"><i class="flaticon-gear-loading"></i>Account Settings</a></li>
                                <li><a href="login.html"><i class="flaticon-turn-off"></i>Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="navbar-item dropdown header-message">
                    <a class="navbar-nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-expanded="false">
                        <i class="far fa-envelope"></i>
                        <div class="item-title d-md-none text-16 mg-l-10">Message</div>
                        <span>5</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="item-header">
                            <h6 class="item-title">05 Message</h6>
                        </div>
                        <div class="item-content">
                            <div class="media">
                                <div class="item-img bg-skyblue author-online">
                                    <img src="${pageContext.servletContext.contextPath}/img/figure/student11.png" alt="img">
                                </div>
                                <div class="media-body space-sm">
                                    <div class="item-title">
                                        <a href="#">
                                            <span class="item-name">Maria Zaman</span>
                                            <span class="item-time">18:30</span>
                                        </a>
                                    </div>
                                    <p>What is the reason of buy this item.
                                        Is it usefull for me.....</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="item-img bg-yellow author-online">
                                    <img src="${pageContext.servletContext.contextPath}/img/figure/student12.png" alt="img">
                                </div>
                                <div class="media-body space-sm">
                                    <div class="item-title">
                                        <a href="#">
                                            <span class="item-name">Benny Roy</span>
                                            <span class="item-time">10:35</span>
                                        </a>
                                    </div>
                                    <p>What is the reason of buy this item.
                                        Is it usefull for me.....</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="item-img bg-pink">
                                    <img src="${pageContext.servletContext.contextPath}/img/figure/student13.png" alt="img">
                                </div>
                                <div class="media-body space-sm">
                                    <div class="item-title">
                                        <a href="#">
                                            <span class="item-name">Steven</span>
                                            <span class="item-time">02:35</span>
                                        </a>
                                    </div>
                                    <p>What is the reason of buy this item.
                                        Is it usefull for me.....</p>
                                </div>
                            </div>
                            <div class="media">
                                <div class="item-img bg-violet-blue">
                                    <img src="${pageContext.servletContext.contextPath}/img/figure/student11.png" alt="img">
                                </div>
                                <div class="media-body space-sm">
                                    <div class="item-title">
                                        <a href="#">
                                            <span class="item-name">Joshep Joe</span>
                                            <span class="item-time">12:35</span>
                                        </a>
                                    </div>
                                    <p>What is the reason of buy this item.
                                        Is it usefull for me.....</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="navbar-item dropdown header-notification">
                    <a class="navbar-nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-expanded="false">
                        <i class="far fa-bell"></i>
                        <div class="item-title d-md-none text-16 mg-l-10">Notification</div>
                        <span>8</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="item-header">
                            <h6 class="item-title">03 Notifiacations</h6>
                        </div>
                        <div class="item-content">
                            <div class="media">
                                <div class="item-icon bg-skyblue">
                                    <i class="fas fa-check"></i>
                                </div>
                                <div class="media-body space-sm">
                                    <div class="post-title">Complete Today Task</div>
                                    <span>1 Mins ago</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="item-icon bg-orange">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                <div class="media-body space-sm">
                                    <div class="post-title">Director Metting</div>
                                    <span>20 Mins ago</span>
                                </div>
                            </div>
                            <div class="media">
                                <div class="item-icon bg-violet-blue">
                                    <i class="fas fa-cogs"></i>
                                </div>
                                <div class="media-body space-sm">
                                    <div class="post-title">Update Password</div>
                                    <span>45 Mins ago</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="navbar-item dropdown header-language">
                    <a class="navbar-nav-link dropdown-toggle" href="#" role="button"
                       data-toggle="dropdown" aria-expanded="false"><i class="fas fa-globe-americas"></i>EN</a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">English</a>
                        <a class="dropdown-item" href="#">Spanish</a>
                        <a class="dropdown-item" href="#">Franchis</a>
                        <a class="dropdown-item" href="#">Chiness</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->
        <%--<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color">
            <div class="mobile-sidebar-header d-md-none">
                <div class="header-logo">
                    <a href="index.html"><img src="${pageContext.servletContext.contextPath}/img/logo1.png" alt="logo"></a>
                </div>
            </div>
            <div class="sidebar-menu-content">
                <ul class="nav nav-sidebar-menu sidebar-toggle-view">
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-dashboard"></i><span>Dashboard</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="index.html" class="nav-link"><i class="fas fa-angle-right"></i>Admin</a>
                            </li>
                            <li class="nav-item">
                                <a href="index3.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Students</a>
                            </li>
                            <li class="nav-item">
                                <a href="index4.html" class="nav-link"><i class="fas fa-angle-right"></i>Parents</a>
                            </li>
                            <li class="nav-item">
                                <a href="index5.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Teachers</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-classmates"></i><span>Students</span></a>
                        <ul class="nav sub-group-menu sub-group-active">
                            <li class="nav-item">
                                <a href="all-student.html" class="nav-link"><i class="fas fa-angle-right"></i>All
                                    Students</a>
                            </li>
                            <li class="nav-item">
                                <a href="student-details.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Student Details</a>
                            </li>
                            <li class="nav-item">
                                <a href="admit-form.html" class="nav-link menu-active"><i
                                        class="fas fa-angle-right"></i>Admission Form</a>
                            </li>
                            <li class="nav-item">
                                <a href="student-promotion.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Student Promotion</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i
                                class="flaticon-multiple-users-silhouette"></i><span>Teachers</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="all-teacher.html" class="nav-link"><i class="fas fa-angle-right"></i>All
                                    Teachers</a>
                            </li>
                            <li class="nav-item">
                                <a href="teacher-details.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Teacher Details</a>
                            </li>
                            <li class="nav-item">
                                <a href="add-teacher.html" class="nav-link"><i class="fas fa-angle-right"></i>Add
                                    Teacher</a>
                            </li>
                            <li class="nav-item">
                                <a href="teacher-payment.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Payment</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-couple"></i><span>Parents</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="all-parents.html" class="nav-link"><i class="fas fa-angle-right"></i>All
                                    Parents</a>
                            </li>
                            <li class="nav-item">
                                <a href="parents-details.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Parents Details</a>
                            </li>
                            <li class="nav-item">
                                <a href="add-parents.html" class="nav-link"><i class="fas fa-angle-right"></i>Add
                                    Parent</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-books"></i><span>Library</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="all-book.html" class="nav-link"><i class="fas fa-angle-right"></i>All
                                    Book</a>
                            </li>
                            <li class="nav-item">
                                <a href="add-book.html" class="nav-link"><i class="fas fa-angle-right"></i>Add New
                                    Book</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-technological"></i><span>Acconunt</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="all-fees.html" class="nav-link"><i class="fas fa-angle-right"></i>All Fees
                                    Collection</a>
                            </li>
                            <li class="nav-item">
                                <a href="all-expense.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Expenses</a>
                            </li>
                            <li class="nav-item">
                                <a href="add-expense.html" class="nav-link"><i class="fas fa-angle-right"></i>Add
                                    Expenses</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i
                                class="flaticon-maths-class-materials-cross-of-a-pencil-and-a-ruler"></i><span>Class</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="all-class.html" class="nav-link"><i class="fas fa-angle-right"></i>All
                                    Classes</a>
                            </li>
                            <li class="nav-item">
                                <a href="add-class.html" class="nav-link"><i class="fas fa-angle-right"></i>Add New
                                    Class</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="all-subject.html" class="nav-link"><i
                                class="flaticon-open-book"></i><span>Subject</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="class-routine.html" class="nav-link"><i class="flaticon-calendar"></i><span>Class
                                    Routine</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="student-attendence.html" class="nav-link"><i
                                class="flaticon-checklist"></i><span>Attendence</span></a>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-shopping-list"></i><span>Exam</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="exam-schedule.html" class="nav-link"><i class="fas fa-angle-right"></i>Exam
                                    Schedule</a>
                            </li>
                            <li class="nav-item">
                                <a href="exam-grade.html" class="nav-link"><i class="fas fa-angle-right"></i>Exam
                                    Grades</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="transport.html" class="nav-link"><i
                                class="flaticon-bus-side-view"></i><span>Transport</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="hostel.html" class="nav-link"><i class="flaticon-bed"></i><span>Hostel</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="notice-board.html" class="nav-link"><i
                                class="flaticon-script"></i><span>Notice</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="messaging.html" class="nav-link"><i
                                class="flaticon-chat"></i><span>Messeage</span></a>
                    </li>
                    <li class="nav-item sidebar-nav-item">
                        <a href="#" class="nav-link"><i class="flaticon-menu-1"></i><span>UI Elements</span></a>
                        <ul class="nav sub-group-menu">
                            <li class="nav-item">
                                <a href="notification-alart.html" class="nav-link"><i class="fas fa-angle-right"></i>Alart</a>
                            </li>
                            <li class="nav-item">
                                <a href="button.html" class="nav-link"><i class="fas fa-angle-right"></i>Button</a>
                            </li>
                            <li class="nav-item">
                                <a href="grid.html" class="nav-link"><i class="fas fa-angle-right"></i>Grid</a>
                            </li>
                            <li class="nav-item">
                                <a href="modal.html" class="nav-link"><i class="fas fa-angle-right"></i>Modal</a>
                            </li>
                            <li class="nav-item">
                                <a href="progress-bar.html" class="nav-link"><i class="fas fa-angle-right"></i>Progress Bar</a>
                            </li>
                            <li class="nav-item">
                                <a href="ui-tab.html" class="nav-link"><i class="fas fa-angle-right"></i>Tab</a>
                            </li>
                            <li class="nav-item">
                                <a href="ui-widget.html" class="nav-link"><i
                                        class="fas fa-angle-right"></i>Widget</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="map.html" class="nav-link"><i
                                class="flaticon-planet-earth"></i><span>Map</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="account-settings.html" class="nav-link"><i
                                class="flaticon-settings"></i><span>Account</span></a>
                    </li>
                </ul>
            </div>
        </div>--%>
        <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Students</h3>
                <ul>
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>Student Admit Form</li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->
            <!-- Admit Form Area Start Here -->
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>Add New Students</h3>
                        </div>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                               aria-expanded="false">...</a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-times text-orange-red"></i>Close</a>
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-cogs text-dark-pastel-green"></i>Edit</a>
                                <a class="dropdown-item" href="#"><i
                                        class="fas fa-redo-alt text-orange-peel"></i>Refresh</a>
                            </div>
                        </div>
                    </div>
                    <form:form action="addmit" method="post" modelAttribute="studentForm" class="new-added-form" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-xl-3 col-lg-6 col-12 form-group">


                                <label>First Name *</label>
                                <form:errors path="name" cssStyle="color: red"/>
                                <form:input path="name" type="text" placeholder="" class="form-control"/>

                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">

                                <label>Last Name *</label>
                                <form:errors path="surname" cssStyle="color: red"/>
                                <form:input path="surname" type="text" placeholder="" class="form-control"/>

                            </div>

                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Father Name</label>
                                <form:errors path="fatherName" cssStyle="color: red"/>
                                <form:input path="fatherName" type="text" placeholder="" class="form-control"/>
                            </div>

                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Date Of Birth *</label>
                                <form:errors path="birthDate" cssStyle="color: red"/>
                                <form:input  path="birthDate" type="text" placeholder="dd/mm/yyyy" class="form-control air-datepicker"
                                       data-position='bottom right'/>
                                <i class="far fa-calendar-alt"></i>
                            </div>


                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Gender *</label>
                                <form:select path="gender" class="select2">
                                    <option value="-1">Please Select Gender *</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                </form:select>

                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Category *</label>
                                <form:errors path="category" cssStyle="color: red"/>
                                <form:select path="category" class="select2" >
                                    <option value="">Please Select Category *</option>
                                    <form:options items="${categoryMap}" />
                                </form:select>
                            </div>



                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Subject *</label>
                                <form:errors path="subjectList" cssStyle="color: red"/>
                                <form:select path="subjectList" class="select2" multiple="true">
                               <%--     <option value="">Please Select Subject *</option>--%>
                                    <form:options items="${subjectMap}" />
                                 <%--   <option value="1">Islam</option>
                                    <option value="2">Hindu</option>
                                    <option value="3">Christian</option>
                                    <option value="3">Buddish</option>
                                    <option value="3">Others</option>--%>
                                </form:select>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>E-Mail</label>
                                <form:errors path="contact.email" cssStyle="color: red"/>
                                <form:input path="contact.email" type="email" placeholder="" class="form-control"/>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Class *</label>
                                <form:errors path="grade" cssStyle="color: red;"/>
                                <form:select path="grade" class="select2">
                                    <option value="">Please Select Class *</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                </form:select>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Section *</label>
                                <form:errors path="section" cssStyle="color: red"/>
                                <form:select path="section" class="select2">
                                    <option value="-1">Please Select Section *</option>
                                    <option value="1">Az</option>
                                    <option value="2">Rus</option>

                                </form:select>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>PIN Code</label>
                                <form:errors path="fincod" cssStyle="color: red"/>
                                <form:input path="fincod" type="text" placeholder="" class="form-control"/>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">


                                <label>Password *</label>
                              <%--  <form:errors path="password" cssStyle="color: red"/>--%>
                                <form:input path="password" type="text" placeholder="" class="form-control" readonly="true"/>

                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>Phone</label>
                                <form:errors path="contact.telMobile" cssStyle="color: red"/>
                                <form:input path="contact.telMobile" type="text" placeholder="" class="form-control"/>
                            </div>
                            <div class="col-xl-3 col-lg-6 col-12 form-group">
                                <label>School</label>
                                <form:errors path="school" cssStyle="color: red"/>
                                <form:input path="school" type="text" placeholder="" class="form-control"/>
                            </div>
                            <div class="col-lg-6 col-12 form-group">
                                <label>Address</label>
                                <form:errors path="contact.address" cssStyle="color: red"/>
                                <form:textarea path="contact.address"  class="textarea form-contro" name="message" id="form-message" cols="20"
                                               rows="2"></form:textarea>
                            </div>
                            <div class="col-lg-6 col-12 form-group">
                                <label>Short BIO</label>
                                <form:textarea path="additionalInform" class="textarea form-control" name="message" id="form-message" cols="10"
                                          rows="9"></form:textarea>
                            </div>
                            <div class="col-lg-6 col-12 form-group mg-t-30">
                                <label class="text-dark-medium">Upload Student Photo (150px X 150px)</label>
                                <input   type="file" class="form-control-file" name="file"/>
                            </div>
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                                <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
            <!-- Admit Form Area End Here -->
           <%-- <footer class="footer-wrap-layout1">
                <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by <a
                        href="#">PsdBosS</a></div>
            </footer>--%>
            <jsp:include page="footer.jsp"/>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Select 2 Js -->
<script src="${pageContext.servletContext.contextPath}/js/select2.min.js"></script>
<!-- Date Picker Js -->
<script src="${pageContext.servletContext.contextPath}/js/datepicker.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/admit-form.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:09:56 GMT -->
</html>

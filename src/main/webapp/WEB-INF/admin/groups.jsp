<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 07-Nov-19
  Time: 3:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html class="no-js" lang="">


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/messaging.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:17 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <jsp:include page="meta.jsp"/>
</head>

<body>
<!-- Preloader Start Here -->
<div id="preloader"></div>
<!-- Preloader End Here -->
<div id="wrapper" class="wrapper bg-ash">
    <!-- Header Menu Area Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header Menu Area End Here -->
    <!-- Page Area Start Here -->
    <div class="dashboard-page-one">
        <!-- Sidebar Area Start Here -->
        <jsp:include page="menu.jsp"/>
        <!-- Sidebar Area End Here -->
        <div class="dashboard-content-one">
            <!-- Breadcubs Area Start Here -->
            <div class="breadcrumbs-area">
                <h3>Messaging</h3>
                <ul>
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>Compose Message</li>
                </ul>
            </div>
            <!-- Breadcubs Area End Here -->

            <c:choose>
                <c:when test="${not empty groupList}">
                    <div class="row">
                        <c:forEach items="${groupList}" var="group">



                            <div class="col-lg-4" >
                                <h4 class="text-headline margin-v-0-10"><a
                                        href="/admin/viewGroup?id=${group.id}">Qrup adi:${group.name} <br/>
                                    Muellim :${group.teacherFullName} <br/> Otaq:${group.room.name}</a>
                                </h4>
                                <div class="ui-grid-box" style="background-color:orange;height: 200px;width: 300px;border-radius: 5%;">




                                    <table border="1" style="text-align: center;width: 100%;font-size: 20px; color: white;" >
                                        <tr style=" color: #0a0a0a;text-align: center; height: 35px">
                                            <th style="text-align: center">Gun</th>
                                            <th style="text-align: center">Baslama saati</th>
                                            <th style="text-align: center">Bitme saati</th>
                                        </tr>

                                        <c:forEach items="${group.timeTables}" var="timeTable">
                                            <tr style="text-align: center;width: 100%;font-size: 20px;color: #0a0a0a;height: 25px;">
                                                <td>${timeTable.weekDay}</td>
                                                <td>${timeTable.startTime}</td>
                                                <td>${timeTable.endTime}</td>
                                            </tr>
                                        </c:forEach>

                                    </table>



                                </div>

                            </div>


                        </c:forEach>
                    </div>




                </c:when>
            </c:choose>

            <footer class="footer-wrap-layout1">
                <div class="copyright">© Copyrights <a href="#">akkhor</a> 2019. All rights reserved. Designed by
                    <a href="#">PsdBosS</a></div>
            </footer>
        </div>
    </div>
    <!-- Page Area End Here -->
</div>
<!-- jquery-->
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.3.1.min.js"></script>
<!-- Plugins js -->
<script src="${pageContext.servletContext.contextPath}/js/plugins.js"></script>
<!-- Popper js -->
<script src="${pageContext.servletContext.contextPath}/js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<!-- Select 2 Js -->
<script src="${pageContext.servletContext.contextPath}/js/select2.min.js"></script>
<!-- Scroll Up Js -->
<script src="${pageContext.servletContext.contextPath}/js/jquery.scrollUp.min.js"></script>
<!-- Custom Js -->
<script src="${pageContext.servletContext.contextPath}/js/main.js"></script>

</body>


<!-- Mirrored from www.radiustheme.com/demo/html/psdboss/akkhor/akkhor/messaging.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 16 Oct 2019 13:10:17 GMT -->
</html>

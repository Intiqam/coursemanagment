package edu.coursemanagment.view;

import edu.coursemanagment.domain.DataTableUser;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class DataTableExcelView extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook,
                                      HttpServletRequest httpServletRequest,
                                      HttpServletResponse httpServletResponse) throws Exception {

        List<DataTableUser> dataTableUserList = (List<DataTableUser>) model.get("dataTableUserList");

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddhhmmss");
        String filename = String.format("student-list-%s.xls", formatter.format(now));
        httpServletResponse.setHeader("Content-Disposition", "filename=\"" + filename + "\"");

        Sheet sheet = workbook.createSheet("student list");
        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 14);
        font.setFontName("Arial");
        headerStyle.setFont(font);
        headerStyle.setFillForegroundColor((short) 0);
        header.setHeightInPoints(24);

        sheet.setDefaultColumnWidth(20);
        Cell no = header.createCell(0);
        no.setCellValue("No");
        no.setCellStyle(headerStyle);

        /* private int id;
    private String name;
    private String surname;
    private String email;
    private String mobile1;*/



        Cell name = header.createCell(1);
        name.setCellValue("Ad");
        name.setCellStyle(headerStyle);


        Cell surname = header.createCell(2);
        surname.setCellValue("Soyad");
        surname.setCellStyle(headerStyle);

        Cell email = header.createCell(6);
        email.setCellValue("Email");
        email.setCellStyle(headerStyle);

        Cell mobil = header.createCell(7);
        mobil.setCellValue("Mobil");
        mobil.setCellStyle(headerStyle);

        for (int i = 0; i < dataTableUserList.size(); i++) {
            Row row = sheet.createRow(i + 1);

            Cell c1 = row.createCell(0, CellType.NUMERIC);
            c1.setCellValue(i + 1);

            Cell c2 = row.createCell(1, CellType.STRING);
            c2.setCellValue(dataTableUserList.get(i).getName());

            Cell c3 = row.createCell(2, CellType.STRING);
            c3.setCellValue(dataTableUserList.get(i).getSurname());


            Cell c4 = row.createCell(3, CellType.STRING);
            c4.setCellValue(dataTableUserList.get(i).getEmail());

            Cell c5 = row.createCell(4, CellType.STRING);
            c5.setCellValue(dataTableUserList.get(i).getMobile1());

        }

    }
}

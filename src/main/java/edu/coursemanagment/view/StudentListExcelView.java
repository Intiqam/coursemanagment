/*
package edu.coursemanagment.coursemanagment.view;


import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class StudentListExcelView extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest httpServletRequest,
                                      HttpServletResponse httpServletResponse) throws Exception {

        List<StudentForm> studentList = (List<StudentForm>) model.get("studentFormList");

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddhhmmss");
        String filename = String.format("student-list-%s.xls", formatter.format(now));
        httpServletResponse.setHeader("Content-Disposition", "filename=\"" + filename + "\"");

        Sheet sheet = workbook.createSheet("student list");
        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) 14);
        font.setFontName("Arial");
        headerStyle.setFont(font);
        headerStyle.setFillForegroundColor((short) 0);
        header.setHeightInPoints(24);

        sheet.setDefaultColumnWidth(20);
        Cell no = header.createCell(0);
        no.setCellValue("No");
        no.setCellStyle(headerStyle);
        */
/*u.surname,u.father_name,u.school, c.address , c.email,c.tel_mobil*//*

        //  id
        // ad
        // soyad
        // ata adi
        //school
        //address
        //email
        //telmobil


        Cell name = header.createCell(1);
        name.setCellValue("Ad");
        name.setCellStyle(headerStyle);


        Cell surname = header.createCell(2);
        surname.setCellValue("Soyad");
        surname.setCellStyle(headerStyle);

        Cell father_name = header.createCell(3);
        father_name.setCellValue("Ata Adi");
        father_name.setCellStyle(headerStyle);

        Cell school = header.createCell(4);
        school.setCellValue("Mekteb");
        school.setCellStyle(headerStyle);


        Cell address = header.createCell(5);
        address.setCellValue("Unvan");
        address.setCellStyle(headerStyle);

        Cell email = header.createCell(6);
        email.setCellValue("Email");
        email.setCellStyle(headerStyle);

        Cell mobil = header.createCell(7);
        mobil.setCellValue("Mobil");
        mobil.setCellStyle(headerStyle);

        for (int i = 0; i < studentList.size(); i++) {
            Row row = sheet.createRow(i + 1);

            Cell c1 = row.createCell(0, CellType.NUMERIC);
            c1.setCellValue(i + 1);

            Cell c2 = row.createCell(1, CellType.STRING);
            c2.setCellValue(studentList.get(i).getName());

            Cell c3 = row.createCell(2, CellType.STRING);
            c3.setCellValue(studentList.get(i).getSurname());

            Cell c4 = row.createCell(3, CellType.STRING);
            c4.setCellValue(studentList.get(i).getFatherName());

            Cell c5 = row.createCell(4, CellType.STRING);
            c5.setCellValue(studentList.get(i).getSchool());

            Cell c6 = row.createCell(5, CellType.STRING);
            c6.setCellValue(studentList.get(i).getSchool());

            Cell c7 = row.createCell(6, CellType.STRING);
            c7.setCellValue(studentList.get(i).getContact().getAddress());

            Cell c8 = row.createCell(7, CellType.STRING);
            c8.setCellValue(studentList.get(i).getContact().getEmail());

            Cell c9 = row.createCell(8, CellType.STRING);
            c9.setCellValue(studentList.get(i).getContact().getTelMobile());

        }

    }
}
*/

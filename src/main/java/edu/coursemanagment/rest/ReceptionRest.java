package edu.coursemanagment.rest;


import edu.coursemanagment.domain.DataTableResult;
import edu.coursemanagment.domain.DataTableUser;
import edu.coursemanagment.domain.User;
import edu.coursemanagment.service.StudentService;
import edu.coursemanagment.view.DataTableExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/reception-rest")
public class ReceptionRest {
    @Autowired
    StudentService studentService;
    int start1;
    int length1;

   /* @GetMapping("/")
    public DataTableResult getStudentListAjax(@RequestParam("draw") int draw,
                                              @RequestParam("start") int start,
                                              @RequestParam("length") int length) {
        DataTableResult dataTableResult = new DataTableResult();
        try {
            start1 = start;
            length1 = length;
            dataTableResult = studentService.getStudentListAjax(draw, start, length);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "sagird siyahi bosdur");
        }


        return dataTableResult;
    }*/

    @GetMapping("/excel")
    public ModelAndView exportStudentListToExcel() {
        List<User> studentFormList = studentService.getStudentLisByStatus();
        List<DataTableUser> dataTableUserList = studentService.getStudentListAjax(length1,start1);
        ModelAndView mav = new ModelAndView(new DataTableExcelView());
        mav.addObject("dataTableUserList", dataTableUserList);
        return mav;
    }

    @GetMapping("/days/{roomId}/{checkday}")
    public List<LocalTime> emptyTimes(@PathVariable(name = "roomId") long roomId,
                                      @PathVariable(name = "checkday") long day) {
        studentService.getFreeTimesByRoomId(roomId, day);

        return null;
    }
}

package edu.coursemanagment.rest;

import java.util.Collections;
import java.util.List;

import edu.coursemanagment.domain.DataTable;
import edu.coursemanagment.domain.User;
import edu.coursemanagment.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/rest")
public class AdminRest {
    @Autowired
    private StudentService studentService;

    @GetMapping("/students")
    public List<User> allStudents(){
       List<User> userList= Collections.emptyList();
       userList=studentService.getStudentLisByStatus();
       if (userList.isEmpty()){
           throw new ResponseStatusException(HttpStatus.NOT_FOUND,"list is empty");
       }else {

           return userList;
       }
    }



    @GetMapping("/excel")
    public ModelAndView exportStudentListToExcel(){
        List<User>studentList=studentService.getStudentLisByStatus();
        ModelAndView mav = new ModelAndView(new StudentListExcelView());
        mav.addObject("studentList",studentList);
        return mav;
    }
    @GetMapping("/datatable")
    public DataTable getPersonList(
            @RequestParam(name = "draw", required = false, defaultValue = "0") int draw,
            @RequestParam(name = "start", required = false, defaultValue = "0") int start,
            @RequestParam(name = "length", required = false, defaultValue = "10") int length,
            @RequestParam(name = "search[value]", required = false, defaultValue = "") String search,
            @RequestParam(name = "order[0][column]", required = false, defaultValue = "0") int sortColumn,
            @RequestParam(name = "order[0][dir]", required = false, defaultValue = "asc") String sortDirection
    ) {
        DataTable dataTable = new DataTable(draw, start, length, sortColumn, sortDirection,search);
        System.out.println("rest/datatable"+dataTable);

        return  studentService.getPersonDataTable(dataTable);
    }
}

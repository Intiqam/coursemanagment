package edu.coursemanagment.repository;


import edu.coursemanagment.domain.*;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

public interface StudentRepository {
    User addStudent(User user);

    List<Subject> getSubjectList();

    List<Group> getGroupBySubjectId(long subjectId);

    List<TimeTable> getGroupsTimeTablesBySubjectId(long subjectId);

    List<TimeTable> getTimeTablesByGroupId(long groupId);

    List<User> getTeachersBySubjectId(long subject_id);

    List<TimeTable> getEmptyTimesByRoomId(long roomId);

    List<Room> getRooms();

    List<User> getStudentBySubjectId(long subject_id);

    List<User> getStudentByGroupId(long groupId);

    List<LocalTime> getFreeTimesByRoomId(long roomId, long weekday);

    List<DataTableUser> getStudentListAjax(int start, int length);

    void addAttdendance(List<Attandance> attandanceList, long group_id);

    int getStudentCount();

    int getTeacherCount();

    void addGroup(Group group);

    Optional<User> getStudentById(long id);

    List<Subject> getSubjectListByStudentId(long id);

    void updateStudent(User user);



    List<Role> getRoleListByUsername(long userId);

    Optional<User> getUserByUsername(String username);

    List<User> getStudentLisByStatus();

    void addNewSubject(Subject subject);

    Optional<Subject> getSubjectById(long id);

    void deleteSubjectById(long id);

    Optional<User> getUserByEmail(String email);


    List<Category> getCategories();
    List<User> getTeachers();

    List<Attandance> getAttendanceList(Long groupId);
    List<User> getUserListPaging(String sql,int start, int length, String search);

    List<Group> getAllGroups();


}

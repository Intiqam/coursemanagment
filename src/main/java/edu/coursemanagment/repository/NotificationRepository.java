package edu.coursemanagment.repository;



import edu.coursemanagment.domain.Notification;
import edu.coursemanagment.domain.NotificationType;

import java.util.List;

public interface NotificationRepository {
   void  insertNotification(Notification notification);
   List<Notification> getPendingNotificatios(NotificationType notificationType, int limit);
   void updateNotification(Notification notification);
}

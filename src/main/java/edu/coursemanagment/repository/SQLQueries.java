package edu.coursemanagment.repository;


public class SQLQueries {


    public static final String addStudent = " insert into  user(name,surname,father_name,gender,section,fincod,grade,school, password,username , category_id ) " +
            "values(?,?,?,?,?,?,?,?,?,?,?) ";

    public static final String addStudentSubject = " insert into student_subject(student_id,subject_id) values (?,?) ";

    public static final String getSubjectList = " select id,name,price from subject where status=1  ";

    public static final String GET_GROUPS_BY_SUBJECT_ID = " select g.id,g.name as group_name ,r.name  as room_name,  concat(t.name ,' ', t.surname)  as fullname " +
            " from `group`  g join room r on g.room_id=r.id and r.status=1 " +
            " join user t on  g.teacher_id=t.id and t.status=1 and g.status=1 where g.subject_id=? ";

    public static final String GET_GROUPS_TIME_TABLE_BY_SUBJECT_ID=" select tt.id,r.id as room_id,r.name as room_name  , g.id as group_id ,g.name as group_name,weekdays,start_time,end_time from time_table tt join room r on tt.room_id=r.id and r.status=1  " +
            " join `group` g on g.id=tt.group_id and g.status=1 " +
            " join subject s on s.id=g.subject_id where s.id=? ";

    public static final String GET_TIME_TABLE_BY_GROUP_ID=" select tt.id, r.id as room_id, r.name as room_name  , " +
            "   g.id as group_id, g.name as group_name, weekdays, start_time, end_time " +
            " from time_table tt join room r on tt.room_id=r.id and r.status=1  " +
            " join `group` g on g.id=tt.group_id and g.status=1 " +
            " where g.id=? ";

    public static final String GET_TEACHERS_BY_SUBJECT_ID="  SELECT u.id,u.name,u.surname,u.father_name, null as gender,null as password,null as section ,null as grade,dob,null as unvan,null as   email,null as tel_mobil,null as tel_home FROM teacher_subject ts join user u on u.id=ts.user_id and  ts.status=1 \n" +
            "                  join subject s on s.id=ts.subject_id and s.status=1 where s.id=?  ";


    public static final String GET_EMPTY_TIMES_BY_ROOM_ID = "  SELECT tt.weekdays, tt.start_time,tt.end_time,g.id group_id,g. name group_name " +
            "  FROM coursemanagment.time_table tt " +
            "  join `group` g  where tt.room_id=? ";

    public static final String GET_ROOMS = " SELECT id,name from room  ";

    public static final String GET_STUDENT_LIST_BY_SUBJECT_ID = "select u.id, u.name ,u.surname, null as father_name ,null as password , null as gender,null as section ,null as unvan,null as   email,null as tel_mobil,null as tel_home,null as grade,null as dob from student_subject s " +
            "join user u on s.student_id=u.id " +
            "where subject_id=? and type=0 ";


    public static final String ADD_GROUP =" insert into `group` (name,subject_id,room_id,branch_id,teacher_id) values (?,?,?,1,?) ";


        public static final String ADD_GROUP_MEMMBERS=" insert into group_member(group_id,student_id,branch_id) values(? ,? ,2) ";

         public static final String UPDATE_STUDENT_SUBJECT_TYPE=" update student_subject set type=1 where student_id= ? ";

        public static final String ADD_STUDENT_SUBJECT= "    insert into student_subject (student_id,subject_id) values(?, ?)  ";


        public static  final String ADD_CONTACT = " insert into contact (tel_home,tel_mobil,address,email,user_id)  " +
           "  values(?,?,?,?,?)   ";

        public static final String ADD_TIME_TABLE= " insert into time_table(room_id,group_id,weekdays,start_time,end_time) values(?,?,?,?,?) ";

            public static final String GET_STUDENT_LIST_BY_GROUP_ID="select gm.id  id, u.name name , u.surname surname ," +
                    " u.father_name father_name,  null as password , null as gender,null as section , " +
                    "null as unvan,null as   email,null as tel_mobil,null as tel_home," +
                    "null as grade,null as dob  from group_member gm  " +
           "  join user u on gm.student_id=u.id  "  +
           "  left join contact c on c.user_id=u.id  " +
           "  where group_id=? and gm.status = 1  ";


            public static final String INSERT_USER_ROLE=" insert into user_role(user_id , role_id) values(? ,?) " ;

        public static final String INSERT_TOKEN=" insert into token(userid ,value,type) values(?,? ,?) ";

        public static final String INSERT_NOTIFICATION= " insert into notification(`from`, `to`,subject,content,source_system ) values( ?,?,?, ?,?)  ";


            public  static  final String INSERT_ATTDENDACE=" insert into attandance(attandance,groupmember_id,group_id) " +
                    "values(?,?,?);";


    public static final String GET_FREE_TIMES_BY_ROOM_ID = " SELECT distinct(start_time),weekdays FROM coursemanagment.time_table " +
            " where room_id= ? and weekdays=? " +
            " order by start_time ";
    public static final String GET_PENDING_NOTIFICATIONS_BY_PENDING = " select id, `from`,`to`,type, content,subject," +
            " source_system, process_status, log_data, idate, udate " +
            " from notification " +
            " where type = ? and process_status = 0 and status = 1 " +
            " order by id" +
            " limit ?";

    public static final String UPDATE_NOTIFICATION ="update notification " +
            " set process_status = ?, udate = current_timestamp(), log_data = ? " +
            " where id = ? and status = 1" ;
    public static final String GET_STUDENT_LIST_AJAX=" select id,name,surname,father_name from user " +
            " where category_id<10 " +
            " order by id " +
            " limit ? offset ?  ";

    public static final String GET_SUBJECT_LIST_BY_STUDENT_ID="  select s.id,s.name,s.price from  student_subject ss  " +
            "  join user u on ss.student_id=u.id   " +
            "  join subject s on s.id=ss.subject_id   " +
            "  where u.id=? ";


    public static final String GET_STUDENT_COUNT= "  select count(*) studentCount from user " +
            "  where category_id < 10 and status=1 ";

    public static final String GET_TEACHER_COUNT= "  select count(*) studentCount from user " +
            "  where category_id = 10 and status=1 ";

    public static final String GET_STUDENT_BY_ID="  select u.id ,u.name,u.surname,u.father_name,u.gender,u.section,u.fincod,u.grade,u.school,u.diplom_no, ca.id as caId ,c.id as contactId, " +
            " c.email,c.address as unvan ,c.tel_home,c.tel_mobil,u.dob,null as password" +
            "   from user u " +
            " left join contact c on c.user_id=u.id " +
            " join category ca on ca.id=u.category_id  " +
            " where u.id = ?  ";

    public static  final String UPDATE_STUDENT_BY_ID=" update   user set name=?,surname=?,father_name=?, " +
            " gender=?,section=?,fincod=?,grade=?,school=?,category_id=?, " +
            " udate=current_timestamp() where id=?  ";


    public static  final String UPDATE_CONTACT="  update contact set tel_home=?,tel_mobil=?,address=?,email=?,udate=current_timestamp() where user_id=? ";
    public static final String GET_LAST_USERNAME=" select max(username) from user ";

    public static  final String UPDATE_STUDENT_SUBJECT=" update student_subject set subject_id=? ," +
            " udate=current_timestamp() where student_id=? ";

    public  static  final String GET_STUDENT_LIST_BY_STATUS =" select u.id,null as password, u.name,u.surname,u.father_name, " +
            " u.school,   gender, section,null as fincod, grade, " +
            " null as caId,null as ad, null as tel_home , dob ,c.address as unvan, c.email,c.tel_mobil " +
            " from user  u  " +
            " left join contact c on c.user_id=u.id " +
            " where u.status= 1 and u.category_id<10  ";


    public static final String ADD_MEDIA = "      insert into media(user_id,original_file_name, file_name,main,  video, mobile,mime_type, file_length, status ) " +
            "                   values(?, ?, ? ,?, ?, ?, ?, ?,1)  ";

    public static final String GET_MEDIA_BY_ID = " select m.id, m.user_id, m.original_file_name, m.file_name, m.main ,m.mime_type, m.video, m.mobile, m.file_length  " +
            "                  from media m   " +
            "                  join user u on u.id=m.user_id  " +
            "                   where m.status = 1 and u.id = ? ";


    public static final String GET_ROLELIST_BY_USERNAME=" select r.id,r.succespage,r.name from user u  " +
            " join user_role ur on ur.user_id = u.id  " +
            " join role r on r.id=ur.role_id " +
            " where u.id = ? " +
            " order by r.priority desc ";

    public static final String GET_USER_BY_USERNAME="  select *, null as unvan ,null as tel_mobil,null as tel_home, null as email from user " +
            " where username=? ";

    public static final String ADD_NEW_SUBJECT=" insert into subject (name,price) values (?, ?)  ";

    public static final String GET_SUBJECT_BY_ID=" select id,name,price " +
            "  from subject where id =?  ";

    public static final String DELETE_SUBJECT_BY_ID="  update subject set status =0  " +
            "  where id=? ";

    public static final String GET_USER_BY_EMAIL ="select u.id , u.name,u.surname,u.father_name,u.password,  " +
            "c.email,c.address,c.tel_mobil,c.tel_home from user u " +
            "left join  contact c on c.user_id=u.id " +
            "where c.email = ? and u.status= 1 " ;

    public static final String GET_CATEGORIES=  " SELECT id,name from category where status=1" ;


public static final String GET_TEACHERS=" select s.name,u.id,u.name,u.surname,u.father_name, gender,section,fincod, diplom_no,u.idate hire_date,dob,username " +
        "           ,c.address , c.email,c.tel_mobil ,password " +
        "             from user  u   join teacher_subject ts on ts.user_id=u.id  " +
        "             " +
        "               join subject s on ts.subject_id-s.id " +
        "        join contact c on c.user_id=u.id " +
        "             where u.status= 1 and u.category_id=10  ";


public static final String GET_ATTENDANCE_LIST=" select a.groupmember_id,a.attandance,a.date from attandance a  " +
        "    join `group` g on g.id=a.group_id and a.status=1 " +
        "    join group_member gm on a.groupmember_id=gm.id and gm.status=1 and g.status=1 " +
        "    where a.group_id=? ";

public static final String GET_STUDENTLIST_WITH_PAGING=" select null as unvan, null as password,null as email,null as tel_home, u.id,u.name,u.surname,u.father_name,u.gender,u.grade,u.section,u.dob,c.tel_mobil from user u  join contact c " +
        " on u.id=c.user_id " +
        "  where concat(u.name,' ',u.surname,' ',u.father_name) like ? and u.category_id<10 and u.status=1   " +
        "  order by {order} {direction} limit ?,?  ";

        public static final String GET_ALL_GROUPS=" select id,name as group_name ,null as fullname ,null as room_name from `group` where status =1 ";

}

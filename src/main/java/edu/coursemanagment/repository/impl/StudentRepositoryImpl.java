package edu.coursemanagment.repository.impl;


import edu.coursemanagment.domain.*;
import edu.coursemanagment.repository.NotificationRepository;
import edu.coursemanagment.repository.SQLQueries;
import edu.coursemanagment.repository.StudentRepository;
import edu.coursemanagment.util.SecurityUtil;

import edu.coursemanagment.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    NotificationRepository notificationRepository;

    private SubjectRowMapper subjectRowMapper = new SubjectRowMapper();
    private GroupRowMapper groupRowMapper = new GroupRowMapper();
    private TimeTableMapRow timeTableMapRow = new TimeTableMapRow();
    private UserRowMapper userRowMapper = new UserRowMapper();
    private RoomRowMapper roomRowMapper = new RoomRowMapper();
    private TimeRowMapper timeRowMapper = new TimeRowMapper();
    private DataTableUserRowMapper dataTableUserRowMapper = new DataTableUserRowMapper();
    private UserRowmapperById userRowmapperById = new UserRowmapperById();
    private RoleMapperByUsername roleMapperByUsername = new RoleMapperByUsername();
    private CategoriesRowMapper categoriesRowMapper = new CategoriesRowMapper();
    private StudentRowMapper studentRowMapper = new StudentRowMapper();
    private TeacherRowMapper teacherRowMapper = new TeacherRowMapper();

    @Autowired
    private Notification notification;
//    private StudentRowMapper studentRowMapper = new StudentRowMapper();


    @Value("${coursemanagment.mail.activation.template}")
    private String template;
    @Value("${coursemanagment.mail.activation.link}")
    private String link;
    @Value("${coursemanagment.mail.activation.source}")
    private String source;
    @Value("${coursemanagment.mail.activation.subject}")
    private String subject;
    @Value("${spring.datasource.username}")
    private String from;


    @Override
    public Optional<User> getStudentById(long id) {
        Object[] objects = new Object[]{id};
        Optional<User> optionalStudentForm = Optional.empty();
        List<User> studentFormList = jdbcTemplate.query(SQLQueries.GET_STUDENT_BY_ID, objects, userRowMapper);
        if (!studentFormList.isEmpty()) {
            optionalStudentForm = Optional.of(studentFormList.get(0));
        }
        return optionalStudentForm;
    }

    @Override
    @Transactional
    public void addGroup(Group group) {
        boolean flag = true;
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            int count = jdbcTemplate.update(new PreparedStatementCreator() {

                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    //  insert into `group` (name,subject_id,room_id,branch_id,teacher_id) values (?,?,?,1,?) ";

                    PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.ADD_GROUP, Statement.RETURN_GENERATED_KEYS);
                    preparedStatement.setString(1, group.getName());
                    // group.setSubject(new Subject());
                    preparedStatement.setLong(2, group.getSubject().getId());
                    preparedStatement.setLong(3, group.getRoom().getId());
                    preparedStatement.setLong(4, group.getTeacherId());
                    return preparedStatement;
                }

            }, keyHolder);
            group.setId(keyHolder.getKey().longValue());
            if (count == 0) {
                flag = false;
            } else {
                int studentCount = group.getStudentId().length;
                //           insert into group_member(group_id,student_id,branch_id) values(? ,? ,2) ";
                for (int i = 0; i < studentCount; i++) {
                    Object[] objects = new Object[]{group.getId(), group.getStudentId()[i]};
                    int result = jdbcTemplate.update(SQLQueries.ADD_GROUP_MEMMBERS, objects);
                    if (result == 0) {
                        flag = false;
                        System.out.println("AddGroup memmbers zamani error bas verdi");
                        break;
                    }
                }
                if (flag) {
                    //    update student_subject set type=1 where student_id= ?
                    for (int i = 0; i < studentCount; i++) {
                        Object[] objects = new Object[]{group.getStudentId()[i]};
                        jdbcTemplate.update(SQLQueries.UPDATE_STUDENT_SUBJECT_TYPE, objects);
                    }
                    //insert into time_table(room_id,group_id,weekdays,start_time,end_time)
                    // values(1,15,'5','14:00','16:00');
                    for (int i = 0; i < group.getTimeTables().size(); i++) {
                        Object[] args = new Object[]{group.getRoom().getId(), group.getId(), group.getTimeTables().get(i).getWeekDay(),
                                group.getTimeTables().get(i).getStartTime(), group.getTimeTables().get(i).getEndTime()};
                        jdbcTemplate.update(SQLQueries.ADD_TIME_TABLE, args);
                    }


                } else {
                    flag = false;
                    System.out.println("Update student zamamini xeta bas verdi");
                }


            }


        } catch (Exception e) {
            System.out.println("Tranzaksiya zamani xeta bas verdi");
            System.out.println(e.getMessage());
        }
    }


    @Override
    @Transactional
    public User addStudent(User user) throws RuntimeException {
        System.out.println("StuRepoImpl ");
        boolean flag = true;

        /* try {*/
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                /*
                 * name,surname,father_name,gender,section,fincod,grade,school, password,username
                 * */
                PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.addStudent, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getSurname());
                preparedStatement.setString(3, user.getFatherName());
                preparedStatement.setInt(4, user.getGender());
                preparedStatement.setInt(5, user.getSection());
                preparedStatement.setString(6, user.getFincod());
                preparedStatement.setString(7, user.getGrade());
                preparedStatement.setString(8, user.getSchool());
//                preparedStatement.setString(9, user.getDiplomNo());
//                preparedStatement.setLong(10, user.getCategory().getId());
//                preparedStatement.setLong(11, 1);
                preparedStatement.setString(9, user.getPassword());

                preparedStatement.setString(10, (user.getUsername()));
                preparedStatement.setLong(11, (user.getCategory().getId()));
             /*  DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-dd-MM");
                LocalDate localDate = LocalDate.parse(user.getBirthDate(), format);
                System.out.println("Dogum tarixin  "+localDate);
                preparedStatement.setDate(12, java.sql.Date.valueOf(localDate));*/
                return preparedStatement;
            }
        }, keyHolder);

        user.setId(keyHolder.getKey().longValue());
        System.out.println("User id  "+user.getId());
        if (count == 1) {
            if (user.getRoleList() != null) {
                System.out.println("if in ici");
                for (int i = 0; i < user.getRoleList().size(); i++) {
                    Object[] objects = new Object[]{user.getId(), user.getRoleList().get(i).getValue()};
                    count = jdbcTemplate.update(SQLQueries.INSERT_USER_ROLE, objects);
                }
            } else {

                Object[] objects = new Object[]{user.getId(), Role.STUDENT.getValue()};
                count = jdbcTemplate.update(SQLQueries.INSERT_USER_ROLE, objects);


            }

            if (count == 1) {
                String token = SecurityUtil.generateToken();
                Object[] objects = new Object[]{user.getId(), token, 0};
                count = jdbcTemplate.update(SQLQueries.INSERT_TOKEN, objects);
                if (count == 1) {

                    notification.setFrom(from);
                    notification.setTo(user.getContact().getEmail());
                    notification.setSubject(subject);
                    notification.setContent(String.format(template, user.getName(), user.getSurname()) + "\n " + String.format(link, token));
                    notification.setSource(source);
                    notificationRepository.insertNotification(notification);
                }


            } else {
                System.out.println("else 000");

            }


        }

        if (count == 0) {
            flag = false;
        } else {
            for (int i = 0; i < user.getSubjectList().size(); i++) {
                Object[] objects = new Object[]{
                        user.getId(), user.getSubjectList().get(i).getId()
                };

                int result = jdbcTemplate.update(SQLQueries.ADD_STUDENT_SUBJECT, objects);
                if (result == 0) {
                    flag = false;
                    break;
                } else {

                }
            }

            Object[] objects = new Object[]{user.getContact().getTelHome(), user.getContact().getTelMobile(), user.getContact().getAddress(),
                    user.getContact().getEmail(), user.getId()};
            int netice = jdbcTemplate.update(SQLQueries.ADD_CONTACT, objects);
            if (netice == 0) {
                flag = false;
            } else {

            }


        }

        return user;

    }

    @Override
    public List<Subject> getSubjectList() {
        List<Subject> subjectList = (List<Subject>) jdbcTemplate.query(SQLQueries.getSubjectList, subjectRowMapper);

        return subjectList;
    }

    @Override
    public List<Group> getGroupBySubjectId(long subjectId) {
        Object[] objects = new Object[]{subjectId};
        List<Group> groupList = jdbcTemplate.query(SQLQueries.GET_GROUPS_BY_SUBJECT_ID, objects, groupRowMapper);
        groupList.forEach(group -> group.setTimeTables(getTimeTablesByGroupId(group.getId())));
        return groupList;
    }

    @Override
    public List<TimeTable> getGroupsTimeTablesBySubjectId(long subjectId) {
        Object[] objects = new Object[]{subjectId};
        List<TimeTable> timeTableList;
        timeTableList = jdbcTemplate.query(SQLQueries.GET_GROUPS_TIME_TABLE_BY_SUBJECT_ID, objects, timeTableMapRow);
        return timeTableList;
    }

    @Override
    public List<TimeTable> getTimeTablesByGroupId(long groupId) {
        List<TimeTable> list = jdbcTemplate.query(SQLQueries.GET_TIME_TABLE_BY_GROUP_ID, new Object[]{groupId}, timeTableMapRow);
        return list;
    }

    @Override
    public List<User> getTeachersBySubjectId(long subject_id) {
        Object[] objects = new Object[]{subject_id};
        List<User> teacherList = jdbcTemplate.query(SQLQueries.GET_TEACHERS_BY_SUBJECT_ID, objects, userRowMapper);
        return teacherList;
    }


    @Override
    public Optional<User> getUserByUsername(String username) {
        Optional<User> optionalUser = Optional.empty();
        Object[] args = new Object[]{username};
        List<User> userList = jdbcTemplate.query(SQLQueries.GET_USER_BY_USERNAME, args, userRowMapper);
        if (!userList.isEmpty()) {
            optionalUser = Optional.of(userList.get(0));
        }
        return optionalUser;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> optionalUser = Optional.empty();
        List<User> userList = jdbcTemplate.query(SQLQueries.GET_USER_BY_EMAIL, new Object[]{email}, studentRowMapper);
        if (!userList.isEmpty()) {
            optionalUser = Optional.of(userList.get(0));
        }
        return optionalUser;
    }

    @Override
    public void deleteSubjectById(long id) {
        Object[] args = new Object[]{id};
        int count = jdbcTemplate.update(SQLQueries.DELETE_SUBJECT_BY_ID, args);
    }

    @Override
    public List<User> getTeachers() {
        return jdbcTemplate.query(SQLQueries.GET_TEACHERS, teacherRowMapper);
    }


    @Override
    public List<Group> getAllGroups() {
        return jdbcTemplate.query(SQLQueries.GET_ALL_GROUPS,groupRowMapper);
    }

    @Override
    public List<User> getUserListPaging(String sql,int start, int length, String search) {
        Object [] arg=new Object[]{"%"+search+"%",start,length};
        return jdbcTemplate.query(sql,arg,userRowMapper);
    }

    @Override
    public List<Attandance> getAttendanceList(Long groupId) {
        return null;
    }

    @Override
    public List<Category> getCategories() {
        List<Category> categories = jdbcTemplate.query(SQLQueries.GET_CATEGORIES, categoriesRowMapper);
        return categories;
    }

    @Override
    public Optional<Subject> getSubjectById(long id) {
        Optional<Subject> optionalSubject = Optional.empty();
        Object[] args = new Object[]{id};
        List<Subject> subjectList = jdbcTemplate.query(SQLQueries.GET_SUBJECT_BY_ID, args, subjectRowMapper);
        if (!subjectList.isEmpty()) {
            optionalSubject = Optional.of(subjectList.get(0));
        }
        return optionalSubject;
    }

    @Override
    public void addNewSubject(Subject subject) {
        Object[] args = new Object[]{subject.getName(), subject.getPrice()};

        jdbcTemplate.update(SQLQueries.ADD_NEW_SUBJECT, args);
    }

    @Override
    public List<User> getStudentLisByStatus() {
        List<User> studentFormList = jdbcTemplate.query(SQLQueries.GET_STUDENT_LIST_BY_STATUS, userRowMapper);

        return studentFormList;
    }

    @Override
    public List<Role> getRoleListByUsername(long userId) {
        Object[] args = new Object[]{userId};
        List<Role> roleList = jdbcTemplate.query(SQLQueries.GET_ROLELIST_BY_USERNAME, args, roleMapperByUsername);
        return roleList;
    }

    @Transactional
    @Override
    public void updateStudent(User user) throws RuntimeException {
        boolean flag = true;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

 /*
        public static  final String UPDATE_STUDENT_BY_ID="
            update   user set name=?,surname=?,father_name=?,gender=?,section=?,fincod=?,grade=?,school=?,diplom_no=?,category_id=?,udate=current_timestamp() where id=?  ";

         */

                PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.UPDATE_STUDENT_BY_ID, Statement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getSurname());
                preparedStatement.setString(3, user.getFatherName());
                preparedStatement.setInt(4, user.getGender());
                preparedStatement.setInt(5, user.getSection());
                preparedStatement.setString(6, user.getFincod());
                preparedStatement.setString(7, user.getGrade());
                preparedStatement.setString(8, user.getSchool());
                preparedStatement.setString(9, user.getDiplomNo());
                preparedStatement.setLong(10, user.getCategory().getId());
                preparedStatement.setLong(11, user.getId());
                //todo update other variant

                return preparedStatement;
            }
        }, keyHolder);
        user.setId(keyHolder.getKey().longValue());


        if (count == 0) {
            flag = false;
        } else {
/*
            for (int i = 0; i < user.getSubjectList().size(); i++) {
                Object[] objects = new Object[]{
                        user.getId(), user.getSubjectList().get(i).getId()
                };

                int result = jdbcTemplate.update(SQLQueries.UPDATE_STUDENT_SUBJECT, objects);
                if (result == 0) {
                    flag = false;
                    break;
                } else {
                }
            }
*/
            //(tel_home,tel_mobil,address,email,user_id)
            // values('0123454563','0555220290','Qax','ferid.xelilov.2019@mail.ru',53);
            Object[] objects = new Object[]{user.getContact().getTelHome(), user.getContact().getTelMobile(), user.getContact().getAddress(),
                    user.getContact().getEmail(), user.getId()};

            int netice = jdbcTemplate.update(SQLQueries.UPDATE_CONTACT, objects);
            System.out.println("netice " + netice);
            if (netice == 0) {
                flag = false;
            } else {
            }


        }
       /* } catch (Exception e) {
            throw new RuntimeException("xeta var ay melim " + e.getMessage());
        }*/
    }

    @Override
    public List<Subject> getSubjectListByStudentId(long id) {
        Object[] args = new Object[]{id};
        List<Subject> subjectList = jdbcTemplate.query(SQLQueries.GET_SUBJECT_LIST_BY_STUDENT_ID, args, subjectRowMapper);
        return subjectList;
    }

    @Override
    public int getStudentCount() {
        int count = jdbcTemplate.queryForObject(SQLQueries.GET_STUDENT_COUNT, Integer.class);
        System.out.println("student count " + count);
        return count;
    }

    @Override
    public int getTeacherCount() {
        int count = jdbcTemplate.queryForObject(SQLQueries.GET_TEACHER_COUNT, Integer.class);
        System.out.println("teacher count " + count);
        return count;
    }

    @Override
    public void addAttdendance(List<Attandance> attandanceList, long group_id) throws RuntimeException {

        /*  insert into attandance(attandance,groupmember_id,group_id)\n" +
                    "values(?,?,?);*/
        for (int i = 0; i < attandanceList.size(); i++) {
            Object[] objects = new Object[]{attandanceList.get(i).getState(), attandanceList.get(i).getGroupMemberId(),
                    group_id};
            jdbcTemplate.update(SQLQueries.INSERT_ATTDENDACE, objects);

        }

    }

    @Override
    public List<User> getStudentByGroupId(long groupId) {
        Object[] args = new Object[]{groupId};
        List<User> studentList = jdbcTemplate.query(SQLQueries.GET_STUDENT_LIST_BY_GROUP_ID, args, userRowMapper);
        //todo change query
        return studentList;
    }

    @Override
    public List<DataTableUser> getStudentListAjax(int length, int start) {

        if (length == -1) {
            length = getStudentCount();
        }
        Object[] args = new Object[]{length, start};
        List<DataTableUser> dataTableUserList = jdbcTemplate.query(SQLQueries.GET_STUDENT_LIST_AJAX, args, dataTableUserRowMapper);
        return dataTableUserList;
    }

    @Override
    public List<LocalTime> getFreeTimesByRoomId(long roomId, long weekday) {
        Object[] args = new Object[]{roomId, weekday};
        List<LocalTime> time = jdbcTemplate.query(SQLQueries.GET_FREE_TIMES_BY_ROOM_ID, args, timeRowMapper);
        return time;
    }

    @Override
    public List<User> getStudentBySubjectId(long subject_id) {
        Object[] objects = new Object[]{subject_id};
        List<User> studentList = jdbcTemplate.query(SQLQueries.GET_STUDENT_LIST_BY_SUBJECT_ID, objects, userRowMapper);
        return studentList;
    }

    @Override
    public List<Room> getRooms() {
        List<Room> roomList = jdbcTemplate.query(SQLQueries.GET_ROOMS, roomRowMapper);
        return roomList;
    }

    @Override
    public List<TimeTable> getEmptyTimesByRoomId(long roomId) {
        Object[] args = new Object[]{roomId};
        List<TimeTable> timeTableList = jdbcTemplate.query(SQLQueries.GET_EMPTY_TIMES_BY_ROOM_ID, args, timeTableMapRow);
        return timeTableList;
    }

    private class SubjectRowMapper implements RowMapper<Subject> {

        @Override
        public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
            Subject subject = new Subject();
            subject.setId(rs.getInt("id"));
            subject.setName(rs.getString("name"));
            subject.setPrice(rs.getBigDecimal("price"));
            return subject;
        }
    }

    private class GroupRowMapper implements RowMapper<Group> {

        @Override
        public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
            Group group = new Group();
            /*
select g.id,g.name as group_name ,r.name  as room_name,  concat(t.name ,' ', t.surname)  as fullname " +
        " from `group`  g join room r on g.room_id=r.id and r.status=1 " +
        " join user t on  g.teacher_id=t.id and t.status=1 and g.status=1 where g.subject_id=?
             */
            group.setId(rs.getLong("id"));

            group.setName(rs.getString("group_name"));
            Room room = new Room();
            group.setRoom(room);
            group.getRoom().setName(rs.getString("room_name"));
            group.setTeacherFullName(rs.getString("fullname"));

            return group;
        }
    }


    private class TimeTableMapRow implements RowMapper<TimeTable> {
        @Override
        public TimeTable mapRow(ResultSet rs, int rowNum) throws SQLException {
            TimeTable timeTable = new TimeTable();
            /*
            select tt.id,r.id as room_id,r.name as room_name  , g.id as group_id ,g.name as group_name,weekdays,start_time,end_time from time_table tt join room r on tt.room_id=r.id and r.status=1  " +
            " join `group` g on g.id=tt.group_id and g.status=1 " +
            " join subject s on s.id=g.subject_id where s.id=?
             */
            if (rs.getLong("group_id") > 0) {
                timeTable.setGroupId(rs.getLong("group_id"));

            }
            if (rs.getString("group_name") != null) {
                timeTable.setGroupName(rs.getString("group_name"));
            }
            timeTable.setWeekDay(rs.getString("weekdays"));
            timeTable.setStartTime(rs.getString("start_time"));
            timeTable.setEndTime(rs.getString("end_time"));

            return timeTable;
        }
    }

    private class StudentRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            Contact contact = new Contact();
            user.setContact(contact);
            user.setId(rs.getLong("id"));
            user.setName(rs.getString("name"));
            user.setSurname(rs.getString("surname"));

            if (rs.getString("father_name") != null) {
                user.setFatherName(rs.getString("father_name"));
            }

            if (rs.getString("password") != null) {
                user.setPassword(rs.getString("password"));
            }
            if (rs.getString("email") != null) {
                user.getContact().setEmail(rs.getString("email"));
            }
            if (rs.getString("address") != null) {
                user.getContact().setAddress(rs.getString("address"));
            }
            if (rs.getString("tel_home") != null) {
                user.getContact().setTelMobile(rs.getString("tel_home"));
            }
            if (rs.getString("tel_mobil") != null) {
                user.getContact().setTelHome(rs.getString("tel_mobil"));
            }
            return user;
        }
    }

    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            Contact contact = new Contact();
            user.setContact(contact);

            user.setId(rs.getLong("id"));
            user.setName(rs.getString("name"));
            user.setSurname(rs.getString("surname"));
            if (rs.getString("father_name") != null) {
                user.setFatherName(rs.getString("father_name"));
            }
            if (rs.getInt("gender") == 1) {
                user.setGender(1);
            } else {
                user.setGender(2);
            }
            if (rs.getInt("section") == 0) {
                user.setSection(0);
            } else {
                user.setSection(1);
            }
            user.getContact().setAddress(rs.getString("unvan"));
            user.getContact().setEmail(rs.getString("email"));
            user.getContact().setTelMobile(rs.getString("tel_mobil"));
            user.getContact().setTelHome(rs.getString("tel_home"));


            user.setGrade(rs.getString("grade"));
            if (rs.getString("dob") != null) {
                user.setBirthDate(rs.getDate("dob").toString());
            }

            user.setPassword(rs.getString("password"));
            return user;
        }
    }


    private class RoomRowMapper implements RowMapper<Room> {

        @Override
        public Room mapRow(ResultSet rs, int rowNum) throws SQLException {
            Room room = new Room();
            room.setId(rs.getLong("id"));
            room.setName(rs.getString("name"));
            return room;
        }
    }

    private class TimeRowMapper implements RowMapper<LocalTime> {
        @Override
        public LocalTime mapRow(ResultSet rs, int rowNum) throws SQLException {

            return LocalTime.parse(rs.getString("start_time"));
        }
    }

    private class DataTableUserRowMapper implements RowMapper<DataTableUser> {

        @Override
        public DataTableUser mapRow(ResultSet rs, int rowNum) throws SQLException {
            DataTableUser dataTableUser = new DataTableUser();
            dataTableUser.setId(rs.getInt("id"));
            dataTableUser.setName(rs.getString("name"));
            dataTableUser.setSurname(rs.getString("surname"));

            return dataTableUser;
        }
    }


    private class UserRowmapperById implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            /*
             select u.id ,u.name,u.surname,u.father_name,u.gender,u.section,
             u.fincod,u.grade,u.school,u.diplom_no, ca.id,ca.ad ,c.id, " +
            " c.email,c.address,c.tel_home,c.tel_mobil,u.dob" +
             */
            user.setId(rs.getLong("id"));
            user.setName(rs.getString("name"));
            user.setSurname(rs.getString("surname"));
            user.setFatherName(rs.getString("father_name"));
            user.setGender(rs.getInt("gender"));
            user.setSection(rs.getInt("section"));
            user.setFincod(rs.getString("fincod"));
            user.setGrade(rs.getString("grade"));
            user.setSchool(rs.getString("school"));

//            user.setCategory(rs.getInt("caId"));
//            user.setCategoryName(rs.getString("ad"));
            Contact contact = new Contact();
            user.setContact(contact);
            if (rs.getString("email") != null) {
                user.getContact().setEmail(rs.getString("email"));
            }
            if (rs.getString("address") != null) {
                user.getContact().setAddress(rs.getString("address"));
            }
            if (rs.getString("tel_home") != null) {
                user.getContact().setTelMobile(rs.getString("tel_home"));
            }
            if (rs.getString("tel_mobil") != null) {
                user.getContact().setTelHome(rs.getString("tel_mobil"));
            }
            if (rs.getString("dob") != null) {
                user.setBirthDate(rs.getDate("dob").toString());
            }


            return user;
        }
    }

    private class RoleMapperByUsername implements RowMapper<Role> {
        @Override
        public Role mapRow(ResultSet rs, int rowNum) throws SQLException {
            Role role = Role.fromValue(rs.getInt("id"));
            role.setSuccessPage(rs.getString("succespage"));
            role.setName(rs.getString("name"));
            return role;
        }
    }

    private class CategoriesRowMapper implements RowMapper {

        @Override
        public Object mapRow(ResultSet resultSet, int i) throws SQLException {
            Category category = new Category();
            category.setId(resultSet.getLong("id"));
            category.setName(resultSet.getString("name"));
            return category;
        }
    }


    private class TeacherRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            /*
        select s.name,u.id,u.name,u.surname,u.father_name, gender,section,fincod, diplom_no,u.idate hire_date,dob,username " +
        "           ,c.address , c.email,c.tel_mobil ,password " +
        "             from user  u   join teacher_subject ts on ts.user_id=u.id  " +
        "             " +
        "               join subject s on ts.subject_id-s.id " +
        "        join contact c on c.user_id=u.id " +
        "             where u.status= 1 and u.category_id=10
             */
            user.setId(rs.getLong("id"));
            user.setName(rs.getString("name"));
            user.setSurname(rs.getString("surname"));
            user.setFatherName(rs.getString("father_name"));
            user.setUsername(rs.getString("username"));
            user.setGender(rs.getInt("gender"));
            user.setSection(rs.getInt("section"));
            user.setFincod(rs.getString("fincod"));
            user.setDiplomNo(rs.getString("diplom_no"));
            user.setIdate(rs.getTimestamp("hire_date").toLocalDateTime());


            Contact contact = new Contact();
            user.setContact(contact);
            if (rs.getString("email") != null) {
                user.getContact().setEmail(rs.getString("email"));
            }
            if (rs.getString("address") != null) {
                user.getContact().setAddress(rs.getString("address"));
            }
            if (rs.getString("tel_mobil") != null) {
                user.getContact().setTelMobile(rs.getString("tel_mobil"));
            }
            if (rs.getString("dob") != null) {
                user.setBirthDate(rs.getDate("dob").toString());
            }


            return user;
        }
    }
}

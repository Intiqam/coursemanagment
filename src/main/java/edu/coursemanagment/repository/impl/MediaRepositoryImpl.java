package edu.coursemanagment.repository.impl;


import edu.coursemanagment.domain.Media;
import edu.coursemanagment.repository.MediaRepository;
import edu.coursemanagment.repository.SQLQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
public class MediaRepositoryImpl implements MediaRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private MediaRowMapper mediaRowMapper=new MediaRowMapper();

    @Override
    public void addMedia(Media media) {
        /*
        insert into media(user_id,original_file_name, file_name,main,  video, mobile,mime_type, file_length, status ) " +
            "                   values(?, ?, ? ,?, ?, ?, ?, ?,1)
         */
        Object[] object = new Object[]{
                media.getUserId(),
                media.getOriginalFileName(),
                media.getFileName(),
                media.isMain() ? 1 : 0,
                media.isVideo() ? 1 : 0,
                media.isMobile() ? 1 : 0,
                media.getContentType(),
                media.getLength()};
        int count=  jdbcTemplate.update(SQLQueries.ADD_MEDIA, object);
        if(count==0){
            throw new RuntimeException("Error saving media " + media);
        }
    }

    @Override
    public Optional<Media> getMediaById(long id) {
        Optional<Media> optionalMedia=Optional.empty();
        Media  media=null;
        List<Media> mediaList=jdbcTemplate.query(SQLQueries.GET_MEDIA_BY_ID,new Object[]{id},mediaRowMapper);
        if(!mediaList.isEmpty()){
            media   = mediaList.get(0);
            optionalMedia= Optional.of(media);
        }

        return optionalMedia;
    }

    private class MediaRowMapper implements RowMapper<Media> {
        @Override
        public Media mapRow(ResultSet rs, int i) throws SQLException {
            Media media=new Media();
            media.setId(rs.getLong("id"));
            media.setUserId(rs.getLong("user_id"));
            media.setOriginalFileName(rs.getString("original_file_name"));
            media.setFileName(rs.getString("file_name"));
            media.setMain(rs.getInt("main")==1);
            media.setVideo(rs.getInt("video")==1);
            media.setMobile(rs.getInt("mobile")==1);
            media.setContentType(rs.getString("mime_type"));
            media.setLength(rs.getLong("file_length"));
            return media;
        }
    }
}

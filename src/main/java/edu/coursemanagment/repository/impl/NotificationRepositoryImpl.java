package edu.coursemanagment.repository.impl;


import edu.coursemanagment.domain.Notification;
import edu.coursemanagment.domain.NotificationType;
import edu.coursemanagment.repository.NotificationRepository;
import edu.coursemanagment.repository.SQLQueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class NotificationRepositoryImpl implements NotificationRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private NotificationRowMapper notificationRowMapper = new NotificationRowMapper();

    @Override
    public void insertNotification(Notification notification) {
        /*
        public static final String INSERT_NOTIFICATION= "
        insert into notification(`from`, `to`,subject,content,source_system ) values( ?,?,?, ?,?)  ";

         */
        Object[] object = new Object[]{notification.getFrom(), notification.getTo(), notification.getSubject(), notification.getContent(), notification.getSource()};
        jdbcTemplate.update(SQLQueries.INSERT_NOTIFICATION, object);
    }



    @Override
    public List<Notification> getPendingNotificatios(NotificationType notificationType, int limit) {
        Object[] objects = new Object[]{notificationType.getType(), limit};
        List<Notification> notifications = jdbcTemplate.query(SQLQueries.GET_PENDING_NOTIFICATIONS_BY_PENDING, objects, notificationRowMapper);
        return notifications;
    }

    @Override
    public void updateNotification(Notification notification) {
        Object[] objects = new Object[]{notification.getNotificationStatus(), notification.getLogData(), notification.getId()};
        int count =  jdbcTemplate.update(SQLQueries.UPDATE_NOTIFICATION, objects);
    }

    private class NotificationRowMapper implements RowMapper<Notification> {

        @Override
        public Notification mapRow(ResultSet rs, int rowNum) throws SQLException {
            Notification notification = new Notification();

            /*
            * GET_PENDING_NOTIFICATIONS =" select id, `from`,`to`,type,
            * content,subject," +
            " source_system, process_status, log_data " +
            " from notification " +
            " where type = ? and process_status = 0 and status = 1 " +
            " order by id" +
            " limit ?";
            * */
            notification.setId(rs.getLong("id"));
            notification.setFrom(rs.getString("from"));
            notification.setTo(rs.getString("to"));
            notification.setType(NotificationType.from(rs.getInt("type")));
            notification.setContent(rs.getString("content"));
            notification.setSubject(rs.getString("subject"));
            notification.setSource(rs.getString("source_system"));
            notification.setNotificationStatus(rs.getInt("process_status"));
            notification.setLogData(rs.getString("log_data"));
            notification.setIdate(rs.getTimestamp("idate").toLocalDateTime());

            if (rs.getTimestamp("udate") != null) {
                notification.setUdate(rs.getTimestamp("udate").toLocalDateTime());
            }

            return notification;
        }
    }
}

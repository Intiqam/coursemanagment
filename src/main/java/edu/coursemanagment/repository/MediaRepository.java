package edu.coursemanagment.repository;

import edu.coursemanagment.domain.Media;


import java.util.Optional;

public interface MediaRepository {
    void addMedia(Media media);
    Optional<Media> getMediaById(long id);
}

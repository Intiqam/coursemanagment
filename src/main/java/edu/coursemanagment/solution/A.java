public class A implements Runnable{
        private  int number;

    public A() {
    }

    public A(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println("running task to "+number);
    }
}

package edu.coursemanagment.solution;

public class C {
    private int age;
    private String name;

    public C() {
    }

    public C(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "C{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

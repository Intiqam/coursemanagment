package edu.coursemanagment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

//@EnableScheduling
@EnableAsync
@SpringBootApplication
public class CoursemanagmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoursemanagmentApplication.class, args);
	}

}

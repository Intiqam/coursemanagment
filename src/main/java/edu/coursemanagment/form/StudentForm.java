package edu.coursemanagment.form;

import edu.coursemanagment.domain.Contact;
import edu.coursemanagment.domain.UserBase;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;


public class StudentForm extends UserBase implements Serializable {
    private String name;
    private String surname;
    private String fatherName;
    private int gender;
    private String birthDate;
    private Contact contact;
    private int section;
    private String fincod;
    private String grade;
    private String index;
    private String school;
    private int category;
    private int branch;
    private long [] subjectList;
    private int slot;
    private String additionalInform;
    private String categoryName;
    private String password;

    private MultipartFile file;

    public StudentForm() {
    }

    public StudentForm(String name, String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, int category, int branch, long[] subjectList, int slot, String additionalInform, String categoryName, String password, MultipartFile file) {
        this.name = name;
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.category = category;
        this.branch = branch;
        this.subjectList = subjectList;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.categoryName = categoryName;
        this.password = password;
        this.file = file;
    }

    public StudentForm(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, String name1, String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, int category, int branch, long[] subjectList, int slot, String additionalInform, String categoryName, String password, MultipartFile file) {
        super(id, name, idate, udate, status);
        this.name = name1;
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.category = category;
        this.branch = branch;
        this.subjectList = subjectList;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.categoryName = categoryName;
        this.password = password;
        this.file = file;
    }

    public StudentForm(String name, String name1, String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, int category, int branch, long[] subjectList, int slot, String additionalInform, String categoryName, String password, MultipartFile file) {
        super(name);
        this.name = name1;
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.category = category;
        this.branch = branch;
        this.subjectList = subjectList;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.categoryName = categoryName;
        this.password = password;
        this.file = file;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public String getFincod() {
        return fincod;
    }

    public void setFincod(String fincod) {
        this.fincod = fincod;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getBranch() {
        return branch;
    }

    public void setBranch(int branch) {
        this.branch = branch;
    }

    public long[] getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(long[] subjectList) {
        this.subjectList = subjectList;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getAdditionalInform() {
        return additionalInform;
    }

    public void setAdditionalInform(String additionalInform) {
        this.additionalInform = additionalInform;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "StudentForm{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", gender=" + gender +
                ", birthDate='" + birthDate + '\'' +
                ", contact=" + contact +
                ", section=" + section +
                ", fincod='" + fincod + '\'' +
                ", grade='" + grade + '\'' +
                ", index='" + index + '\'' +
                ", school='" + school + '\'' +
                ", category=" + category +
                ", branch=" + branch +
                ", subjectList=" + Arrays.toString(subjectList) +
                ", slot=" + slot +
                ", additionalInform='" + additionalInform + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", password='" + password + '\'' +
                ", file=" + file +
                "} " + super.toString();
    }
}

package edu.coursemanagment.controller.admin;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


import edu.coursemanagment.domain.*;
import edu.coursemanagment.form.StudentForm;
import edu.coursemanagment.service.MediaService;
import edu.coursemanagment.service.StudentService;
import edu.coursemanagment.util.ConvertUtil;
import edu.coursemanagment.validator.StudentFormValidator;
import edu.coursemanagment.validator.SubjectValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/admin")
public class AdminController {


    private static final Logger logger = Logger.getLogger(AdminController.class.getName());
    @Value("${upload.folder}")
    private String uploadFolder;

    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentFormValidator studentFormValidator;
    @Autowired
    private SubjectValidator subjectValidator;
    @Autowired
    private MediaService mediaService;


    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {

        Object target = dataBinder.getTarget();

        if (target != null && target.getClass() == StudentForm.class) {
            dataBinder.setValidator(studentFormValidator);
        }
        if (target != null && target.getClass() == Subject.class) {
            dataBinder.setValidator(subjectValidator);
        }

    }

    @GetMapping("/")
    public ModelAndView viewInstructor() {
        ModelAndView modelAndView = new ModelAndView("admin/index");
        int studentCount = studentService.getStudentCount();
        int teacherCount = studentService.getTeacherCount();
        modelAndView.addObject("studentsCount", studentCount);
        modelAndView.addObject("teacherCount", teacherCount);

        return modelAndView;

    }


    @GetMapping("/groups/{id}")
    public ModelAndView groups(@PathVariable(name = "id") long id) {
        Group group = new Group();
        List<Group> groupList = studentService.getGroupBySubjectId(id);
        //List<TimeTable> list=studentService.getGroupsTimeTablesBySubjectId(id);

        ModelAndView modelAndView = new ModelAndView("admin/groups");
        modelAndView.addObject("groupList", groupList);
        modelAndView.addObject("subjectId", id);
        //modelAndView.addObject("timeTableList",list);
        return modelAndView;
    }

    @GetMapping("/viewGroup")
    public ModelAndView viewGroupPage(@RequestParam(name = "id") Long id) {

        Attandance attandance = new Attandance();
        ModelAndView modelAndView = new ModelAndView("admin/viewGroup");
        List<User> studentList = studentService.getStudentByGroupId(id);
        Map<Long, String> studentMap = new HashMap<>();
        studentList.stream().forEach(student -> studentMap.put(student.getId(), student.getName() + " " + student.getSurname() + " " + student.getFatherName()));

        modelAndView.addObject("studentList", studentList);
        modelAndView.addObject("studentMap2", studentMap);


        modelAndView.addObject("id", id);
        modelAndView.addObject("attandance", attandance);
        return modelAndView;
    }


    @GetMapping("/students")
    public ModelAndView allStudents() {
        ModelAndView modelAndView = new ModelAndView("admin/studentsAjax");
        List<User> user = studentService.getStudentLisByStatus();


            modelAndView.addObject("studentList", user);
        return modelAndView;
    }


    @GetMapping("/teachers")
    public ModelAndView allTeachers() {
        ModelAndView modelAndView = new ModelAndView("admin/teachers");
        List<User> teachers = studentService.getTeachers();


        modelAndView.addObject("teachers", teachers);
        return modelAndView;
    }

    @GetMapping("addmit")
    public ModelAndView addmit() {
        ModelAndView mav = new ModelAndView("admin/addmit-form");
        StudentForm studentForm = new StudentForm();

        List<Subject> subjectList = studentService.getSubjectList();
        List<Category> categories = studentService.getCategories();
        Map<Long, String> subjectMap = new HashMap<>();
        Map<Long, String> categoryMap = new HashMap<>();

        subjectList.stream().forEach(subject -> subjectMap.put(subject.getId(), subject.getName()));
        categories.stream().forEach(category -> categoryMap.put(category.getId(), category.getName()));
        mav.addObject("subjectMap", subjectMap);
        mav.addObject("categoryMap", categoryMap);

        studentForm.setPassword(String.valueOf(LocalDateTime.now().getNano()));
        mav.addObject("studentForm", studentForm);

        return mav;
    }

    @PostMapping("/addmit")
    public ModelAndView applyFormPost(@RequestParam("file") MultipartFile file,
                                      @ModelAttribute("studentForm")
                                      @Validated StudentForm studentForm, BindingResult bindingResult,
                                      RedirectAttributes redirectAttributes) throws IOException {

        studentForm.setFile(file);
        ModelAndView mav = new ModelAndView("redirect:/admin/students");
        User student = new User();
        if (bindingResult.hasErrors()) {
            List<Subject> subjectList = studentService.getSubjectList();
            Map<Long, String> subjectMap = new HashMap<>();
            subjectList.stream().forEach(subject -> subjectMap.put(subject.getId(), subject.getName()));
            mav.addObject("subjectMap", subjectMap);
            mav.setViewName("/admin/addmit-form");
        } else {
            student = ConvertUtil.convertForm(studentForm);
            try {
                student = studentService.addStudent(student);
                try {

                    if (!file.isEmpty()) {
                        System.out.println("Media try 2 ");
                        Media media = new Media();
                        media.setUserId(student.getId());
                        media.setOriginalFileName(StringUtils.getFilename(file.getOriginalFilename()));
                        media.setContentType(file.getContentType());
                        media.setLength(file.getSize());
                        media.setMobile(false);
                        media.setVideo(false);
                        media.setMobile(false);
                        media.setInputStream(file.getInputStream());
                        System.out.println("media1  " + media);
                        mediaService.addMedia(media);
                        System.out.println("media1  " + media);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                redirectAttributes.addFlashAttribute("mesage", studentForm.getName() + "  qeydiyyat ugurla tamamlandi");
            } catch (Exception e) {
                logger.error("error occurred while adding student. id = " + student.getId());
                mav.setViewName("redirect:/admin/students");
            }
        }
        return mav;
    }

    @GetMapping("/subjects")
    public ModelAndView subjectsPage() {
        Subject subject = new Subject();
        ModelAndView modelAndView = new ModelAndView("admin/subjectList");
        List<Subject> subjectList = studentService.getSubjectList();
        modelAndView.addObject("subjectList", subjectList);
        modelAndView.addObject("subject", subject);

        return modelAndView;
    }

    @PostMapping("/addSubject")
    public ModelAndView addSubject(Model model,

                                   @ModelAttribute("subject")
                                   @Validated Subject subject,
                                   BindingResult bindingResult) throws SQLException {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/subjects");
        System.out.println(subject);
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("admin/subjectList");
            List<Subject> subjectList = studentService.getSubjectList();
            modelAndView.addObject("subjectList", subjectList);

        } else {
            try {
                studentService.addNewSubject(subject);
            } catch (Exception e) {
                throw new SQLException("INSERT OLUNMADI" + e);
            }
        }


        return modelAndView;
    }

    @GetMapping("/edit/{id}")
    public ModelAndView editSubject(@PathVariable("id") long id) {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/subjectList");
        Subject subject = new Subject();
        Optional<Subject> optionalSubject = Optional.empty();

        optionalSubject = studentService.getSubjectById(id);
        if (optionalSubject.isPresent()) subject = optionalSubject.get();

        List<Subject> subjectList = studentService.getSubjectList();
        modelAndView.addObject("subjectList", subjectList);
        modelAndView.addObject("subject", subject);

        return modelAndView;
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteSubjectById(@PathVariable("id") long id) {
        System.out.println(id);
        studentService.deleteSubjectById(id);
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/subjects");


        return modelAndView;
    }

    @GetMapping("/createGroup/{id}")
    public ModelAndView createGroup(@PathVariable(name = "id", required = false) Long subjectId) {
        ModelAndView mav = new ModelAndView("admin/create-group");
        Group group = new Group();
        Subject subject = new Subject();
        group.setSubject(subject);
        group.getSubject().setId(subjectId);
        System.out.println("get grup.subjevt.id= " + group.getSubject().getId());

        List<User> teacherList = studentService.getTeachersBySubjectId(subjectId);
        List<Room> roomList = studentService.getRooms();
        List<TimeTable> timeTableList = studentService.getEmptyTimesByRoomId(2);
        List<User> studentlist = studentService.getStudentBySubjectId(subjectId);
        Map<Long, String> mapTeacher = new HashMap<>();
        Map<Long, String> mapRoom = new HashMap<>();
        Map<Long, String> mapStudent = new HashMap<>();

        System.out.println("studentList=" + studentlist);

        teacherList.stream().forEach(user -> mapTeacher.put(user.getId(), user.getName() + " " + user.getSurname() + " " + user.getFatherName()));
        roomList.stream().forEach(room -> mapRoom.put(room.getId(), room.getName()));
        studentlist.stream().forEach(user -> mapStudent.put(user.getId(), user.getName() + " " + user.getSurname()));

        //System.out.println("timetablelist=" + timeTableList);
        Map<String, TimeTable> mapTimeTable = new HashMap<>();
        for (TimeTable timeTable : timeTableList) {
            mapTimeTable.put(timeTable.getWeekDay(), timeTable);
        }
        timeTableList.stream().forEach(timeTable -> mapTimeTable.put(timeTable.getWeekDay(), new TimeTable(timeTable.getStartTime(), timeTable.getEndTime())));

        // List<LocalTime>localTimeList= studentService.getFreeTimesByRoomId(2,1);
        mav.addObject("group", group);
        mav.addObject("teacherList", teacherList);
        mav.addObject("mapStudent", mapStudent);
        mav.addObject("mapTeacher", mapTeacher);
        mav.addObject("mapRoom", mapRoom);
        // mav.addObject("emptyTimes",localTimeList);
        mav.addObject("subjectId", subjectId);
        return mav;
    }

    @PostMapping("/createGroup/{subjectId}")
    public ModelAndView createGroupPost(Model model, @ModelAttribute("group") Group group,
                                        @PathVariable(name = "subjectId") Long subjectId) {
        ModelAndView modelAndView = new ModelAndView("reception/test");

        System.out.println("group  ==" + group);
//        String[] startTime = Arrays.stream(group.getStartTime()).filter(e -> e != null && e.length() > 0).toArray(size -> new String[size]);
//        String[] endTime = Arrays.stream(group.getEndTime()).filter(e -> e != null && e.length() > 0).toArray(size -> new String[size]);
        String[] days = group.getDays();

        List<TimeTable> timeTableList = new ArrayList<>();

        for (int i = 0; i < group.getDays().length; i++) {
            TimeTable timeTable = new TimeTable();

            timeTable.setWeekDay(days[i]);
            timeTable.setStartTime(group.getStartTime());
            timeTable.setEndTime(group.getEndTime());
            timeTableList.add(timeTable);

        }
        Subject subject = new Subject();
        group.setSubject(subject);
        group.getSubject().setId(subjectId);


        group.setTimeTables(timeTableList);
        studentService.addGroup(group);

        return modelAndView;
    }

    @GetMapping("/studentDetail/{id}")
    public ModelAndView studentDetaisl(@PathVariable("id") long id) {
        System.out.println("id==" + id);
        User user = new User();
        ModelAndView modelAndView = new ModelAndView("admin/student-detail");
        Optional<User> studentForm = studentService.getStudentById(id);
        if (studentForm.isPresent()) {
            user = studentForm.get();
        }
        System.out.println("stu " + user);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @GetMapping("/studentDetail-edit/{id}")
    public ModelAndView studentDetaislEdit(@PathVariable("id") long id) {
        Optional<User> studentForm = Optional.empty();
        User user = new User();
        ModelAndView modelAndView = new ModelAndView("admin/student-detail-edit");
        studentForm = studentService.getStudentById(id);
        if (studentForm.isPresent()) {
            user = studentForm.get();
        }
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @GetMapping("/messaging")
    public String message() {
        return "admin/message";
    }

    @PostMapping("/attendance/{id}")
    public ModelAndView attendance(HttpServletRequest request, @PathVariable(name = "id") Long id) {
        Map<String, String[]> requestMap = request.getParameterMap();
        List<User> userList = studentService.getStudentByGroupId(id);
        List<Attandance> attendanceList = new ArrayList<>();
        for (User user : userList) {
            Attandance attandance = new Attandance(user.getId(),
                    requestMap.get(String.valueOf(user.getId())));
            attandance.setIdate(LocalDate.now());
            attendanceList.add(attandance);
        }
        System.out.println(attendanceList);
        studentService.addAttdendance(attendanceList, id);
        return new ModelAndView("admin/index");
    }

    @GetMapping("/attendance")
    public ModelAndView attendancePage() {
                List<Group> groupList=studentService.getAllGroups();
                Map<Long,String> groupMap=new HashMap<>();
                ModelAndView mav=new ModelAndView("admin/attendance");
                groupList.forEach(group -> groupMap.put(group.getId(),group.getName()));
                mav.addObject("groupList",groupList);
        return mav;
    }

    @PostMapping("/showAttendance")
    public ModelAndView showAttendance(@RequestParam Long id){
        ModelAndView mav= new ModelAndView("redirect:/admin/attendance");

        return mav;
    }

    @GetMapping("/media/{id}")
    public ResponseEntity<Resource> getMediaById(@PathVariable(name = "id") long id) {
        ResponseEntity<Resource> entity = null;
        Optional<Media> optionalMedia = mediaService.getMediaById(id);
        if (optionalMedia.isPresent()) {
            Media media = optionalMedia.get();
            try {
                Resource resource = new FileUrlResource(uploadFolder + File.separator + media.getFileName());
                System.out.println("resource==" + resource);

                if (resource.exists()) {
                    HttpHeaders httpHeaders = new HttpHeaders();

                    httpHeaders.add("Content-Type", media.getContentType());
                    entity = new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
                } else {
                    entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } catch (Exception e) {
                entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return entity;
    }


}

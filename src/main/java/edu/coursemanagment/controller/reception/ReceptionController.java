/*
package edu.coursemanagment.coursemanagment.controller.reception;


import edu.coursemanagment.coursemanagment.domain.*;
import edu.coursemanagment.coursemanagment.form.StudentForm;
import edu.coursemanagment.coursemanagment.repository.StudentRepository;
import edu.coursemanagment.coursemanagment.service.NotificationService;
import edu.coursemanagment.coursemanagment.service.StudentService;
import edu.coursemanagment.coursemanagment.util.ConvertUtil;

import edu.coursemanagment.coursemanagment.validator.StudentFormValidator;
import edu.coursemanagment.coursemanagment.view.StudentListExcelView;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/reception")
public class ReceptionController {

    private static final Logger logger = Logger.getLogger(ReceptionController.class.getName());

    private static String UPLOADED_FOLDER = "C:\\Users\\Admin\\Desktop\\";
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentFormValidator studentFormValidator;
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    NotificationService notificationService;

    @GetMapping("/")
    public String indexPage() {

        return "reception/index";
    }

    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {

        Object target = dataBinder.getTarget();

        if (target != null && target.getClass() == StudentForm.class) {
            dataBinder.setValidator(studentFormValidator);
        }

    }

    @GetMapping("/apply")
    public ModelAndView applyForm() {
        System.out.println("get apply form");
        ModelAndView mav = new ModelAndView("reception/apply");
        StudentForm studentForm = new StudentForm();

        List<Subject> subjectList = studentService.getSubjectList();
        Map<Long, String> subjectMap = new HashMap<>();

//        fuelTypeList.stream().forEach(fuelType -> fuelMap.put(fuelType.getId(), fuelType.getName()));
        subjectList.stream().forEach(subject -> subjectMap.put(subject.getId(),subject.getName()));
        mav.addObject("subjectMap", subjectMap);
        mav.addObject("subjectList", subjectList);
        mav.addObject("studentForm", studentForm);
        System.out.println("get apply form 2");
        return mav;
    }


    @PostMapping("/apply")
    public ModelAndView applyFormPost(Model model, @ModelAttribute("studentForm")
    @Validated StudentForm studentForm,
                                      BindingResult bindingResult, RedirectAttributes redirectAttributes) throws IOException {

        ModelAndView mav = new ModelAndView("reception/apply");
        User student = new User();
        if (bindingResult.hasErrors()) {

        } else {

            logger.info("converting student form to student  = " +studentForm);
            System.out.println("student form"+studentForm);

            byte[] bytes = studentForm.getFile().getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + studentForm.getFile().getOriginalFilename());
            Files.write(path, bytes);

            student = ConvertUtil.convertForm(studentForm);
                int username=studentService.getLastUsername();

                student.setUsername(username+1);
            logger.info("converting student form to student id = " +student.getId());

            try {
                System.out.println("add student"+student);
                studentService.addStudent(student);
                redirectAttributes.addFlashAttribute("mesage", studentForm.getName() + "  qeydiyyat ugurla tamamlandi");
                mav.setViewName("redirect:/reception/");

            } catch (Exception e) {
                logger.error("error occurred while adding student. id = "+student.getId());

                mav.setViewName("redirect:/reception/");
            }
        }

        return mav;
    }


    @GetMapping("/mycourses")
    public ModelAndView myCourses() {
        ModelAndView modelAndView = new ModelAndView("reception/mycourses");
        List<Subject> subjectList = studentService.getSubjectList();
        logger.info("subjectList = "+subjectList);

        modelAndView.addObject("subjectList", subjectList);
        return modelAndView;
    }

    @GetMapping("/editcourse")
    public String editCourse() {
        return "reception/editcourses";
    }

    @GetMapping("/groups")
    public ModelAndView groups(@RequestParam(name = "id") Long id) {
        Group group = new Group();
        List<Group> groupList = studentService.getGroupBySubjectId(id);
        //List<TimeTable> list=studentService.getGroupsTimeTablesBySubjectId(id);

        ModelAndView modelAndView = new ModelAndView("reception/groups");
        modelAndView.addObject("groupList", groupList);
        modelAndView.addObject("subjectId", id);
        //modelAndView.addObject("timeTableList",list);
        return modelAndView;
    }


    @GetMapping("/createGroup")
    public ModelAndView createGroup(@RequestParam(name = "id") Long subjectId) {
        ModelAndView mav = new ModelAndView("reception/createGroup");
        Group group = new Group();
        Subject subject = new Subject();
        group.setSubject(subject);
        group.getSubject().setId(subjectId);
        System.out.println("get grup.subjevt.id= " + group.getSubject().getId());

        List<User> teacherList = studentService.getTeachersBySubjectId(subjectId);
        List<Room> roomList = studentService.getRooms();
        List<TimeTable> timeTableList = studentService.getEmptyTimesByRoomId(2);
        List<User> studentlist = studentService.getStudentBySubjectId(subjectId);
        Map<Long, String> mapTeacher = new HashMap<>();
        Map<Long, String> mapRoom = new HashMap<>();
        Map<Long, String> mapStudent = new HashMap<>();

        System.out.println("studentList=" + studentlist);

        teacherList.stream().forEach(user -> mapTeacher.put(user.getId(), user.getName() + " " + user.getSurname() + " " + user.getFatherName()));
        roomList.stream().forEach(room -> mapRoom.put(room.getId(), room.getName()));
        studentlist.stream().forEach(user -> mapStudent.put(user.getId(), user.getName() + " " + user.getSurname()));

        //System.out.println("timetablelist=" + timeTableList);
        Map<String, TimeTable> mapTimeTable = new HashMap<>();
        for (TimeTable timeTable : timeTableList) {
            mapTimeTable.put(timeTable.getWeekDay(), timeTable);
        }
        timeTableList.stream().forEach(timeTable -> mapTimeTable.put(timeTable.getWeekDay(), new TimeTable(timeTable.getStartTime(), timeTable.getEndTime())));

        List<LocalTime>localTimeList= studentService.getFreeTimesByRoomId(2,1);
        mav.addObject("group", group);
        mav.addObject("teacherList", teacherList);
        mav.addObject("mapStudent", mapStudent);
        mav.addObject("mapTeacher", mapTeacher);
        mav.addObject("mapRoom", mapRoom);
        mav.addObject("emptyTimes",localTimeList);
        return mav;
    }

    @PostMapping("/createGroup")
    public ModelAndView createGroupPost(Model model, @ModelAttribute("group") Group group,
                                        @RequestParam(name = "id") Long subjectId) {
        ModelAndView modelAndView = new ModelAndView("reception/test");


        String[] startTime = Arrays.stream(group.getStartTime()).filter(e -> e != null && e.length() > 0).toArray(size -> new String[size]);
        String[] endTime = Arrays.stream(group.getEndTime()).filter(e -> e != null && e.length() > 0).toArray(size -> new String[size]);
        String[] days = group.getDays();

        List<TimeTable> timeTableList = new ArrayList<>();

        for (int i = 0; i < group.getDays().length; i++) {
            TimeTable timeTable = new TimeTable();

            timeTable.setWeekDay(days[i]);
            timeTable.setStartTime(startTime[i]);
            timeTable.setEndTime(endTime[i]);
            timeTableList.add(timeTable);

        }
        Subject subject = new Subject();
        group.setSubject(subject);
        group.getSubject().setId(subjectId);
        System.out.println("timetable =" + timeTableList);


        group.setTimeTables(timeTableList);
        studentService.addGroup(group);
        System.out.println("post group " + group);
        System.out.println("post grup.subjevt.id= " + group.getSubject().getId());

        return modelAndView;
    }

    @GetMapping("/viewGroup")
    public ModelAndView viewGroupPage(@RequestParam(name = "id") Long id) {

        Attandance attandance = new Attandance();
        ModelAndView modelAndView = new ModelAndView("reception/viewgroup");
        List<User> studentList = studentService.getStudentByGroupId(id);
        Map<Long, String> studentMap = new HashMap<>();
        studentList.stream().forEach(student -> studentMap.put(student.getId(), student.getName() + " " + student.getSurname() + " " + student.getFatherName()));

        modelAndView.addObject("studentList", studentList);
        modelAndView.addObject("studentMap2", studentMap);
        System.out.println("studentList grup=" + studentList);

        studentMap.forEach((k, v) -> {
            System.out.println(k + " " + v);
        });
        modelAndView.addObject("id", id);
        modelAndView.addObject("attandance", attandance);
        return modelAndView;
    }

    @PostMapping("/attandance")
    public ModelAndView attandanceResult(Model model, @ModelAttribute("attandance") Attandance attandance, @RequestParam(name = "id") Long id) {
        ModelAndView modelAndView = new ModelAndView("reception/test");

        List<Long> allStudentListId = new ArrayList<>();
        List<Long> presentList;
        List<Attandance> attandanceList = new ArrayList<>();
        try {

            studentService.getStudentByGroupId(id).stream().forEach(s -> allStudentListId.add(s.getId()));
            presentList = Arrays.stream(attandance.getUserId()).collect(Collectors.toList());

            for (int i = 0; i < allStudentListId.size(); i++) {
                Attandance attandance1 = new Attandance();
                attandance1.setGroupMemberId(allStudentListId.get(i));
                attandance1.setIdate(LocalDateTime.now());
                if (presentList.contains(allStudentListId.get(i))) {
                    attandance1.setState((long) 1);
                } else {
                    attandance1.setState((long) 0);
                }
                attandanceList.add(attandance1);
            }
            System.out.println("attendList hazir=" + attandanceList);
            logger.info("attendancelist = "+attandanceList);
            studentService.addAttdendance(attandanceList, id);
            logger.info("attdendance added success ");
        } catch (Exception ex) {
            logger.error("error occurred while adding atendance "+ex);
        }

        return modelAndView;
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("file") MultipartFile[] file,
                                   RedirectAttributes redirectAttributes) {

        if (file.length == 0) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:uploadStatus";
        }

        try {

            // Get the file and save it somewhere
            for (int i = 0; i < file.length; i++) {
                byte[] bytes = file[i].getBytes();
                Path path = Paths.get(UPLOADED_FOLDER + file[i].getOriginalFilename());
                Files.write(path, bytes);

                redirectAttributes.addFlashAttribute("message",
                        "You successfully uploaded '" + file[i].getOriginalFilename() + "'");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return "redirect:/reception/";
    }

    @GetMapping("/editStudent")
    public ModelAndView editStudenProfile(@RequestParam(name = "id") long id){
        ModelAndView mav=new ModelAndView("reception/editstudent");

        User studentForm=new User();
      List<Subject>subjectList=  studentService.getSubjectList();
      Map<Long, String>subjectmap= new HashMap<>();

      subjectList.stream().forEach(subject -> subjectmap.put(subject.getId(),subject.getName()));
        if(studentRepository.getStudentById(id).isPresent()){
       studentForm=studentRepository.getStudentById(id).get();
            studentForm.setId(id);
           mav.addObject("studentForm",studentForm);
           mav.addObject("subjectmap",subjectmap);
            System.out.println("student id ="+studentForm.getId());
            System.out.println(" id =" +id);
        }

        //todo  bezi isler
        //studentRepository.get
        mav.addObject("id",id);
        return mav;
    }

    @PostMapping("/editStudent")
    public ModelAndView updateUser(Model model, @ModelAttribute("studentForm")
                @Validated StudentForm studentForm,
                                   BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        ModelAndView mav = new ModelAndView("reception/editstudent");
        User student = new User();
        if (bindingResult.hasErrors()) {

        } else {
           // logger.info("converting student form to student  = " +studentForm);

            student = ConvertUtil.convertForm(studentForm);
            student.setId(studentForm.getId());
           // logger.info("converting student form to student id = " +student.getId());

            try {
                studentService.updateStudent(student);
                redirectAttributes.addFlashAttribute("mesage", studentForm.getName() + "  update ugurla tamamlandi");
                mav.setViewName("redirect:/reception/");

            } catch (Exception e) {
                logger.error("error occurred while update student. id = "+student.getId());
                mav.setViewName("redirect:/reception/");
            }
        }
        return  mav;
    }
        @GetMapping("/excel")
    public ModelAndView exportStudentListToExcel(){
        List<User>studentFormList=studentService.getStudentLisByStatus();
        ModelAndView mav = new ModelAndView(new StudentListExcelView());
        mav.addObject("studentFormList",studentFormList);
        return mav;
    }



    @GetMapping("/message")
    public ModelAndView messagePage() {
        ModelAndView mav = new ModelAndView("reception/message");
        Notification notification = new Notification();
        mav.addObject("notification",notification);
        return mav;
    }



    @PostMapping("/message")
    public ModelAndView message(Model model , @ModelAttribute("notification")
            Notification notification, RedirectAttributes redirectAttributes
                                ) {
        ModelAndView mav = new ModelAndView("reception/message");
        mav.addObject("notification",notification);
        System.out.println("notification ="+notification);
        notificationService.insertNotification(notification);
        return mav;
    }
}
*/

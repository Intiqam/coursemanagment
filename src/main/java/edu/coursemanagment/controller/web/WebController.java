package edu.coursemanagment.controller.web;



import edu.coursemanagment.domain.Media;
import edu.coursemanagment.domain.User;

import edu.coursemanagment.form.RegistrationForm;
import edu.coursemanagment.service.CommonService;
import edu.coursemanagment.service.MediaService;
import edu.coursemanagment.service.StudentService;
import edu.coursemanagment.util.ConvertUtil;
import edu.coursemanagment.validator.RegistrationFormValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class WebController {


    @Autowired
    private RegistrationFormValidator registrationFormValidator;

    @Autowired
    private CommonService commonService;
    @Autowired
    private StudentService studentService;

    @Autowired
    private MediaService mediaService;

    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {

        Object target = dataBinder.getTarget();

        if (target != null && target.getClass() == RegistrationForm.class) {
            dataBinder.setValidator(registrationFormValidator);
        }

    }

    @GetMapping("/")
    public String viewInstructor() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String AvtoError() {
        return "access-denied";
    }


    @GetMapping("/home")
    public String login() {
        return "web/index";
    }

@GetMapping("/login-error")
public String LoginError(){
        return "web/loginError";
}






        @GetMapping("/register")
        public ModelAndView registerIndexPage () {
            ModelAndView mav = new ModelAndView("/web/register");
            RegistrationForm form = new RegistrationForm();
            mav.addObject("registrationForm", form);
            return mav;
        }

        @PostMapping("/register")
        public ModelAndView register (Model model,
                                      @ModelAttribute("registrationForm") @Validated RegistrationForm form,
                                      BindingResult result
    ){
            ModelAndView mav = new ModelAndView("/web/register");

            if (result.hasErrors()) {
                System.out.println("invalid registration form, forward to register page");
            } else {
                User user = ConvertUtil.convertRegistration(form);
                System.out.println("regidtreted user " + user);
                studentService.addStudent(user);
            }

            return mav;
        }

        @GetMapping("/reg-validate")
        public ResponseEntity<Boolean> registrationAjaxValidation (
                @RequestParam(name = "value") String value){
            boolean isValid = false;
            isValid = commonService.checkEmail(value);

            return new ResponseEntity<>(isValid, HttpStatus.OK);
        }


        @GetMapping("/forget")
        public String forgetPassword () {
            return "web/forgetpassword";
        }

        @GetMapping("/tutors")
        public String tutorPage () {
            return "web/tutors";
        }

        @GetMapping("/upload")
        public String uploadIndexPage () {
            return "upload";
        }

        @PostMapping("/upload")
        public ModelAndView processUpload (
                @RequestParam(name = "userId") String userId,
                @RequestParam(name = "image1") MultipartFile image1,
                @RequestParam(name = "image2", required = false) MultipartFile image2,
                @RequestParam(name = "image3", required = false) MultipartFile image3
    ){
            boolean success = true;
            try {
                if (!image1.isEmpty()) {
                    Media media = new Media();
                    media.setUserId(Integer.parseInt(userId));
                    media.setOriginalFileName(StringUtils.getFilename(image1.getOriginalFilename()));
                    media.setContentType(image1.getContentType());
                    media.setLength(image1.getSize());
                    media.setMobile(false);
                    media.setVideo(false);
                    media.setMain(false);
                    media.setInputStream(image1.getInputStream());
                    mediaService.addMedia(media);

                }
            } catch (Exception e) {
                success = false;
                System.out.println("conrtollerde catche de image ile bagli");
                e.printStackTrace();
            }
            ModelAndView mav = new ModelAndView("upload-result");
            mav.addObject("success", success);
            return mav;
        }


        @GetMapping("/media")
        public ResponseEntity<Resource> getMediaById (@RequestParam(name = "id") long id){
            ResponseEntity<Resource> entity = null;
            Optional<Media> optionalMedia = mediaService.getMediaById(id);
            if (optionalMedia.isPresent()) {
                Media media = optionalMedia.get();
                try {
                    Resource resource = new FileUrlResource(media.getFileName());
                    if (resource.exists()) {
                        HttpHeaders httpHeaders = new HttpHeaders();

                        httpHeaders.add("Content-Type", media.getContentType());
                        entity = new ResponseEntity<>(resource, httpHeaders, HttpStatus.OK);
                    } else {
                        entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } catch (Exception e) {
                    entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return entity;
        }

        @GetMapping("/logout")
    public String logout(HttpSession session){

        session.invalidate();

        return "login";
        }

    @GetMapping("/blocked")
    public String blocked() {
        return "web/blocked";
    }
    }


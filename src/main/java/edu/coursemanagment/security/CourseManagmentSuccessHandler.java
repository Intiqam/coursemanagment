package edu.coursemanagment.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
public class CourseManagmentSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    HttpSession session;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        httpServletRequest.getSession().setAttribute("user", userPrincipal.getUser());
        String page = userPrincipal.getUser().getRoleList().get(0).getSuccessPage();
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + page);

        session.setAttribute("userId",userPrincipal.getUser().getId());
        String email =userPrincipal.getUser().getContact().getEmail();
       // loginAttemptService.loginSucceeded(email);
    }
}

package edu.coursemanagment.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Component
public class CourseManagmentFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

        String message = "";
        String page="";
 response.sendRedirect(request.getContextPath()+"/login-error");



        if (exception instanceof BadCredentialsException){
            page="/login-error";

        } else if(exception.getMessage().equals("Blocked!")){
            message=exception.getMessage();
            System.out.println(message);
            page="/blocked";

        }

        request.setAttribute("errorMessage",message);
        response.sendRedirect(request.getContextPath() + page);
    }
}

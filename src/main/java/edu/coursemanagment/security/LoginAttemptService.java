package edu.coursemanagment.security;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;



import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


@Service
public class LoginAttemptService {


    private final int MAX_ATTEMPT = 5;

    private LoadingCache<String, Integer> attemptsCache;

    public LoginAttemptService() {
        super(); //for reseting time
        attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(2, TimeUnit.MINUTES)
                .build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(String key) {
                return 0;
            }

        });

    }

    public void loginFailed(String email) {
        int attempts = 0;

        try {
            attempts = attemptsCache.get(email);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        attempts++;
        attemptsCache.put(email, attempts);

    }


    public boolean isBlocked(String key) {
        boolean result = false;
        try {
            int countValue = attemptsCache.get(key);
            if (countValue >= MAX_ATTEMPT) {
                result = true;
            }
            return result;
        } catch (ExecutionException e) {
            e.printStackTrace();
            return result;
        }

    }

    public void loginSucceeded(String key) {
        if (key!=null){
        attemptsCache.invalidate(key);}
        else{
            System.out.println("cache is null");
        }

    }
}

package edu.coursemanagment.security;

import edu.coursemanagment.domain.User;
import edu.coursemanagment.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class CourseManagmentUserDetailsService implements UserDetailsService {

    private static final Logger logger = Logger.getLogger(CourseManagmentUserDetailsService.class.getName());

    @Autowired
    private StudentService studentService;

    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {


        UserPrincipal userPrincipal=null;
        if (username != null && !username.isEmpty()) {
            loginAttemptService.loginFailed(username);
        }else {
            //todo
        }

        if (loginAttemptService.isBlocked(username)) {
            logger.warn("attempts is more ");
            throw new RuntimeException("Blocked!");

        }

        Optional<User> optionalUser1 = studentService.getUserByUsername(username);
        Optional<User> optionalUser = studentService.getUserByEmail(username);

        if(optionalUser.isPresent()){
            User user= optionalUser.get();
            user.setRoleList(studentService.getRoleListByUsername(user.getId()));
            userPrincipal=new UserPrincipal(user);
        }else if (optionalUser1.isPresent()){
            User user= optionalUser1.get();
            user.setRoleList(studentService.getRoleListByUsername(user.getId()));
            userPrincipal=new UserPrincipal(user);
        }
        else{
            throw new UsernameNotFoundException("User "+username+" not found");
        }



        return userPrincipal;
    }
}

package edu.coursemanagment.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class CourseManagmentSecurityConifg extends WebSecurityConfigurerAdapter {

    @Autowired
    private CourseManagmentUserDetailsService courseManagmentUserDetailsService;

    @Autowired
    private CourseManagmentSuccessHandler courseManagmentSuccessHandler;
    @Autowired
    private CourseManagmentFailureHandler courseManagmentFailureHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(courseManagmentUserDetailsService);
        //bu axis necedir
        authenticationProvider.setPasswordEncoder(getPasswordEncoder());
        return authenticationProvider;
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
//                .antMatchers("/admin/**").hasRole("admin")
                .antMatchers("/student/**").hasAnyRole("student","admin")
                .antMatchers("/teacher/**").hasAnyRole("teacher","admin")
                .antMatchers("/").permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied")
                .and()
                .formLogin()
                .loginPage("/")
                .permitAll()
                .successHandler(courseManagmentSuccessHandler)
                .failureHandler(courseManagmentFailureHandler)
                .and()
                .rememberMe()
                .userDetailsService(courseManagmentUserDetailsService)
                .and()
                .logout()
                .logoutSuccessUrl("/")
                .permitAll();

    }
}

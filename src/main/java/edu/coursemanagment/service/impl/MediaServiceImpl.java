package edu.coursemanagment.service.impl;

import edu.coursemanagment.domain.Media;
import edu.coursemanagment.repository.MediaRepository;
import edu.coursemanagment.service.MediaService;
import edu.coursemanagment.util.FileUtility;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

@Service
public class MediaServiceImpl implements MediaService {
    private static final Logger logger = Logger.getLogger(MediaServiceImpl.class.getName());

    @Autowired
    private MediaRepository mediaRepository;
    @Value("${upload.folder}")
    private String uploadFolder;


    @Override
    public void addMedia(Media media) {
        try {
            String newFileName = String.format(
                    "coursemanagment-%d-%d-%d%s",

                    //todo media.getUserId
                    media.getUserId(),
                    LocalDateTime.now().toEpochSecond(ZoneOffset.UTC),
                    LocalDateTime.now().getNano(),
                    FileUtility.getFileExtension(media.getOriginalFileName())
            );
            String newFilePath = media.getUserId()+ File.separator +
                    newFileName;
            //  newFilePath = /home/student/autobazar-upload/3/autobazar-3-123123123.jpg

            logger.info("new file name = " + newFileName);
            logger.info("new file path = " + newFilePath);
            media.setFileName(newFilePath);

            Path path = Paths.get(uploadFolder+File.separator+newFilePath);

            if (!Files.exists(path.getParent())) {
                // no folder for this auto
                Files.createDirectory(path.getParent());
            }

            Files.copy(media.getInputStream(), path);
            //todo
            mediaRepository.addMedia(media);

        } catch (Exception e) {
            logger.error("Error saving new media ", e);
            throw new RuntimeException("Error saving new media ", e);
        }
    }

    @Override
    public Optional<Media> getMediaById(long id) {
      Optional<Media> optionalMedia = mediaRepository.getMediaById(id);
        return optionalMedia;
    }
}

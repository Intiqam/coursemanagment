package edu.coursemanagment.service.impl;

import edu.coursemanagment.domain.*;
import edu.coursemanagment.repository.SQLQueries;
import edu.coursemanagment.repository.StudentRepository;
import edu.coursemanagment.service.StudentService;

import edu.coursemanagment.domain.*;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public User addStudent(User user) {

        String passwordHash = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
        user.setPassword(passwordHash);
        user.setUsername(user.getName());
        return studentRepository.addStudent(user);
    }

    @Override
    public List<Subject> getSubjectList() {
        return studentRepository.getSubjectList();
    }

    @Override
    public List<TimeTable> getGroupsTimeTablesBySubjectId(long subjectId) {
        Map<Integer, Integer> lastPosition = new HashMap<>();

        return studentRepository.getGroupsTimeTablesBySubjectId(subjectId);
    }

    @Override
    public List<User> getTeachersBySubjectId(long subject_id) {
        return studentRepository.getTeachersBySubjectId(subject_id);
    }

    @Override
    public List<TimeTable> getEmptyTimesByRoomId(long roomId) {
        return studentRepository.getEmptyTimesByRoomId(roomId);
    }

    @Override
    public List<User> getStudentBySubjectId(long subject_id) {
        return studentRepository.getStudentBySubjectId(subject_id);
    }

    @Override
    public List<LocalTime> getFreeTimesByRoomId(long roomId, long weekday) {

        List<LocalTime> busyTimes = studentRepository.getFreeTimesByRoomId(roomId, weekday);
        List<LocalTime> localTimeList = new ArrayList<>();

        long muddet = 60;
        LocalTime start = LocalTime.parse("09:00");
        LocalTime end = LocalTime.parse("21:00");
        LocalTime offTime = LocalTime.parse("13:00");


        long hour = end.getHour() - start.getHour();

        for (int i = 0; i < hour * 60 / muddet; i++) {


            if (!start.equals(offTime)) {
                localTimeList.add(start);
            }
            start = start.plusMinutes(muddet);
        }
        localTimeList.removeAll(busyTimes);

        return localTimeList;
    }

    @Override
    public List<User> getStudentByGroupId(long groupId) {
        return studentRepository.getStudentByGroupId(groupId);
    }


    /*public DataTableResult getStudentListAjax(int draw, int start, int length) {
        DataTableResult dataTableResult = new DataTableResult();
        dataTableResult.setDraw(draw);
        int count = studentRepository.getStudentCount();
        dataTableResult.setRecordsFiltered(count);
        dataTableResult.setRecordsTotal(count);
        List<DataTableUser> dataTableUsers = studentRepository.getStudentListAjax(length, start);
        Object[][] data = new Object[dataTableUsers.size()][5];
        int counter = 0;
        for (DataTableUser user : dataTableUsers) {
            data[counter][0] = user.getId();
            String a = "";
            a = studentRepository.getSubjectList().stream().map(UserBase::getName).collect(Collectors.joining(","));
            data[counter][3] = a;
            data[counter][1] = user.getName();
            data[counter][2] = user.getSurname();
            data[counter][4] = "  <td class=\"text-right\">\n" +
                    "                      <a href=editStudent?id=" + user.getId() + " class=\"btn btn-default btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><i class=\"fa fa-pencil\"></i></a>\n" +
                    "                      <a href=\"#\" class=\"btn btn-danger btn-xs\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><i\n" +
                    "                                        class=\"fa fa-times\"></i></a>\n" +
                    "                    </td>";
            counter++;
        }
        dataTableResult.setData(data);

        return dataTableResult;
    }*/

    @Override
    public void addGroup(Group group) {
        studentRepository.addGroup(group);
    }

    @Override
    public List<Room> getRooms() {
        return studentRepository.getRooms();
    }

    @Override
    public List<Group> getGroupBySubjectId(long subjectId) {
        return studentRepository.getGroupBySubjectId(subjectId);
    }

    @Override
    public Optional<User> getStudentById(long id) {
        return studentRepository.getStudentById(id);

    }

    @Override
    public int getStudentCount() {
        return studentRepository.getStudentCount();
    }

    @Override
    public int getTeacherCount() {
        return studentRepository.getTeacherCount();
    }

    @Override
    public void addAttdendance(List<Attandance> attandanceList, long group_id) {
        studentRepository.addAttdendance(attandanceList, group_id);
    }

    @Override
    public List<Subject> getSubjectListByStudentId(long id) {
        return studentRepository.getSubjectListByStudentId(id);
    }

    @Override
    public void updateStudent(User user) {
        studentRepository.updateStudent(user);
    }

    @Override
    public List<Category> getCategories() {
        return studentRepository.getCategories();
    }

    @Override
    public void deleteSubjectById(long id) {
        studentRepository.deleteSubjectById(id);
    }

    @Override
    public Optional<Subject> getSubjectById(long id) {
        return studentRepository.getSubjectById(id);
    }

    @Override
    public List<User> getTeachers() {
        return studentRepository.getTeachers();
    }

    @Override
    public void addNewSubject(Subject subject) {
        studentRepository.addNewSubject(subject);
    }

    @Override
    public List<DataTableUser> getStudentListAjax(int start, int length) {
        return studentRepository.getStudentListAjax(start, length);
    }

    @Override
    public List<User> getStudentLisByStatus() {
        return studentRepository.getStudentLisByStatus();
    }


    @Override
    public Optional<User> getUserByUsername(String username) {
        return studentRepository.getUserByUsername(username);
    }


    @Override
    public Optional<User> getUserByEmail(String email) {
        return studentRepository.getUserByEmail(email);
    }

    @Override
    public List<Role> getRoleListByUsername(long userId) {
        return studentRepository.getRoleListByUsername(userId);
    }


    @Override
    public List<Group> getAllGroups() {
        return studentRepository.getAllGroups();
    }

    @Override
    public DataTable getPersonDataTable(DataTable dataTableRequest) {
        DataTable dataTable = new DataTable();
        dataTable.setDraw(dataTableRequest.getDraw());
        dataTable.setRecordsTotal(getStudentCount());

        Map<Integer, String> map = new HashMap<>();

        map.put(0, "u.id");
        map.put(1, "u.name");
        map.put(2, "u.surname");
        map.put(3, "u.gender");
        map.put(4, "u.class");
        map.put(5, "u.section");
        map.put(6, "u.dob");
        map.put(7, "c.tel_mobil");

        String sql = SQLQueries.GET_STUDENTLIST_WITH_PAGING
                .replace("{order}", map.get(dataTableRequest.getSortColumn()))
                .replace("{direction}", dataTableRequest.getSortDirection());
        List<User> userList = studentRepository.getUserListPaging(sql, dataTableRequest.getStart(), dataTableRequest.getLength(), dataTableRequest.getSearch());
            dataTable.setRecordsFiltered(userList.size());
            List<Map<String,Object>> result=new ArrayList<>();
        for (int i = 0; i <userList.size() ; i++) {
            Map<String,Object> item=new LinkedHashMap<>();
            item.put("id",userList.get(i).getId());
            item.put("name",userList.get(i).getName());
            item.put("surname",userList.get(i).getSurname());
            item.put("gender",userList.get(i).getGender());
            item.put("class",userList.get(i).getGrade());
            item.put("section",userList.get(i).getSection());
            item.put("birthdate",userList.get(i).getBirthDate());
            item.put("phone",userList.get(i).getContact().getTelMobile());
        result.add(item);
        }
        dataTable.setData(result);
        return dataTable;
    }
}

package edu.coursemanagment.service.impl;


import edu.coursemanagment.domain.Notification;
import edu.coursemanagment.domain.NotificationType;
import edu.coursemanagment.repository.NotificationRepository;
import edu.coursemanagment.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public void insertNotification(Notification notification) {
        notificationRepository.insertNotification(notification);
    }

    @Override
    public List<Notification> getPendingNotifications(NotificationType notificationType, int limit) {
        return notificationRepository.getPendingNotificatios(notificationType, limit);
    }

    @Override
    public void updateNotification(Notification notification) {
        notificationRepository.updateNotification(notification);
    }

    @Override
    public Optional<Notification> getNotificationById(long id) {
        return Optional.empty();
    }
}

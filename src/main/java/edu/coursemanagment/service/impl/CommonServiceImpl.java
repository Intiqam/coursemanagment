package edu.coursemanagment.service.impl;

import edu.coursemanagment.repository.CommonRepository;
import edu.coursemanagment.service.CommonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CommonServiceImpl implements CommonService {

    @Autowired
    CommonRepository commonRepository;
    @Override
    public boolean checkEmail(String email) {

        return commonRepository.checkEmail(email);
    }
}

package edu.coursemanagment.service;


import edu.coursemanagment.domain.*;
import edu.coursemanagment.domain.*;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;


public interface StudentService {

    User addStudent(User user);

    List<Subject> getSubjectList();

    List<Group> getGroupBySubjectId(long subjectId);

    List<TimeTable> getGroupsTimeTablesBySubjectId(long subjectId);

    List<User> getTeachersBySubjectId(long subject_id);

    List<TimeTable> getEmptyTimesByRoomId(long roomId);

    List<Room> getRooms();

    List<User> getStudentBySubjectId(long subject_id);

    List<User> getStudentByGroupId(long groupId);

    List<LocalTime> getFreeTimesByRoomId(long roomId, long weekday);

    void addGroup(Group group);



    void addAttdendance(List<Attandance> attandanceList, long group_id);

    int getStudentCount();

    int getTeacherCount();


    Optional<User> getStudentById(long id);

    List<Subject> getSubjectListByStudentId(long id);

    void updateStudent(User user);

    List<Role> getRoleListByUsername(long userId);

    Optional<User> getUserByUsername(String username);

    List<User> getStudentLisByStatus();

    List<DataTableUser> getStudentListAjax(int start, int length);

    void addNewSubject(Subject subject);

    Optional<Subject> getSubjectById(long id);

    void deleteSubjectById(long id);

    Optional<User> getUserByEmail(String email);

    List<Category> getCategories();

    List<User> getTeachers();
    DataTable getPersonDataTable(DataTable dataTableRequest);
    List<Group> getAllGroups();




}

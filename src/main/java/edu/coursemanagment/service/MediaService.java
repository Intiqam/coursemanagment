package edu.coursemanagment.service;


import edu.coursemanagment.domain.Media;

import java.util.Optional;

public interface MediaService {
    void addMedia(Media media);
    Optional<Media> getMediaById(long id);
}

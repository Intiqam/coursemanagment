package edu.coursemanagment.service;



import edu.coursemanagment.domain.Notification;
import edu.coursemanagment.domain.NotificationType;

import java.util.List;
import java.util.Optional;

public interface NotificationService {
    void  insertNotification(Notification notification);

    List<Notification> getPendingNotifications(NotificationType notificationType, int limit);

    void updateNotification(Notification notification);

    Optional<Notification> getNotificationById(long id);
}

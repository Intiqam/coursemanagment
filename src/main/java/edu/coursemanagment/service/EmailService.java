package edu.coursemanagment.service;

public interface EmailService {
    void sendEmail(String from, String to, String subject, String body);
}

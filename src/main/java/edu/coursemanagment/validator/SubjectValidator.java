package edu.coursemanagment.validator;

import edu.coursemanagment.domain.Subject;
import edu.coursemanagment.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SubjectValidator implements Validator {
    @Autowired
    private StudentService studentService;
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass== Subject.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
            Subject subject= (Subject) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","subjectForm.name.required");
        if (!errors.hasFieldErrors("name")){
            if (studentService.getSubjectList().stream().anyMatch(subject1 -> subject1.getName().equals(subject.getName()))){
                errors.rejectValue("name","subjectForm.name.dublicate");
            }
        }

    }
}

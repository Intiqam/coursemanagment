package edu.coursemanagment.validator;


import edu.coursemanagment.form.StudentForm;
import edu.coursemanagment.util.RegexValidation;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static edu.coursemanagment.validator.ValidationConstants.*;


@Component
public class StudentFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {

        return clazz == StudentForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {

        StudentForm studentForm = (StudentForm) target;


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "studentForm.name.required");
        if (!errors.hasFieldErrors("name")){
            if (!GenericValidator.isInRange(studentForm.getName().length(), NAME_MIN_LENGTH, NAME_MAX_LENGTH)) {
                errors.rejectValue("name", "studentForm.name.length");
            }
            if(!RegexValidation.alphaRegex(studentForm.getName())) {
                errors. rejectValue("name", "studentForm.name.matchRegexp");
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "studentForm.surname.required");
        if (!errors.hasFieldErrors("surname")){
            if (!GenericValidator.isInRange(studentForm.getSurname().length(), SURNAME_MIN_LENGTH, SURNAME_MAX_LENGTH)) {
                errors.rejectValue("surname", "studentForm.surname.length");
            }
            if(!RegexValidation.alphaRegex(studentForm.getSurname())) {
                studentForm.getSurname();

                errors. rejectValue("surname", "studentForm.surname.matchRegexp");
            }
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fatherName", "studentForm.fatherName.required");
        if (!errors.hasFieldErrors("fatherName")){
            if (!GenericValidator.isInRange(studentForm.getFatherName().length(), FATHER_NAME_MIN_LENGTH, FATHER_NAME_MAX_LENGTH)) {
                errors.rejectValue("fatherName", "studentForm.fatherName.length");
            }
            if(!RegexValidation.alphaRegex(studentForm.getFatherName())) {
                errors. rejectValue("fatherName", "studentForm.fatherName.matchRegexp");
            }
        }


       ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contact.email", "studentForm.email.required");
            if (!errors.hasFieldErrors("contact.email")){
                if (!GenericValidator.isEmail(studentForm.getContact().getEmail())) {
                    errors.rejectValue("contact.email", "studentForm.email.invalid");
                }
            }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"birthDate","studentForm.birthDate.required");
            if (!errors.hasFieldErrors("birthDate")){
                if(!GenericValidator.isDate(studentForm.getBirthDate(),"dd/MM/yyyy",true)){
                    errors.rejectValue("birthDate","studentForm.birthDate.invalid");
                }
            }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"fincod","studentForm.fincode.required");
        if (!errors.hasFieldErrors("fincod")){
            if (!GenericValidator.isInRange(studentForm.getFincod().length(),CODE_LENGTH,CODE_LENGTH)){
                errors.rejectValue("fincod","studentForm.fincode.length");
            }
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"section","studentForm.section.required");
        if (!errors.hasFieldErrors("section")){
            if (studentForm.getSection()==-1){
                errors.rejectValue("section","studentForm.section.required");
            }
        }


       ValidationUtils.rejectIfEmptyOrWhitespace(errors,"contact.address","studentForm.address.invalid");
       ValidationUtils.rejectIfEmptyOrWhitespace(errors,"contact.telMobile","studentForm.mobil.invalid");
      // ValidationUtils.rejectIfEmptyOrWhitespace(errors,"contact.telHome","studentForm.evTel.invalid");
    }
}

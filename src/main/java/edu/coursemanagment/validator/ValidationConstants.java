package edu.coursemanagment.validator;

public class ValidationConstants {

    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 45;

    public static final int SURNAME_MIN_LENGTH = 3;
    public static final int SURNAME_MAX_LENGTH = 45;

    public static final int FATHER_NAME_MIN_LENGTH = 3;
    public static final int FATHER_NAME_MAX_LENGTH = 45;

    public static final int EMAIL_MIN_LENGTH = 6;
    public static final int EMAIL_MAX_LENGTH = 200;

    public static final int PASSWORD_MIN_LENGTH = 6;
    public static final int PASSWORD_MAX_LENGTH = 20;

    public static final int CODE_LENGTH = 7;
}

package edu.coursemanagment.validator;


import edu.coursemanagment.form.RegistrationForm;
import edu.coursemanagment.service.CommonService;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class RegistrationFormValidator implements Validator {

    @Autowired
    private CommonService commonService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == RegistrationForm.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationForm form = (RegistrationForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "registrationForm.name.required");

        if(!GenericValidator.isInRange(form.getName().length(), ValidationConstants.NAME_MIN_LENGTH, ValidationConstants.NAME_MAX_LENGTH)) {
            errors. rejectValue("name", "registrationForm.name.length");
        }
        if(!GenericValidator.matchRegexp(form.getName(),"\"(?<=\\\\s|^)[a-zA-Z]*(?=[.,;:]?\\\\s|$)\"")) {
            errors. rejectValue("name", "registrationForm.name.matchRegexp");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "registrationForm.surname.required");
        if(!GenericValidator.isInRange(form.getSurname().length(), ValidationConstants.NAME_MIN_LENGTH, ValidationConstants.NAME_MAX_LENGTH)) {
            errors.rejectValue("surname", "registrationForm.surname.length");
        }

        if(!GenericValidator.matchRegexp(form.getSurname(),"\"(?<=\\\\s|^)[a-zA-Z]*(?=[.,;:]?\\\\s|$)\"")) {
            errors. rejectValue("surname", "registrationForm.surname.matchRegexp");
        }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "registrationForm.email.required");
        if(!GenericValidator.isEmail(form.getEmail())) {
            errors.rejectValue("email", "registrationForm.email.invalid");
        } else {
            // // todo check email is not dublicate
            if(commonService.checkEmail(form.getEmail())) {
                errors.rejectValue("email", "registrationForm.email.dublicate");
            }
        }


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "registrationForm.password.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordConfirmation", "registrationForm.passwordConfirmation.required");

        if(!errors.hasFieldErrors("password") && !errors.hasFieldErrors("passwordConfirmation")) {
            if(!GenericValidator.isInRange(form.getPassword().length(), ValidationConstants.PASSWORD_MIN_LENGTH, ValidationConstants.PASSWORD_MAX_LENGTH)) {
                errors.rejectValue("password", "registrationForm.password.length");
            } else {
              if(!form.getPassword().equals(form.getPasswordConfirmation())) {
                  errors.rejectValue("passwordConfirmation", "registrationForm.password.mismatch");
              }
            }
        }
    }
}

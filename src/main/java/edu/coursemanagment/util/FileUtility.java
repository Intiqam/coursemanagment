package edu.coursemanagment.util;

public class FileUtility {

    public static String getFileExtension(String filename) {
        String extension = "";
        if(filename != null) {
            int pos = filename.lastIndexOf(".");
            if (pos != -1) {
                extension = filename.substring(pos);
            }
        }
        return extension;
    }

/*    public static String getUploadedFileName(Part part) {

        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(content.indexOf("=") + 2, content.length() - 1);
            }
        }
        return part.getName();

    }*/
}

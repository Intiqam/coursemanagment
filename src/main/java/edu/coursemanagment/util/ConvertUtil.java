package edu.coursemanagment.util;



import edu.coursemanagment.domain.*;
import edu.coursemanagment.form.RegistrationForm;
import edu.coursemanagment.form.StudentForm;
import edu.coursemanagment.domain.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ConvertUtil {

    public static User convertForm(StudentForm studentForm){
        User user = new User();
        user.setContact(new Contact());
        user.setCategory(new Category());
        user.setBranch(new Branch());
        List<Subject>subjectList=new ArrayList<>();

        Arrays.stream(studentForm.getSubjectList()).forEach(id->{

            Subject subject=new Subject();
            subject.setId(id);
            subjectList.add(subject);

        });
        user.setPassword(studentForm.getPassword());
        user.setName(studentForm.getName());
        user.setSurname(studentForm.getSurname());
        user.setGender(studentForm.getGender());
        user.setFatherName(studentForm.getFatherName());
        user.setBirthDate(studentForm.getBirthDate());
        user.getContact().setTelHome(studentForm.getContact().getTelHome());
        user.getContact().setTelMobile(studentForm.getContact().getTelMobile());
        user.getContact().setAddress(studentForm.getContact().getAddress());
        user.getContact().setEmail(studentForm.getContact().getEmail());
        user.setSection(studentForm.getSection());
        user.setFincod(studentForm.getFincod());
        user.setGrade(studentForm.getGrade());
        user.setIndex(studentForm.getIndex());
        user.setSchool(studentForm.getSchool());
        user.getCategory().setId(studentForm.getCategory());
        user.getBranch().setId(studentForm.getBranch());
        user.setSlot(studentForm.getSlot());
        user.setSubjectList(subjectList);
        user.setPassword(studentForm.getPassword());


        return user;
    }

    public static User convertRegistration(RegistrationForm form){
        User user=new User();
        user.setName(form.getName());
        user.setSurname(form.getSurname());
        Contact contact=new Contact();
        user.setContact(contact);
        user.getContact().setEmail(form.getEmail());
        user.setPassword(form.getPassword());
 return user;
    }




}

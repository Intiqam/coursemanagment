package edu.coursemanagment.schedule;


import edu.coursemanagment.domain.Notification;
import edu.coursemanagment.domain.NotificationType;
import edu.coursemanagment.service.EmailService;
import edu.coursemanagment.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SafakSchedule {
    @Autowired
    private EmailService emailService;
    @Autowired
    private NotificationService notificationService;

    @Scheduled(fixedRate = 5 * 10000)
    public void processNotificationQueue() {
        List<Notification> notificationList = notificationService.getPendingNotifications(NotificationType.EMAIL, 10);
        for (Notification notification : notificationList) {
            processEmailNotification(notification);
        }
    }

    public void processEmailNotification(Notification notification) {
        notification.setNotificationStatus(1);
        notificationService.updateNotification(notification);
        try {
            emailService.sendEmail(notification.getFrom(), notification.getTo(), notification.getSubject(), notification.getContent());
            notification.setNotificationStatus(2);
        } catch (Exception e) {

            notification.setNotificationStatus(3);
            notification.setLogData(e.getMessage());
        }

        notificationService.updateNotification(notification);
    }
}

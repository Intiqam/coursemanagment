package edu.coursemanagment.domain;

import java.util.Arrays;

public enum Role {

    STUDENT(1),
    TEACHER(2),
    ADMIN(3),
    RECEPTION(4);

    private int value;
    private String name;
    private String successPage;

    Role(int value) {
        this.value = value;
    }

     Role(int value, String name, String successPage) {
        this.value = value;
        this.name = name;
        this.successPage = successPage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuccessPage() {
        return successPage;
    }

    public void setSuccessPage(String successPage) {
        this.successPage = successPage;
    }

    public int getValue() {
        return value;
    }

    public static Role fromValue(int id) {
        Role r = null;
//        if(id == 1) {
//            r = Role.AUTOBAZAR_USER;
//        } else if(id == 2) {
//            r = Role.AUTOSHOP_ADMIN;
//        } else if(id == 3) {
//            r = Role.AUTOSHOP_MODERATOR;
//        } else if(id == 4) {
//            r = AUTOBAZAR_ADMIN;
//        } else if(id == 5) {
//            r = AUTOBAZAR_MODERATOR;
//        } else {
//            throw new IllegalArgumentException("Unknown user role value " + id);
//        }

        r = Arrays.stream(values())
                .filter(f -> f.value == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Unknown user role value " + id));

        return r;
    }

    @Override
    public String toString() {
        return "Role{" +
                "value=" + value +
                ", name='" + name + '\'' +
                ", successPage='" + successPage + '\'' +
                '}';
    }
}

package edu.coursemanagment.domain;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class Group extends UserBase {
    private Subject subject;
    private Long teacherId;
    private String teacherFullName;
    private Branch branch;
    private Room room;
    private List<TimeTable> timeTables;
    private String[] days;
    private String startTime;
    private String endTime;
    private int [] studentId;

    public Group() {
    }

    public Group(Subject subject, Long teacherId, String teacherFullName, Branch branch, Room room, List<TimeTable> timeTables, String [] days, String startTime, String endTime, int[] studentId) {
        this.subject = subject;
        this.teacherId = teacherId;
        this.teacherFullName = teacherFullName;
        this.branch = branch;
        this.room = room;
        this.timeTables = timeTables;
        this.days = days;
        this.startTime = startTime;
        this.endTime = endTime;
        this.studentId = studentId;
    }

    public Group(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, Subject subject, Long teacherId, String teacherFullName, Branch branch, Room room, List<TimeTable> timeTables, String[] days, String startTime, String endTime, int[] studentId) {
        super(id, name, idate, udate, status);
        this.subject = subject;
        this.teacherId = teacherId;
        this.teacherFullName = teacherFullName;
        this.branch = branch;
        this.room = room;
        this.timeTables = timeTables;
        this.days = days;
        this.startTime = startTime;
        this.endTime = endTime;
        this.studentId = studentId;
    }

    public Group(String name, Subject subject, Long teacherId, String teacherFullName, Branch branch, Room room, List<TimeTable> timeTables, String[] days, String startTime, String endTime, int[] studentId) {
        super(name);
        this.subject = subject;
        this.teacherId = teacherId;
        this.teacherFullName = teacherFullName;
        this.branch = branch;
        this.room = room;
        this.timeTables = timeTables;
        this.days = days;
        this.startTime = startTime;
        this.endTime = endTime;
        this.studentId = studentId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName) {
        this.teacherFullName = teacherFullName;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public List<TimeTable> getTimeTables() {
        return timeTables;
    }

    public void setTimeTables(List<TimeTable> timeTables) {
        this.timeTables = timeTables;
    }

    public String[] getDays() {
        return days;
    }

    public void setDays(String[] days) {
        this.days = days;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int[] getStudentId() {
        return studentId;
    }

    public void setStudentId(int[] studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "Group{" +
                "subject=" + subject +
                ", teacherId=" + teacherId +
                ", teacherFullName='" + teacherFullName + '\'' +
                ", branch=" + branch +
                ", room=" + room +
                ", timeTables=" + timeTables +
                ", days=" + Arrays.toString(days) +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", studentId=" + Arrays.toString(studentId) +
                "} " + super.toString();
    }

}

package edu.coursemanagment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Category extends UserBase implements Serializable {
    public Category() {
    }

    public Category(long id, String name, LocalDateTime idate, LocalDateTime udate, int status) {
        super(id, name, idate, udate, status);
    }

    @Override
    public String toString() {
        return "Category{} " + super.toString();
    }
}

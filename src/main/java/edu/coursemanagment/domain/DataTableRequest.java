package edu.coursemanagment.domain;

public class DataTableRequest {
    private int draw;
    private int start;
    private int length;
    private String search;
    private int sortColumn;
    private String sortDirection;

    public DataTableRequest() {
    }

    public DataTableRequest(int draw, int start, int length, String search, int sortColumn, String sortDirection) {
        this.draw = draw;
        this.start = start;
        this.length = length;
        this.search = search;
        this.sortColumn = sortColumn;
        this.sortDirection = sortDirection;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(int sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    @Override
    public String toString() {
        return "DataTableRequest{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", search='" + search + '\'' +
                ", sortColumn=" + sortColumn +
                ", sortDirection='" + sortDirection + '\'' +
                '}';
    }
}

package edu.coursemanagment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class User extends UserBase implements Serializable {
    private String surname;
    private String fatherName;
    private int gender;
    private String birthDate;
    private Contact contact;
    private int section;
    private String fincod;
    private String grade;
    private String index;
    private String school;
    private String diplomNo;
    private Category category;
    private Branch branch;
    private List<Role> roleList;
    private List<Subject> subjectList;
    private String teachername;
    private int slot;
    private String additionalInform;
    private String username;
    private String password;



    public User() {
        new Contact();
        new Category();
    }

    public User(String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, String diplomNo, Category category, Branch branch, List<Role> roleList, List<Subject> subjectList, String teachername, int slot, String additionalInform, String username, String password) {
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.diplomNo = diplomNo;
        this.category = category;
        this.branch = branch;
        this.roleList = roleList;
        this.subjectList = subjectList;
        this.teachername = teachername;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.username = username;
        this.password = password;
    }

    public User(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, String diplomNo, Category category, Branch branch, List<Role> roleList, List<Subject> subjectList, String teachername, int slot, String additionalInform, String username, String password) {
        super(id, name, idate, udate, status);
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.diplomNo = diplomNo;
        this.category = category;
        this.branch = branch;
        this.roleList = roleList;
        this.subjectList = subjectList;
        this.teachername = teachername;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.username = username;
        this.password = password;
    }

    public User(String name, String surname, String fatherName, int gender, String birthDate, Contact contact, int section, String fincod, String grade, String index, String school, String diplomNo, Category category, Branch branch, List<Role> roleList, List<Subject> subjectList, String teachername, int slot, String additionalInform, String username, String password) {
        super(name);
        this.surname = surname;
        this.fatherName = fatherName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.contact = contact;
        this.section = section;
        this.fincod = fincod;
        this.grade = grade;
        this.index = index;
        this.school = school;
        this.diplomNo = diplomNo;
        this.category = category;
        this.branch = branch;
        this.roleList = roleList;
        this.subjectList = subjectList;
        this.teachername = teachername;
        this.slot = slot;
        this.additionalInform = additionalInform;
        this.username = username;
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public String getFincod() {
        return fincod;
    }

    public void setFincod(String fincod) {
        this.fincod = fincod;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getDiplomNo() {
        return diplomNo;
    }

    public void setDiplomNo(String diplomNo) {
        this.diplomNo = diplomNo;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    public String getTeachername() {
        return teachername;
    }

    public void setTeachername(String teachername) {
        this.teachername = teachername;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public String getAdditionalInform() {
        return additionalInform;
    }

    public void setAdditionalInform(String additionalInform) {
        this.additionalInform = additionalInform;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "surname='" + surname + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", gender=" + gender +
                ", birthDate='" + birthDate + '\'' +
                ", contact=" + contact +
                ", section=" + section +
                ", fincod='" + fincod + '\'' +
                ", grade='" + grade + '\'' +
                ", index='" + index + '\'' +
                ", school='" + school + '\'' +
                ", diplomNo='" + diplomNo + '\'' +
                ", category=" + category +
                ", branch=" + branch +
                ", roleList=" + roleList +
                ", subjectList=" + subjectList +
                ", teachername='" + teachername + '\'' +
                ", slot=" + slot +
                ", additionalInform='" + additionalInform + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                "} " + super.toString();
    }
}

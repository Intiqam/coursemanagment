package edu.coursemanagment.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public  class UserBase {
    private long id;
    private String name;
    private LocalDateTime idate;
    private LocalDateTime udate;
    private int status;

    public UserBase() {
    }

    public UserBase(long id, String name, LocalDateTime idate, LocalDateTime udate, int status) {
        this.id = id;
        this.name = name;
        this.idate = idate;
        this.udate = udate;
        this.status = status;
    }
    public UserBase( String name) {
        this.name = name;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getIdate() {
        return idate;
    }

    public void setIdate(LocalDateTime idate) {
        this.idate = idate;
    }

    public LocalDateTime getUdate() {
        return udate;
    }

    public void setUdate(LocalDateTime udate) {
        this.udate = udate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserBase userBase = (UserBase) o;
        return id == userBase.id &&
                status == userBase.status &&
                name.equals(userBase.name) &&
                idate.equals(userBase.idate) &&
                udate.equals(userBase.udate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, idate, udate, status);
    }

    @Override
    public String toString() {
        return "UserBase{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idate=" + idate +
                ", udate=" + udate +
                ", status=" + status +
                '}';
    }
}

package edu.coursemanagment.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDateTime;

@Component
public class Notification extends UserBase implements Serializable {
    private String from;
    private String to;
    private String subject;
    private String content;
    private NotificationType type;
    private int notificationStatus;
    private String source;
    private String logData;

    public Notification() {
    }

    public Notification(String from, String to, String subject, String content, NotificationType type, int notificationStatus, String source, String logData) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.type = type;
        this.notificationStatus = notificationStatus;
        this.source = source;
        this.logData = logData;
    }

    public Notification(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, String from, String to, String subject, String content, NotificationType type, int notificationStatus, String source, String logData) {
        super(id, name, idate, udate, status);
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.type = type;
        this.notificationStatus = notificationStatus;
        this.source = source;
        this.logData = logData;
    }

    public Notification(String name, String from, String to, String subject, String content, NotificationType type, int notificationStatus, String source, String logData) {
        super(name);
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.type = type;
        this.notificationStatus = notificationStatus;
        this.source = source;
        this.logData = logData;
    }

    public Notification(String name) {
        super(name);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public int getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(int notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLogData() {
        return logData;
    }

    public void setLogData(String logData) {
        this.logData = logData;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", type=" + type +
                ", notificationStatus=" + notificationStatus +
                ", source='" + source + '\'' +
                ", logData='" + logData + '\'' +
                "} " + super.toString();
    }
}

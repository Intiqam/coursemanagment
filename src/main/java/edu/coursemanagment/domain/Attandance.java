package edu.coursemanagment.domain;


import java.time.LocalDate;
import java.time.LocalDateTime;


public class Attandance {
    private User user;
    private Long groupMemberId;
    private Long groupId;
    private Long  state;
    private Long  userId;
    private LocalDate idate;


    public Attandance() {
    }

    public Attandance(Long groupMemberId, Long state) {
        this.groupMemberId = groupMemberId;
        this.state = state;
    }

    public Attandance(Long groupMemberId, Long groupId, Long state, Long userId, LocalDate idate) {
        this.groupMemberId = groupMemberId;
        this.groupId = groupId;
        this.state = state;
        this.userId = userId;
        this.idate = idate;
    }

    public Attandance(String k, String[] v) {
        this.groupMemberId=Long.parseLong(k);
        this.state=Long.parseLong(v[0]);
    }

    public Attandance(long id, String[] strings) {
        this.groupMemberId=id;
        this.state=Long.valueOf(strings[0]);
    }

    public Long getGroupMemberId() {
        return groupMemberId;
    }

    public void setGroupMemberId(Long groupMemberId) {
        this.groupMemberId = groupMemberId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getIdate() {
        return idate;
    }

    public void setIdate(LocalDate idate) {
        this.idate = idate;
    }
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Attandance{" +
                "user=" + user +
                ", groupMemberId=" + groupMemberId +
                ", groupId=" + groupId +
                ", state=" + state +
                ", userId=" + userId +
                ", idate=" + idate +
                '}';
    }
}

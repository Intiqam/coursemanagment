package edu.coursemanagment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class TimeTable extends UserBase implements Serializable {
    private long groupId;
    private String roomName;
    private String groupName;
    private String weekDay;
    private String startTime;
    private String endTime;

    public TimeTable(String roomName, String groupName, String weekDay, String startTime, String endTime) {
        this.roomName = roomName;
        this.groupName = groupName;
        this.weekDay = weekDay;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeTable( String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeTable(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, String roomName, String groupName, String weekDay, String startTime, String endTime) {
        super(id, name, idate, udate, status);
        this.roomName = roomName;
        this.groupName = groupName;
        this.weekDay = weekDay;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeTable(String name, String roomName, String groupName, String weekDay, String startTime, String endTime) {
        super(name);
        this.roomName = roomName;
        this.groupName = groupName;
        this.weekDay = weekDay;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public TimeTable() {
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(String weekDay) {
        this.weekDay = weekDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TimeTable timeTable = (TimeTable) o;
        return weekDay == timeTable.weekDay &&
                roomName.equals(timeTable.roomName) &&
                groupName.equals(timeTable.groupName) &&
                startTime.equals(timeTable.startTime) &&
                endTime.equals(timeTable.endTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), roomName, groupName, weekDay, startTime, endTime);
    }

    @Override
    public String toString() {
        return "TimeTable{" +
                "roomName='" + roomName + '\'' +
                ", groupName='" + groupName + '\'' +
                ", weekDay=" + weekDay +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                "} " + super.toString();
    }
}

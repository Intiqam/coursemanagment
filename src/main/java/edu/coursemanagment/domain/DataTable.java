package edu.coursemanagment.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataTable {
        private int draw;
        @JsonIgnore
        private int start;
        @JsonIgnore
        private int length;
        @JsonIgnore
        private int sortColumn;
        @JsonIgnore
        private String sortDirection;
        @JsonIgnore
        private String search;

        private long recordsTotal;
        private long recordsFiltered;
        private List<Map<String, Object>> data;

        public DataTable() {
            this.draw = 0;
            this.start = 0;
            this.length = 0;
            this.start = 0;
            this.sortColumn = 0;
            this.sortDirection = "asc";
            this.search = "";

            this.recordsTotal = 0;
            this.recordsFiltered = 0;
            this.data = new ArrayList<>();
        }

        public DataTable(int draw, int start, int length, int sortColumn, String sortDirection, String search) {
            this.draw = draw;
            this.start = start;
            this.length = length;
            this.sortColumn = sortColumn;
            this.sortDirection = sortDirection;
            this.search = search;
        }

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public int getSortColumn() {
            return sortColumn;
        }

        public void setSortColumn(int sortColumn) {
            this.sortColumn = sortColumn;
        }

        public String getSortDirection() {
            return sortDirection;
        }

        public void setSortDirection(String sortDirection) {
            this.sortDirection = sortDirection;
        }

        public String getSearch() {
            return search;
        }

        public void setSearch(String search) {
            this.search = search;
        }

        public int getDraw() {
            return draw;
        }

        public void setDraw(int draw) {
            this.draw = draw;
        }

        public long getRecordsTotal() {
            return recordsTotal;
        }

        public void setRecordsTotal(long recordsTotal) {
            this.recordsTotal = recordsTotal;
        }

        public long getRecordsFiltered() {
            return recordsFiltered;
        }

        public void setRecordsFiltered(long recordsFiltered) {
            this.recordsFiltered = recordsFiltered;
        }

        public List<Map<String, Object>> getData() {
            return data;
        }

        public void setData(List<Map<String, Object>> data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "DataTableResult{" +
                    "draw=" + draw +
                    ", start=" + start +
                    ", length=" + length +
                    ", sortColumn=" + sortColumn +
                    ", sortDirection='" + sortDirection + '\'' +
                    ", search='" + search + '\'' +
                    ", recordsTotal=" + recordsTotal +
                    ", recordsFiltered=" + recordsFiltered +
                    ", data=" + data +
                    '}';
        }
    }



package edu.coursemanagment.domain;

import java.util.Objects;

public class Contact {
    private String telHome;
    private String telMobile;
    private String address;
    private String email;

    public Contact() {
    }

    public Contact(String telHome, String telMobile, String address, String email) {
        this.telHome = telHome;
        this.telMobile = telMobile;
        this.address = address;
        this.email = email;
    }

    public String getTelHome() {
        return telHome;
    }

    public void setTelHome(String telHome) {
        this.telHome = telHome;
    }

    public String getTelMobile() {
        return telMobile;
    }

    public void setTelMobile(String telMobile) {
        this.telMobile = telMobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "telHome='" + telHome + '\'' +
                ", telMobile='" + telMobile + '\'' +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return telHome.equals(contact.telHome) &&
                telMobile.equals(contact.telMobile) &&
                address.equals(contact.address) &&
                email.equals(contact.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(telHome, telMobile, address, email);
    }
}

package edu.coursemanagment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Branch extends UserBase implements Serializable {
private Contact contact;

    public Branch(Contact contact) {
        this.contact = contact;
    }

    public Branch(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, Contact contact) {
        super(id, name, idate, udate, status);
        this.contact = contact;
    }

    public Branch() {
    }

    public Branch(long id, String name, LocalDateTime idate, LocalDateTime udate, int status) {
        super(id, name, idate, udate, status);
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Branch branch = (Branch) o;
        return contact.equals(branch.contact);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), contact);
    }

    @Override
    public String toString() {
        return "Branch{" +
                "contact=" + contact +
                '}';
    }
}

package edu.coursemanagment.domain;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Objects;

public class Media extends UserBase implements Serializable {
    private long userId;
    private String originalFileName;
    private String fileName;
    private boolean main;
    private String contentType;
    private boolean video;
    private boolean mobile;
    private long length;
    private InputStream inputStream;

    @Override
    public String toString() {
        return "Media{" +
                "userId=" + userId +
                ", originalFileName='" + originalFileName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", main=" + main +
                ", contentType='" + contentType + '\'' +
                ", video=" + video +
                ", mobile=" + mobile +
                ", length=" + length +
                ", inputStream=" + inputStream +
                "} " + super.toString();
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public boolean isMobile() {
        return mobile;
    }

    public void setMobile(boolean mobile) {
        this.mobile = mobile;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Media media = (Media) o;
        return userId == media.userId &&
                main == media.main &&
                video == media.video &&
                mobile == media.mobile &&
                length == media.length &&
                originalFileName.equals(media.originalFileName) &&
                fileName.equals(media.fileName) &&
                contentType.equals(media.contentType) &&
                inputStream.equals(media.inputStream);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), userId, originalFileName, fileName, main, contentType, video, mobile, length, inputStream);
    }
}

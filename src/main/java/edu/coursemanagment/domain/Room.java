package edu.coursemanagment.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Room extends UserBase implements Serializable {
    private int capacity;

    public Room(int capacity) {
        this.capacity = capacity;
    }

    public Room(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, int capacity) {
        super(id, name, idate, udate, status);
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Room room = (Room) o;
        return capacity == room.capacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), capacity);
    }



    public Room() {
    }

    public Room(long id, String name, LocalDateTime idate, LocalDateTime udate, int status) {
        super(id, name, idate, udate, status);
    }

    @Override
    public String toString() {
        return "Room{" +
                "capacity=" + capacity +
                "} " + super.toString();
    }
}

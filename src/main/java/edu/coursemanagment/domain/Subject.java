package edu.coursemanagment.domain;


import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;


public class Subject extends UserBase implements Serializable {
    private BigDecimal price;

    public Subject(BigDecimal price) {
        this.price = price;
    }

    public Subject() {
    }

    public Subject(long id, String name, LocalDateTime idate, LocalDateTime udate, int status, BigDecimal price) {
        super(id, name, idate, udate, status);
        this.price = price;
    }

    public Subject( String name, BigDecimal price) {
        super(name);
        this.price = price;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "price=" + price +
                "} " + super.toString();
    }
}

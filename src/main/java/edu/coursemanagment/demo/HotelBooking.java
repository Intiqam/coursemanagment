package edu.coursemanagment.demo;

import org.springframework.stereotype.Component;

@Component
public class HotelBooking {
    private String name;
   private double pricePerNight;
   private int nbOfNights;

    public HotelBooking() {
    }

    public HotelBooking(String hotelName, double pricePerNight, int nbOfNights) {
        this.name = hotelName;
        this.pricePerNight = pricePerNight;
        this.nbOfNights = nbOfNights;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerNight() {
        return pricePerNight;
    }

    public void setPricePerNight(double pricePerNight) {
        this.pricePerNight = pricePerNight;
    }

    public int getNbOfNights() {
        return nbOfNights;
    }

    public void setNbOfNights(int nbOfNights) {
        this.nbOfNights = nbOfNights;
    }

    public double getTotalPrice(){
        return nbOfNights*pricePerNight;
    }
    public String Owner(){
        return "Intiqam N";
    }

    @Override
    public String toString() {
        return "HotelBooking{" +
                "hotelName='" + name + '\'' +
                ", pricePerNight=" + pricePerNight +
                ", nbOfNights=" + nbOfNights +
                '}';
    }
}

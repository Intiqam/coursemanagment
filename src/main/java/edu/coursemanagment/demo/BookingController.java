package edu.coursemanagment.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/rest")
public class BookingController {
    private List<HotelBooking> hotelBookingList;

    public BookingController() {
        this.hotelBookingList = new ArrayList<>();
        hotelBookingList.add(new HotelBooking("Sheraton",150,3));
        hotelBookingList.add(new HotelBooking("Hilton",200,4));
        hotelBookingList.add(new HotelBooking("Intourist",50,5));

    }
    @GetMapping("/all")
    public List<HotelBooking> getAll(){
        System.out.println(hotelBookingList.toString());
        String a="fafg";
        return  hotelBookingList;
    }
    @GetMapping("/affroad/{price}")
    public List<HotelBooking> getAffroadbale(@PathVariable double price){
        return hotelBookingList.stream()
                .filter(p->p
                        .getPricePerNight()<=price)
                .collect(Collectors
                        .toList());
    }
    @PostMapping("/create")

    public List<HotelBooking> createHotel(@RequestBody HotelBooking hotelBooking){
        hotelBookingList.add(hotelBooking);
        System.out.println(hotelBookingList);

        return hotelBookingList;
    }
}
